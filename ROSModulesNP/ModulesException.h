// -*- c++ -*- $Id: ModulesException.h 54820 2008-11-13 12:53:01Z gcrone $

/*
  ATLAS TEST Software

  Class: MODULESEXCEPTION
  Authors: ATLAS ROS group 	
 */

#ifndef MODULESEXCEPTION_H
#define MODULESEXCEPTION_H

#include "DFExceptions/ROSException.h"
#include <string>
#include <iostream>

namespace ROS {
class ModulesException : public ROSException {
public:
	enum ErrorCode {
		NODATA,                 NOFILE,			  STATE,
		ALL_INVALIDPAGE,        ALL_TOOSMALL,               ALL_TRUNCATE,              ALL_SEQUENCE,
		ALL_ILLSIZE2,           ALL_MARKER,
		SSP_OPEN,               SSP_SSPOPEN,                SSP_SSPCLOSE,              SSP_CLOSE,
		SSP_INITREAD,           SSP_ILLID,                  SSP_CAS,                   SSP_STATE,
		SSP_RESET,
		FIL_INFFREE,            FIL_PAGESIN,                FIL_PSIZE,                 FIL_ILLSIZE,
		FIL_OPEN,               FIL_STATE,                  FIL_CLOSE,    	     FIL_RESET,
		FIL_LINKRESET,          FIL_INIT,                   FIL_PAGESOUT,              FIL_ALIGN,
		FIL_FLUSH,              FIL_PSIZE2,
		ETH_SOCKET,             ETH_SOCKET2,                ETH_SSO,                   ETH_BIND,
		ETH_LISTEN,             ETH_SELECT,                 ETH_BADSIZE,               ETH_BADLINK,
		ETH_CLOSE,              ETH_ACCEPT,                 ETH_STATE,                 ETH_ILLID,
		ETH_ILLSIZE,
		PCIROBIN_NOMEMPOOL,     PCIROBIN_HWERROR,           PCIROBIN_INITERROR,        PCIROBIN_NOPAGE,
		PCIROBIN_WRONGREPLY,    PCIROBIN_INCONSISTENTPOOLS, PCIROBIN_TIMEOUT,          PCIROBIN_INVALIDTICKET,
		PCIROBIN_IOERROR,       PCIROBIN_TOOMANYCHANNELS,   PCIROBIN_WRONGPHYSADDRESS, PCIROBIN_NOMSGMEM,
		PCIROBIN_OLDPPCFW,      PCIROBIN_NONDYNAMIC,        PCIROBIN_ILLL1ID,          PCIROBIN_NOROLPA,
		PCIROBIN_PSIZE,         PCIROBIN_INCON,             PCIROBIN_ROBSW,            PCIROBIN_OVTMP,
		PCIROBIN_SETCPDEF,      PCIROBIN_MEMINEF,           PCIROBIN_PSIZE2,           PCIROBIN_OLDFPGAFW,
		PCIROBIN_ROBTOOSMALL,   PCIROBIN_NOUID,
		DDT_ECROVERFLOW,
		VME_OPEN,		      VME_CLOSE,	          VME_MASTERMAP,             VME_MASTERUNMAP,
		SEQ_UNREGISTERNOTFOUND,
		SGLFR_ILLPOOLTYPE,
		TCPTRIGGER_ILLCHAN,     TCPTRIGGER_SOCKETERROR,     TCPTRIGGER_SOCKETOPTERROR, TCPTRIGGER_BINDERROR,
		TCPTRIGGER_LISTENERROR, TCPTRIGGER_SELECTERROR,	  TCPTRIGGER_SENDERROR,	     TCPTRIGGER_RECEIVEERROR,
		TCPTRIGGER_CONNECTIONCLOSED,
		DUPLICATE_L1ID, DUPLICATE_ROBID,
		BAD_HEADER_MARKER, INVALID_MEMPOOL_NUMBER, PCIROBIN_NOTICKETS, ROBINNP_RXDMA, ROBINNP_TOOMANYCHANNELS, ROBINNP_DUPLICATECHANNEL,
		ROBINNP_DUPLICATEROBID,ROBINNP_DUPLICATEROBUID,ROBINNP_BADCAST
	};
	ModulesException(ErrorCode error, std::string description);
	ModulesException(ErrorCode error);
	ModulesException(ErrorCode error, std::string description, const ers::Context& context);
	ModulesException(ErrorCode error, const ers::Context& context);
	ModulesException(const std::exception& cause, ErrorCode error, std::string description, const ers::Context& context);

	virtual ~ModulesException() throw() { }

protected:
	virtual std::string getErrorString(unsigned int errorId) const;
	virtual ers::Issue * clone() const { return new ModulesException( *this ); }
	static const char * get_uid() {return "ROS::ModulesException";}
	virtual const char* get_class_name() const {return get_uid();}
};


inline ModulesException::ModulesException(ModulesException::ErrorCode error, std::string description)
: ROSException("ROSModules",error,getErrorString(error),description) { }

inline ModulesException::ModulesException(ModulesException::ErrorCode error)
: ROSException("ROSModules",error,getErrorString(error)) { }

inline ModulesException::ModulesException(ModulesException::ErrorCode error, std::string description, const ers::Context& context)
: ROSException("ROSModules",error,getErrorString(error),description,context) { }

inline ModulesException::ModulesException(ModulesException::ErrorCode error, const ers::Context& context)
: ROSException("ROSModules",error,getErrorString(error),context) { }

inline ModulesException::ModulesException(const std::exception& cause, ErrorCode error, std::string description, const ers::Context& context)
: ROSException(cause, "ROSModules", error, getErrorString(error), description, context) {}

inline std::string ModulesException::getErrorString(unsigned int errorId) const {
	std::string rc;
	switch (errorId) {
	case NODATA:
		rc = "Event fragment does not (yet) exist";
		break;
	case NOFILE:
		rc = "Cannot stat input file";
		break;
	case STATE:
		rc = "Channel was in wrong state for requested operation";
		break;
	case ALL_INVALIDPAGE:
		rc = "MemoryPoolNP returned an already locked page or a page with wrong size";
		break;
	case ALL_TOOSMALL:
		rc = "Incoming fragment is too small for a ROD fragment";
		break;
	case ALL_TRUNCATE:
		rc = "Incoming fragment is too large to fit into one page";
		break;
	case ALL_SEQUENCE:
		rc = "Event fragment with unexpected L1ID received";
		break;
	case ALL_ILLSIZE2:
		rc = "The size of the ROD fragment does not match the information in the ROD trailer";
		break;
	case ALL_MARKER:
		rc = "ROD header does not start with 0xee1234ee or 0xeeeeeee";
		break;
	case FIL_ILLSIZE:
		rc = "The value of memoryPoolPageSize cannot be mapped onto the FILAR";
		break;
	case FIL_OPEN:
		rc = "Error from FILAR_Open";
		break;
	case FIL_STATE:
		rc = "Channel is not in the proper state";
		break;
	case FIL_CLOSE:
		rc = "Error from FILAR_Close";
		break;
	case FIL_RESET:
		rc = "Error from FILAR_Reset";
		break;
	case FIL_LINKRESET:
		rc = "Error from FILAR_LinkReset";
		break;
	case FIL_INIT:
		rc = "Error from FILAR_Init";
		break;
	case FIL_PAGESOUT:
		rc = "Error from FILAR_PagesOut";
		break;
	case FIL_FLUSH:
		rc = "Error from FILAR_Flush";
		break;
	case FIL_PAGESIN:
		rc = "Error from FILAR_PagesIn";
		break;
	case FIL_INFFREE:
		rc = "Error from FILAR_InFree";
		break;
	case FIL_PSIZE:
		rc = "Memory pool page sizes are not all identical";
		break;
	case FIL_ALIGN:
		rc = "The PCI address is not 64-bit aligned";
		break;
	case FIL_PSIZE2:
		rc = "Please check the Filar page size in the configuration database";
		break;
	case SSP_OPEN:
		rc = "error from SLINK_Open";
		break;
	case SSP_SSPOPEN:
		rc = "error from SLINK_SSPOpen";
		break;
	case SSP_SSPCLOSE:
		rc = "error from SLINK_SSPClose";
		break;
	case SSP_CLOSE:
		rc = "error from SLINK_SSPClose";
		break;
	case SSP_INITREAD:
		rc = "error from SSP_InitRead";
		break;
	case SSP_CAS:
		rc = "error from SSP_ControlAndStatus";
		break;
	case SSP_STATE:
		rc = "Channel is in wrong state for the requested command";
		break;
	case SSP_RESET:
		rc = "error from SSP_Reset";
		break;
	case SSP_ILLID:
		rc = "Parameter <id> is out of range";
		break;
	case ETH_SOCKET:
		rc = "Error from call to socket()";
		break;
	case ETH_SOCKET2:
		rc = "Error from call to SetSocketOptions()";
		break;
	case ETH_BIND:
		rc = "Error from call to bind()";
		break;
	case ETH_LISTEN:
		rc = "Error from call to listen()";
		break;
	case ETH_SELECT:
		rc = "Error from call to select()";
		break;
	case ETH_BADSIZE:
		rc = "Failed to read size of event fragment";
		break;
	case ETH_BADLINK:
		rc = "Unable to read incoming message";
		break;
	case ETH_CLOSE:
		rc = "Error from call to close()";
		break;
	case ETH_ACCEPT:
		rc = "Error from call to accept()";
		break;
	case ETH_STATE:
		rc = "Channel is in wrong state for the requested command";
		break;
	case ETH_ILLID:
		rc = "Parameter <id> is out of range";
		break;
	case ETH_ILLSIZE:
		rc = "The size of the ROS fragment does not match the information in the ROS header";
		break;
	case ETH_SSO:
		rc = "Error from SetSocketOptions";
		break;
	case PCIROBIN_NOMEMPOOL:
		rc = "PciRobin device has no memory pool available";
		break;
	case PCIROBIN_HWERROR:
		rc = "ROBIN hardware error!!";
		break;
	case PCIROBIN_INITERROR:
		rc = "ROBIN initialization error!!";
		break;
	case PCIROBIN_NOPAGE:
		rc = "No memory page for ROBIN replys available";
		break;
	case PCIROBIN_WRONGREPLY:
		rc = "Wrong ROBIN reply message";
		break;
	case PCIROBIN_NOMSGMEM:
		rc = "No DMA memory to send messages to the ROBIN";
		break;
	case PCIROBIN_TIMEOUT:
		rc = "Timeout while waiting for a ROBIN reply";
		break;
	case PCIROBIN_INVALIDTICKET:
		rc = "Invalid ticked passed to getfragment!";
		break;
	case PCIROBIN_IOERROR:
		rc = "ROBIN I/O error!";
		break;
	case PCIROBIN_WRONGPHYSADDRESS:
		rc = "Channel Physical Address out of the valid range";
		break;
	case PCIROBIN_TOOMANYCHANNELS:
		rc = "The configuration defines more channels than supported by the ROBIN H/W";
		break;
	case PCIROBIN_INCONSISTENTPOOLS:
		rc = "Inconsistent page size definition between channels of the same module";
		break;
	case PCIROBIN_OLDPPCFW:
		rc = "The PPC firmware of the Robin is out of date";
		break;
	case PCIROBIN_OLDFPGAFW:
		rc = "The FPGA firmware of the RobinNP is out of date";
		break;
	case PCIROBIN_NONDYNAMIC:
		rc = "You cannot change a non dynamic parameter";
		break;
	case PCIROBIN_ILLL1ID:
		rc = "The Robin has returned an event with an unexpected L1ID";
		break;
	case PCIROBIN_NOROLPA:
		rc = "Unknown ROL";
		break;
	case PCIROBIN_PSIZE:
		rc = "Page sizes are inconsistent";
		break;
	case PCIROBIN_INCON:
		rc = "Certain parameters in the database are not consistent";
		break;
	case PCIROBIN_ROBSW:
		rc = "Error in the status word in the ROB header";
		break;
	case PCIROBIN_ROBTOOSMALL:
		rc = "The ROB fragment is too small to extract the required information (ROD header incomplete)";
		break;
	case PCIROBIN_OVTMP:
		rc = "The over temperature flag of the Robin is set";
		break;
	case PCIROBIN_SETCPDEF:
		rc = "A Robin configuration parameter was forced to the default value";
		break;
	case PCIROBIN_MEMINEF:
		rc = "The event buffer memory of the Robin is not fully used";
		break;
	case PCIROBIN_PSIZE2:
		rc = "The page size of the Robin was forced to a consistent value";
		break;
	case PCIROBIN_NOTICKETS:
		rc = "Attempt to configure RobinNP to use no tickets";
		break;
	case PCIROBIN_NOUID:
		rc = "Unknown UID received";
		break;
	case DDT_ECROVERFLOW:
		rc = "ECR overflow when polling Robin";
		break;
	case VME_OPEN:
		rc = "Error on VME_Open";
		break;
	case VME_CLOSE:
		rc = "Error on VME_Close";
		break;
	case VME_MASTERMAP:
		rc = "Error on VME_MasterMap";
		break;
	case VME_MASTERUNMAP:
		rc = "Error on VME_MasterUnmap";
		break;
	case SEQ_UNREGISTERNOTFOUND:
		rc = "Channel not found when unregister";
		break;
	case SGLFR_ILLPOOLTYPE:
		rc = "Illegal memory pool type";
		break;
	case TCPTRIGGER_ILLCHAN:
		rc = "# channels not equal to one";
		break;
	case TCPTRIGGER_SOCKETERROR:
		rc = "Error on socket call";
		break;
	case TCPTRIGGER_SOCKETOPTERROR:
		rc = "Error on setsockopt call";
		break;
	case TCPTRIGGER_BINDERROR:
		rc = "Error on bind call";
		break;
	case TCPTRIGGER_LISTENERROR:
		rc = "Error on listen call";
		break;
	case TCPTRIGGER_SELECTERROR:
		rc = "Error on select call";
		break;
	case TCPTRIGGER_SENDERROR:
		rc = "Error on send call";
		break;
	case TCPTRIGGER_RECEIVEERROR:
		rc = "Error on receive call";
		break;
	case TCPTRIGGER_CONNECTIONCLOSED:
		rc = "Connection closed";
		break;
	case DUPLICATE_L1ID:
		rc = "Skipping duplicate LVL1 id";
		break;
	case DUPLICATE_ROBID:
		rc = "Found duplicate ROB id ";
		break;
	case BAD_HEADER_MARKER:
		rc = "Unexpected header marker ";
		break;
	case INVALID_MEMPOOL_NUMBER:
		rc = "Memory Pool size mismatch ";
		break;
	case ROBINNP_RXDMA:
		rc = "Invalid max RX pages";
		break;
	case ROBINNP_TOOMANYCHANNELS:
		rc = "The configuration defines more channels than supported by the RobinNP H/W";
		break;
	case ROBINNP_DUPLICATECHANNEL:
		rc = "The configuration contains a duplicate link ID";
		break;
	case ROBINNP_DUPLICATEROBID:
		rc = "The configuration contains a ROB ID";
		break;
	case ROBINNP_DUPLICATEROBUID:
		rc = "The configuration contains a ROB UID";
		break;
	case ROBINNP_BADCAST:
		rc = "Failed typecast";
		break;
	default:
		rc = "";
		break;
	}
	return rc;
}

}
#endif //MODULESEXCEPTION1_H
