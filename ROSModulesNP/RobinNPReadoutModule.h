#ifndef ROBINNPREADOUTMODULE_H
#define ROBINNPREADOUTMODULE_H

#include <vector>
#include <string>
#include <map>
#include "ROSRobinNP/RobinNP.h"
#include "ROSModulesNP/DDTScheduledUserAction.h"
#include "ROSMemoryPoolNP/WrapperMemoryPoolNP.h"
#include "ROSCore/DataChannel.h"
#include "ROSCore/ReadoutModule.h"
#include "DFSubSystemItem/Config.h"
#include "DFThreads/DFCountedPointer.h"
#include "ROSModulesNP/RobinNPDataChannel.h"

namespace ROS 
{
class RobinNPReadoutModule : public ReadoutModule
{
public:
	//! Constructor.
	RobinNPReadoutModule(void);
	//! Destructor.
	virtual ~RobinNPReadoutModule() noexcept;
	//! Configuration storage method.
	/*!
       	 \param configuration configuration block for storage and later use.
	 */
	RobinNPReadoutModule(const RobinNPReadoutModule&) = delete; //!< Copy-Constructor - NON-COPYABLE.
	RobinNPReadoutModule& operator=(const RobinNPReadoutModule&) = delete; //!< Assignment Operator - NON-COPYABLE
	virtual void setup(DFCountedPointer<Config> configuration);
	//! Data channel access method.
	/*!
        	 \return vector of data channels associated with the readout module.
	 */
	virtual const std::vector<DataChannel *> * channels();
	//! Enables all readout links and starts RobinNP control threads
	/*!
         	 \param cmd run control command reference
	 */
	virtual void prepareForRun(const daq::rc::TransitionCmd& cmd);
	//! Disables all readout links and stops RobinNP control threads
	/*!
          	 \param cmd run control command reference
	 */
	virtual void stopGathering(const daq::rc::TransitionCmd& cmd);
	//! Instantiate and configure RobinNP object
	/*!
          	 \param cmd run control command reference
	 */
	virtual void configure(const daq::rc::TransitionCmd& cmd);
	//! De-configure and delete RobinNP object
	/*!
          	 \param cmd run control command reference
	 */
	virtual void unconfigure(const daq::rc::TransitionCmd& cmd);
	//! Clear all data driven trigger statistics
	virtual void clearInfo();
	//! Publish data driven trigger statistics
	virtual void publishFullStatistics(void);
	//! Place data channels in discard mode
	/*!
             	 \param uid vector of data channel names
	 */
	virtual void disable(const std::vector<std::string>& uid);
	//! Take data channels out of discard mode
	/*!
             	 \param uid vector of data channel names
	 */
	virtual void enable(const std::vector<std::string>& uid);
	//! Initialise DMA tracking infrastructure
	/*!
             	 \param numTickets maximum number of parallel requests to process
	 */
	void initTicketing(unsigned int numTickets);
	//! Get number of subRobs (ROBgroups) associated with readout module
	/*!
	             	 \return number of subRobs (ROBgroups) associated with the readout module.
	 */
	unsigned int getNumSubRobs();
	//! Get number of readout links per subRob (ROBgroup) associated with readout module
	/*!
	             	 \return number of readout links per subRob (ROBgroup) associated with the readout module.
	 */
	unsigned int getNumRolsPerSubRob();
	void user(const daq::rc::UserCmd& command);

private:
	std::string getBistErrorString(u_int bistStatus);

	WrapperMemoryPoolNP *m_memoryPool;
	RobinNP *m_robIn;                    // A pointer to the PciRobIn object controlling the RobIn hardware
	u_int m_physicalAddress;           // The RobIn ID number (= PCI logical number)
	u_int m_numberOfDataChannels;
	u_int m_firstRolPhysicalAddress;
	u_int m_rolPhysicalAddressTable[s_maxRols];
	unsigned int m_maxTickets;
	std::map <std::string, RobinNPDataChannel *> m_uidMap;
	std::vector <DataChannel *> m_dataChannels;
	ScheduledUserAction *m_DDTscheduledUserAction;
	static bool s_firstModuleFound;
	static bool s_DDTCreated;
	DFCountedPointer<Config> m_configuration;
};
}
#endif 
