// $Id: DDTScheduledUserAction.h 48105 2008-04-03 07:43:08Z joos $

/*
  ATLAS ROS Software

  Class: DDTScheduledUserAction
  Authors:  M.Joos, J.Petersen
*/

#ifndef DDTSCHEDULEDUSERACTION_H
#define DDTSCHEDULEDUSERACTION_H

#include <vector>
#include <sys/types.h>
#include "ROSCore/ScheduledUserAction.h"
#include "ROSCore/TriggerInputQueue.h"
#include "ROSRobinNP/RobinNP.h"

namespace ROS 
{
  class DDTScheduledUserAction : public ScheduledUserAction 
  {
  public:   
    DDTScheduledUserAction(RobinNP *robIn, u_int rolPhysicalAddress,  int deltaTimeMs,
			   u_int triggerQueueSize, u_int triggerQueueUnblockOffset);
    DDTScheduledUserAction(const DDTScheduledUserAction&) = delete; //!< Copy-Constructor - NON-COPYABLE.
    DDTScheduledUserAction& operator=(const DDTScheduledUserAction&) = delete; //!< Assignment Operator - NON-COPYABLE
    virtual ~DDTScheduledUserAction();
    virtual void reactTo(void);
    void prepareForRun(void);
    void stopEB(void);
    void printTimeHistogram(void);
    void clearInfo(void);

  private:
    RolNP *m_rol;                                 // A pointer to the ROL from which we read the ECRs
    bool m_reactToEnabled;                      // This flag determines if we request and process ECRs
    u_int m_mostRecentIdSent;                   // Highest L1ID that has been pushed into the trigger Q
    u_int m_mostRecentIdReceived;               // Highest L1ID that has been received by the ROL
    TriggerInputQueue *m_inputToTriggerQueue; // The input end of the trigger Q. The other end is in: ROSIO/src/RobinDataDrivenTriggerIn.cpp
    int m_deltaTimeMs;                          // The time intervall for calling the reactTo method
    int m_low;                                  // Same value as m_deltaTimeMs. Low edge of timing histogram. Separate variable for name clarity
    static const int m_width = 10;              // Width of a histogram bin in milliseconds
    static const int m_bins = 12;               // Number of bins in the histogram (10 + underflow + overflow)
    int *m_hist;                                // The histogram for the fluctuation of the time between calls to reactTo

    void pushToQueue(u_int firstToPush, u_int lastToPush);
  };
}
#endif //DDTSCHEDULEDUSERACTION_H
