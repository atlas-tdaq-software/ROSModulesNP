// Emulated Readout module using NPRequestDescriptors

#include <cstdint>
#include <cstdlib>
#include <map>
#include <mutex>
#include <set>
#include <string>
#include <thread>
#include <vector>
#include <pthread.h>

#include "ROSDescriptorNP/DataPage.h"
#include "ROSDescriptorNP/NPReadoutModule.h"
#include "ROSDescriptorNP/NPRequestDescriptor.h"
#include "DFdal/EmulatedNPModule.h"
#include "DFdal/EmulatedReadoutConfiguration.h"
#include "DFdal/InputChannel.h"
#include "DFdal/RobinDataChannel.h"
#include "DFdal/RobinChannelConfiguration.h"

#include "ROSEventFragment/ROBFragment.h"

#include "DFDebug/DFDebug.h"

#include "tbb/concurrent_queue.h"

namespace ROS {
   class EmulatedNPModule : public NPReadoutModule {
   public:
      EmulatedNPModule();
      virtual ~EmulatedNPModule() noexcept;

      virtual void configure(const daq::rc::TransitionCmd&);
      virtual void unconfigure(const daq::rc::TransitionCmd&);
      virtual void clearInfo();
      virtual void prepareForRun(const daq::rc::TransitionCmd&);
      virtual void stopGathering(const daq::rc::TransitionCmd&);

      virtual void disable(const std::vector<std::string>&);
      virtual void enable(const std::vector<std::string>&);

      void user(const daq::rc::UserCmd& command);
      void dump();
      virtual const std::vector<DataChannel *> *channels(); //Get the list of channels connected to this module
      ISInfo *getISInfo();
      virtual void requestFragment(NPRequestDescriptor* descriptor);

      virtual void clearFragments(std::vector<uint32_t>& level1Ids);
      void registerChannels(unsigned int&,
                            std::vector<tbb::concurrent_bounded_queue<DataPage*>* >&,
                            std::vector<std::vector<uint32_t> >&);
      void getL1CountPointers(std::vector<unsigned int*>&,std::vector<bool*>&);
  private:
      void run();
      void channelRequestFragment(unsigned int,NPRequestDescriptor*);

      bool m_active;
      std::thread* m_thread;

      tbb::concurrent_bounded_queue<NPRequestDescriptor*> m_messageQueue;
      tbb::concurrent_bounded_queue<DataPage*> m_pageQueue;
      std::vector<DataPage*> m_pages;


      std::vector<unsigned int> m_channelIds;
      std::vector<unsigned int> m_channelFragSize;

      unsigned int m_getDelay;
      unsigned int m_pageSize;
      unsigned int m_detectorEventType;
      unsigned int m_numberOfStatusElements;
      unsigned int m_maxDataElements;
      unsigned int m_statusBlockPosition;

      float m_getMiss;
      bool m_randomSize;

      uint64_t m_freeTime;

      unsigned int m_notCleared;
      unsigned int m_storeSize;
      unsigned int m_lastl1;
      unsigned int m_maxL1;
      unsigned int m_wrapCount;
      std::set<unsigned int> m_fragmentSet;
      std::mutex m_mutex;
      unsigned int m_lastRequestedId;
      std::map<unsigned int,unsigned int> m_channelIndex;
      std::map<unsigned int,unsigned int> m_channelMap;
      std::map<std::string,unsigned int> m_channelUIDMap;
      unsigned int m_baseIndex;
      bool* m_enabled;
      unsigned int m_channelCount;
   };
}


using namespace ROS;


/********************************************/
EmulatedNPModule::EmulatedNPModule() : m_enabled(0),m_channelCount(0) {
/********************************************/
  DEBUG_TEXT(DFDB_ROSFM, 15, " EmulatedNPModule::constructor: Entered (empty function)");
}


/*********************************************/
EmulatedNPModule::~EmulatedNPModule() noexcept {
/*********************************************/
  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedNPModule::destructor entered");
  if (m_enabled!=0) {
     delete [] m_enabled;
  }
  for (auto page : m_pages) {
     delete [] page;
  }

  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedNPModule::destructor completed");
}

/*****************************************/
void EmulatedNPModule::configure(const daq::rc::TransitionCmd&) {
/*****************************************/
   const daq::df::EmulatedNPModule* myDal=
      m_configurationDB->get<daq::df::EmulatedNPModule>(m_uid);
   const daq::df::EmulatedReadoutConfiguration* myDalConfig=
      myDal->get_Configuration();
   m_getDelay=myDalConfig->get_GetFragmentDelay();
   m_getMiss=myDalConfig->get_GetMissingFragment();

   m_pageSize=myDal->get_fragmentSize();
   if (m_pageSize<25) {
      m_pageSize=72;
   }

   unsigned int nPages=myDal->get_nPages();

   std::string dataSizeEnum=myDalConfig->get_DataSize();
   m_randomSize=(dataSizeEnum=="RANDOM");
   m_detectorEventType=myDalConfig->get_DetEvType();
   m_numberOfStatusElements=myDalConfig->get_NumberOfStatusElements();
   m_maxDataElements=m_pageSize
//     -((sizeof(ROBFragment::ROBHeader)/sizeof(u_int))-ROBFragment::s_nStatusElements+m_numberOfStatusElements)
      -(sizeof(RODFragment::RODHeader)+sizeof(RODFragment::RODTrailer))/sizeof(unsigned int);
   m_statusBlockPosition=myDalConfig->get_StatusBlockPosition();

   m_channelCount=0;
   auto nc=myDal->get_Contains().size();
   m_channelIds.reserve(nc);
   m_channelFragSize.reserve(nc);
   for (std::vector<const daq::core::ResourceBase*>::const_iterator channelIter=myDal->get_Contains().begin(); 
           channelIter!=myDal->get_Contains().end(); channelIter++) {
      const daq::df::InputChannel* channelPtr=
         m_configurationDB->cast<daq::df::InputChannel, daq::core::ResourceBase> (*channelIter);
      if (channelPtr==0) {
         std::cerr << "Module " << m_uid
                   << " Contains relationship to something (" << (*channelIter)->class_name() <<
            ") that is not an InputChannel\n";
         continue;
      }
      else {
         if (!(*channelIter)->disabled(*m_partition)) {
            unsigned int channelId=channelPtr->get_Id();
            std::string channelUID=channelPtr->UID();
            std::cout << "Pushing back rolID " << std::hex << channelId << std::dec
                      << ", UID=" << channelUID << std::endl;
            m_channelIds.push_back(channelId);
            m_channelUIDMap[channelUID]=channelId;

            unsigned int fragSize=m_maxDataElements;
            std::string channelClass=channelPtr->class_name();
            if (channelClass=="RobinDataChannel") {
               std::cout << channelUID << " is a RobinDataChannel\n";

               const daq::df::RobinDataChannel* robinChannelPtr=
                  m_configurationDB->cast<daq::df::RobinDataChannel, daq::core::ResourceBase> (*channelIter);

               const daq::df::RobinChannelConfiguration* channelConf=
                  m_configurationDB->cast<daq::df::RobinChannelConfiguration>(
                     robinChannelPtr->get_Configuration());

               if (channelConf!=0) {
                  std::cout << "Channel test size=" << channelConf->get_TestSize() << std::endl;
                  if (channelConf->get_TestSize()<=m_maxDataElements) {
                     fragSize=channelConf->get_TestSize();
                  }
               }
            }
            m_channelFragSize.push_back(fragSize);
            m_channelCount++;
         }
      }
   }

   m_enabled=new bool[m_channelCount];
   for (unsigned int chan=0;chan<m_channelCount;chan++) {
      m_enabled[chan]=true;
   }

   m_maxL1=myDal->get_maxL1();
   m_storeSize=myDal->get_storeSize();
   if (m_storeSize>m_maxL1) {
      m_storeSize=m_maxL1;
   }

   m_pages.reserve(nPages);
   for (unsigned int page=0;page<nPages;page++) {
      unsigned int* buf=new unsigned int[m_pageSize];
      m_pages.emplace_back(new DataPage(buf,0));
      m_pageQueue.push(m_pages[page]);
   }

   DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedNPModule::configure: done");
}

/***************************************************************************/
void EmulatedNPModule::registerChannels(
   unsigned int& baseIndex,
   std::vector<tbb::concurrent_bounded_queue<DataPage*>* > & pageQueueVec,
   std::vector<std::vector<uint32_t> >& channelIdVec) {
/***************************************************************************/
   m_baseIndex=baseIndex;
   channelIdVec.push_back(m_channelIds);
   pageQueueVec.push_back(&m_pageQueue);
   for (auto id : m_channelIds) {
      m_channelIndex[id]=baseIndex;
      m_channelMap[baseIndex]=id;
      baseIndex++;
   }
}

void EmulatedNPModule::getL1CountPointers(std::vector<unsigned int*>& countPointers,
                                          std::vector<bool*>& enabled) {
   auto nChans=m_channelIds.size();
   countPointers.reserve(countPointers.size()+nChans);
   enabled.reserve(enabled.size()+nChans);
   for (unsigned int chan=0;chan<nChans;chan++) {
      countPointers.push_back(&m_lastl1);
      enabled.push_back(&m_enabled[chan]);
   }
}

void EmulatedNPModule::prepareForRun(const daq::rc::TransitionCmd&) {
   m_fragmentSet.clear();
   for (m_lastl1=0; m_lastl1<m_storeSize; m_lastl1++) {
      m_fragmentSet.insert(m_lastl1);
   }
   m_lastl1--;
   m_notCleared=0;
   m_wrapCount=0;

   m_active=true;
   m_thread=new std::thread(&EmulatedNPModule::run,this);
   pthread_setname_np(m_thread->native_handle(),"EmulatedNP");

}

void EmulatedNPModule::stopGathering(const daq::rc::TransitionCmd&) {
   m_active=false;
   m_messageQueue.abort();
   m_pageQueue.abort();
   std::cout << "EmulatedNPModule::stopGathering() Waiting for thread to exit\n";
   m_thread->join();
   delete m_thread;
   m_thread=0;
}


/*****************************************/
void EmulatedNPModule::unconfigure(const daq::rc::TransitionCmd&) {
/*****************************************/
   if (m_enabled!=0) {
      delete m_enabled;
      m_enabled=0;
   }

   for (auto page : m_pages) {
      delete page->virtualAddress();
      delete page;
   }
   m_pages.clear();
   m_pageQueue.clear();
   m_channelIds.clear();
   m_channelFragSize.clear();
}



void EmulatedNPModule::disable(const std::vector<std::string>& argVec){
   uint64_t disabledCount=0;
   for (std::string arg: argVec) {
      if (m_channelUIDMap.find(arg) != m_channelUIDMap.end()) {
         unsigned int rolID=m_channelUIDMap[arg];
         if (m_channelIndex.find(rolID)!=m_channelIndex.end()) {
            unsigned int chan=m_channelIndex[rolID]-m_baseIndex;
            m_enabled[chan]=false;
            disabledCount++;
            std::cout << "disabling channel " <<chan << ", ROL ID " << std::hex << rolID << std::dec << std::endl;
         }
      }
   }

   for (unsigned int chan=0;chan<m_channelCount;chan++) {
      std::cout << "channel " << chan << " enabled=" << m_enabled[chan] << std::endl;
   }
}

void EmulatedNPModule::enable(const std::vector<std::string>& argVec){
   uint64_t enabledCount=0;
   for (std::string arg: argVec) {
      if (m_channelUIDMap.find(arg) != m_channelUIDMap.end()) {
         unsigned int rolID=m_channelUIDMap[arg];
         if (m_channelIndex.find(rolID)!=m_channelIndex.end()) {
            unsigned int chan=m_channelIndex[rolID]-m_baseIndex;
            m_enabled[chan]=true;
            enabledCount++;
            std::cout << "enabling channel " <<chan << ", ROL ID " << std::hex << rolID << std::dec << std::endl;
         }
      }
   }
   for (unsigned int chan=0;chan<m_channelCount;chan++) {
      std::cout << "channel " << chan << " enabled=" << m_enabled[chan] << std::endl;
   }
}

ISInfo* EmulatedNPModule::getISInfo(){
   std::cout << "Number of failed clears: " << m_notCleared << std::endl;
   std::cout << "Event counter wrapped: " << m_wrapCount << std::endl;
   return 0;
}

/*****************************************/
void EmulatedNPModule::clearInfo(void)
/*****************************************/
{
  DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedNPModule::clearInfo: called");
}


// /*********************************************************************/
const std::vector<DataChannel *> *EmulatedNPModule::channels(void) {
// /*********************************************************************/

   DEBUG_TEXT(DFDB_ROSFM, 15, "EmulatedNPModule::channels: called / done");
//   return &m_dataChannels;
   return 0;
}




/**************************************************************************************/
void EmulatedNPModule::channelRequestFragment(unsigned int channelIndex,
                                              NPRequestDescriptor* descriptor) {
/**************************************************************************************/

   //std::cout << "EmulatedNPModule::channelRequestFragment() requesting data from channel " << id << std::endl;

//    if (m_freeTime < now) {
//       m_freeTime=now;
//    }

//    m_freeTime+=m_getDelay;

   unsigned int rolId=m_channelMap[channelIndex-m_baseIndex];

   ROBFragment::ROBHeader* robHeader=
      (ROBFragment::ROBHeader*)descriptor->headerPage(channelIndex)->data();

   robHeader->generic.startOfHeaderMarker=EventFragment::s_robMarker;
   unsigned int headerSize=
      (sizeof(ROBFragment::ROBHeader)/sizeof(unsigned int))
      -ROBFragment::s_nStatusElements+m_numberOfStatusElements;
   robHeader->generic.headerSize=headerSize;
   robHeader->generic.sourceIdentifier=rolId;
   robHeader->generic.formatVersionNumber=EventFragment::s_formatVersionNumber;
   robHeader->generic.numberOfStatusElements=m_numberOfStatusElements;

//   unsigned int* data=descriptor->dataPage(channelIndex)->virtualAddress();
   unsigned int* data;
   if (descriptor->status(channelIndex)==NPRequestDescriptor::RequestStatus::NEW_REQUEST) {
      DataPage* page;
      m_pageQueue.pop(page);
      descriptor->assignPage(channelIndex,page);
      data=page->virtualAddress();
   }
   else {
      data=descriptor->dataPage(channelIndex)->virtualAddress();
   }

   RODFragment::RODHeader* rodHeader=(RODFragment::RODHeader*) &data[0];
   rodHeader->startOfHeaderMarker=EventFragment::s_rodMarker;
   rodHeader->headerSize=sizeof(RODFragment::RODHeader) / sizeof (unsigned int);
   rodHeader->formatVersionNumber=0x03010000;
   rodHeader->sourceIdentifier=rolId;
   rodHeader->level1Id=descriptor->level1Id();
   rodHeader->detectorEventType=m_detectorEventType;

   unsigned int numberOfDataElements;
   if (m_enabled[channelIndex-m_baseIndex]) {
      if (m_fragmentSet.find(descriptor->level1Id())!=m_fragmentSet.end()) {
         if (m_randomSize) {
            numberOfDataElements=rand()%m_maxDataElements;
         }
         else {
            numberOfDataElements=m_channelFragSize[channelIndex-m_baseIndex];
         }
         for (unsigned int word=0;word<m_numberOfStatusElements;word++) {
            robHeader->statusElement[word]=0;
         }
         descriptor->status(channelIndex,NPRequestDescriptor::RequestStatus::REQUEST_COMPLETE);
      }
      else {
         numberOfDataElements=0;
         if (descriptor->level1Id()>m_lastl1) {
            robHeader->statusElement[0]=0x40000000;
            if (m_numberOfStatusElements>1) {
               robHeader->statusElement[1]=m_lastl1;
            }
            descriptor->status(channelIndex,NPRequestDescriptor::RequestStatus::REQUEST_PENDING);
         }
         else {
            descriptor->status(channelIndex,NPRequestDescriptor::RequestStatus::REQUEST_COMPLETE);
            robHeader->statusElement[0]=0x20000000;
         }
      }
   }
   else {
      numberOfDataElements=0;
      descriptor->status(channelIndex,NPRequestDescriptor::RequestStatus::REQUEST_COMPLETE);
      robHeader->statusElement[0]=0x80000000;
   }

   RODFragment::RODTrailer* rodTrailer=
      (RODFragment::RODTrailer*) &data[rodHeader->headerSize+numberOfDataElements];
   rodTrailer->numberOfStatusElements=0;
   rodTrailer->numberOfDataElements=numberOfDataElements;
   rodTrailer->statusBlockPosition=0;

   robHeader->generic.totalFragmentsize
      =robHeader->generic.headerSize+rodHeader->headerSize+numberOfDataElements+3;

      //std::cout << "EmulatedNPModule::ChannelRequestfragment() queuing request with EmulatedDescriptorReadoutModule  descriptor->dataPage(id)->virtualAddress()="<<std::hex << descriptor->dataPage(id)->virtualAddress() <<std::dec <<"\n";
}


void EmulatedNPModule::dump() {
   int index=0;
   std::cout << "EmulatedNPModule::  Fragment buffer:" << std::hex;
   for (auto frag : m_fragmentSet) {
      std::cout << " " << frag;
      index++;
      if (index%16==0) {
         std::cout << "\n\t\t";
      }
   }
   std::cout << std::dec << std::endl;
}
void EmulatedNPModule::user(const daq::rc::UserCmd& command) {
	if (command.commandName()=="dump") {
//      std::vector<std::string> arguments=command.commandParameters();
//      for (auto argv : arguments) {
//          if (argv=="descriptors") {
//          }
//      }
      dump();
   }
}

void EmulatedNPModule::requestFragment(NPRequestDescriptor* descriptor) {
//    std::cout << "EmulatedNPModule::requestFragment(): ";

   descriptor->expectReply(1);

   m_lastRequestedId=descriptor->level1Id();

   m_messageQueue.push(descriptor);
}

void EmulatedNPModule::clearFragments(std::vector<uint32_t>& level1Ids){
   bool error=false;
   unsigned int last=m_lastl1;
   uint64_t count=0;
   for (auto l1 : level1Ids) {
      auto iter=m_fragmentSet.find(l1);
      if (iter!=m_fragmentSet.end()) {
         m_fragmentSet.erase(iter);
         if (m_lastl1<m_maxL1) {
            m_lastl1++;
         }
         else {
            m_lastl1=0;
            m_wrapCount++;
         }
         m_fragmentSet.insert(m_lastl1);
         count++;
      }
      else {
         if (!error) {
            std::cout << "Failed to delete id" << std::hex;
            error=true;
         }
         std::cout << " " << l1;
         m_notCleared++;
      }
   }

   if (error) {
//      m_active=false;
      std::cout << ", last l1 on entry=" << last
                << ", now=" << m_lastl1 << ", last requested=" << m_lastRequestedId
                << std::dec << ", wraps=" << m_wrapCount << std::endl;
      std::cout << "level1Ids vector:" << std::hex;
      for (auto l1 : level1Ids) {
         std::cout << " " << l1;
      }
      std::cout << std::dec << std::endl;
      dump();
   }
}



void   EmulatedNPModule::run() {
   unsigned int nRequests=0;
   while (m_active) {

      try {
         NPRequestDescriptor* descriptor;
         m_messageQueue.pop(descriptor);
         auto channelList=descriptor->localIds();
         for (unsigned int index=0;index<channelList->size();index++) {
            auto chan=channelList->at(index);
            if (chan>=m_baseIndex && chan<m_baseIndex+m_channelCount) {
               channelRequestFragment(chan,descriptor);
            }
         }
         nRequests++;
         if (m_collectorQueue!=0) {
            m_collectorQueue->push(descriptor);
         }
      }
      catch (tbb::user_abort& abortException) {
         std::cout << "EmulatedNPModule run aborting...\n";
         m_active=false;
      }
   }
   std::cout << "EmulatedNPModule handled " << nRequests << " requests"
             << std::endl;
}

//FOR THE PLUGIN FACTORY
extern "C" {
   extern ReadoutModule* createEmulatedNPModule();
}
ReadoutModule* createEmulatedNPModule(){
   return (new EmulatedNPModule());
}
