/*****************************************************/
/*  ATLAS ROS Software 			 		             */
/*							                         */
/*  Class: RobinNPDescriptorReadoutModule 	    	 */
/*  Author: William Panduro Vazquez     RHUL		 */
/*			(j.panduro.vazquez@cern.ch)				 */
/*****************************************************/


#include "ROSModulesNP/RobinNPDescriptorModule.h"

#include "DFDebug/DFDebug.h"
#include "ROSUtilities/ROSErrorReporting.h"
#include "DFSubSystemItem/ConfigException.h"
#include "ROSEventFragment/ROBFragment.h"
#include "ROSModulesNP/ModulesException.h"

#include "dal/Partition.h"

using namespace ROS;

bool RobinNPDescriptorReadoutModule::s_firstModuleFound = false;
/******************************************/
RobinNPDescriptorReadoutModule::RobinNPDescriptorReadoutModule(void)
/******************************************/
: m_robIn(0),
  m_physicalAddress(0),
  m_numberOfDataChannels(0),
  m_firstRolPhysicalAddress(0),
  m_rolPhysicalAddressTable(0),
  m_numberOfLevel1Last(0),
  m_firstChannelIndex(0),
  m_configurationDal(0),
  m_channelsDal(0),
  m_channelListDal(0),
  m_robinNPConfigurationDal(0)
{ 
	DEBUG_TEXT(DFDB_ROSFM, 20, "RobinNPDescriptorReadoutModule::Constructor: called");
}


/***************************************/
RobinNPDescriptorReadoutModule::~RobinNPDescriptorReadoutModule() noexcept
		/***************************************/
		{
	DEBUG_TEXT(DFDB_ROSFM, 20, "RobinNPDescriptorReadoutModule::Destructor: called");
	if (m_robIn != 0)
	{
		DEBUG_TEXT(DFDB_ROSFM, 20, "RobinNPDescriptorReadoutModule::Destructor: m_robIn is at " << HEX(m_robIn));

		delete m_robIn;
		m_robIn = 0;
	}
		}

/********************************************************************/
void RobinNPDescriptorReadoutModule::setup(DFCountedPointer<Config> /*configuration*/)
/********************************************************************/
{
	// Get the configuration parameters here
	m_configurationDal = m_configurationDB->get<daq::df::RobinNPDescriptorReadoutModule>(m_uid);
	m_channelsDal = m_configurationDB->cast<daq::core::ResourceSet, daq::core::ResourceBase> (m_configurationDal);
	m_channelListDal = new std::vector<const daq::core::ResourceBase*>;
	if(m_channelsDal != 0){
		const std::vector<const daq::core::ResourceBase*>* m_channelListDalTemp =  &m_channelsDal->get_Contains();
		for(auto channelCand : *m_channelListDalTemp){
			const daq::df::InputChannel* channelPtr=m_configurationDB->cast<daq::df::InputChannel, daq::core::ResourceBase> (channelCand);
			if(channelPtr != 0){
				if(!(channelPtr->disabled(*m_partition))){
					m_channelListDal->push_back(channelPtr);
				}
			}
		}
	}

	m_robinNPConfigurationDal = m_configurationDB->cast<daq::df::RobinConfiguration>(m_configurationDal->get_Configuration());
}


/******************************************/
void RobinNPDescriptorReadoutModule::prepareForRun(const daq::rc::TransitionCmd& )
/******************************************/
{ 

	m_robIn->resetSubRobChannels();

	DEBUG_TEXT(DFDB_ROSFM, 15, "RobinNPDescriptorReadoutModule::prepareForRun: called");

	for (u_int chan = 0; chan < m_numberOfDataChannels; chan++){
		m_robIn->initStatistics(m_rolPhysicalAddressTable[chan]);

		m_robIn->setRolEnabled(true, m_rolPhysicalAddressTable[chan]);
		m_robIn->restartRol(m_rolPhysicalAddressTable[chan]);
	}


	if(!m_robIn->getThreadState()) {
		std::cout << "Starting Threads!" << std::endl;
		m_robIn->startThreads();
	}

	m_robIn->resetDumpFileCounter();
	m_robIn->resetErrorDumpFileCounter();

	RobinNPStats *statistics = m_robIn->getGeneralStatistics();
	statistics->m_running = true;


	m_tsStopLast=std::chrono::system_clock::now();

}


/***********************************/
void RobinNPDescriptorReadoutModule::stopGathering(const daq::rc::TransitionCmd& )
/***********************************/
{ 
	DEBUG_TEXT(DFDB_ROSFM, 15, "RobinNPDescriptorReadoutModule::stopEB: called");

	std::cout << "stop gathering" << std::endl;

	RobinNPStats *statistics = m_robIn->getGeneralStatistics();
	statistics->m_running = false;

	//Nothing to be done at this level but the data channels should disable the S-Links
	for (u_int chan = 0; chan < m_numberOfDataChannels; chan++){
		m_robIn->setRolEnabled(false, m_rolPhysicalAddressTable[chan]);
	}

}


void RobinNPDescriptorReadoutModule::dumpDalConfig(){
	std::cout << std::endl;
	std::cout << "DFdal test" << std::endl;
	std::cout << "Module Name: " << m_uid << std::endl;
	std::cout << std::endl;

	std::cout << "EmulateMem = " << m_configurationDal->get_EmulateMem() << std::endl;
	std::cout << "MemWriteTimeout = " << m_configurationDal->get_MemWriteTimeout() << std::endl;
	std::cout << "MemReadTimeout = " << m_configurationDal->get_MemReadTimeout() << std::endl;
	std::cout << "MemFifoFill = " << m_configurationDal->get_MemFifoFill() << std::endl;
	std::cout << "PhysicalAddress = " << m_configurationDal->get_PhysAddress() << std::endl;
	std::cout << "Fragment Dump File Path = " << m_configurationDal->get_FragDumpFilePath() << std::endl;
	std::cout << "Fragment Dump Limit (# files) = " << m_configurationDal->get_FragDumpLimit() << std::endl;
	std::cout << "UPF Delay Line Steps = " << m_configurationDal->get_UPFDelayLineSteps() << std::endl;
	std::cout << std::endl;

	for (auto channel : *m_channelListDal){

		const daq::df::RobinDataChannel* dataChannelConfiguration= m_configurationDB->cast<daq::df::RobinDataChannel, daq::core::ResourceBase> (channel);
		const daq::df::RobinChannelConfiguration* robinNPChannelConfiguration = m_configurationDB->cast<daq::df::RobinChannelConfiguration>(dataChannelConfiguration->get_Configuration());

		std::cout << "Channel ID = " << dataChannelConfiguration->get_Id() << std::endl;
		std::cout << "UID = " << dataChannelConfiguration->UID() << std::endl;

		std::cout << "CRC Check Interval = " << robinNPChannelConfiguration->get_CrcCheckInterval() << std::endl;
		std::cout << "RolDataGen = " << robinNPChannelConfiguration->get_RolDataGen() << std::endl;
		std::cout << "TestSize = " << robinNPChannelConfiguration->get_TestSize() << std::endl;

	}

	std::cout << std::endl;

	std::cout << "FPGA Version = " << HEX(m_robinNPConfigurationDal->get_FPGAVersion()) << std::endl;
	std::cout << "Max Rx Pages = " << m_robinNPConfigurationDal->get_MaxRxPages() << std::endl;
	std::cout << "Page Size = " << m_robinNPConfigurationDal->get_Pagesize() << std::endl;
	std::cout << std::endl;

}

/**************************************/
void RobinNPDescriptorReadoutModule::configure(const daq::rc::TransitionCmd& )
/**************************************/
{

	dumpDalConfig();

	//Get the configuration variables for the whole board

	if (m_channelListDal->empty())
	{
		//ers warning
		return;
	}

	m_numberOfDataChannels = m_channelListDal->size();
	m_physicalAddress      = m_configurationDal->get_PhysAddress();  //This identifies a CARD not a ROL!!!

	//This table links the physical addresses of the ROLs on a ROBIN to the channel numbers. The value "19" means "invalid"
	m_rolPhysicalAddressTable = new u_int[m_numberOfDataChannels];
	m_numberOfLevel1Last = new uint64_t[12];

	m_robIn = new RobinNP(m_physicalAddress, runModeGen2ROS, m_collectorQueue);
	m_robIn->init();

	m_channelVector.resize(m_robIn->getNumRols());

	if(m_numberOfDataChannels > m_robIn->getNumRols()){
		CREATE_ROS_EXCEPTION(ex1, ModulesException, ROBINNP_TOOMANYCHANNELS, "ERROR: Configuration includes " << m_numberOfDataChannels << " ROLs but RobinNP has a maximum of " << m_robIn->getNumRols());
		throw(ex1);
	}

	if (m_robinNPConfigurationDal->get_FPGAVersion() != m_robIn->getFirmwareVersion())
	{
		CREATE_ROS_EXCEPTION(ex1, ModulesException, PCIROBIN_OLDFPGAFW, "ERROR: The FPGA firmware of the RobinNP (physical Address = "
				<< m_physicalAddress << ") is out of date: Configuration expects = 0x" << HEX(m_robinNPConfigurationDal->get_FPGAVersion()) << " but FPGA has = 0x"
				<< HEX(m_robIn->getFirmwareVersion()));
		throw(ex1);
	}

	RobinNPConfig configuration;
	configuration.setMaxRxPages(m_robinNPConfigurationDal->get_MaxRxPages());
	configuration.setPageSize(m_robinNPConfigurationDal->get_Pagesize());
	configuration.setMemEmulationState(m_configurationDal->get_EmulateMem());
	configuration.setMemWriteTimeout(m_configurationDal->get_MemWriteTimeout());
	configuration.setMemReadTimeout(m_configurationDal->get_MemReadTimeout());
	configuration.setMemfifoFill(m_configurationDal->get_MemFifoFill());
	configuration.setDumpFilePath(m_configurationDal->get_FragDumpFilePath());
	configuration.setFragDumpLimit(m_configurationDal->get_FragDumpLimit());
	configuration.setUPFDelaySteps(m_configurationDal->get_UPFDelayLineSteps());
	configuration.setErrorDumpLimit(m_configurationDal->get_InputErrorDumpLimit());
	std::set<u_int> roblist;
	std::set<std::string> uidlist;

	unsigned int channelIndex = 0;
	for (auto channel : *m_channelListDal){

		const daq::df::RobinDataChannel* dataChannelConfiguration= m_configurationDB->cast<daq::df::RobinDataChannel, daq::core::ResourceBase> (channel);
		const daq::df::RobinChannelConfiguration* robinNPChannelConfiguration = m_configurationDB->cast<daq::df::RobinChannelConfiguration>(dataChannelConfiguration->get_Configuration());

		if(channelIndex != 0){
			for(u_int index = 0; index < channelIndex; index++){
				if(uidlist.find(dataChannelConfiguration->UID()) != uidlist.end()){
					CREATE_ROS_EXCEPTION(ex1, ModulesException, ROBINNP_DUPLICATEROBUID, "ERROR: Configuration includes ROB UID " << dataChannelConfiguration->UID().c_str() << " more than once");
					throw(ex1);
				}
				if(m_rolPhysicalAddressTable[index] == dataChannelConfiguration->get_PhysAddress()){
					CREATE_ROS_EXCEPTION(ex1, ModulesException, ROBINNP_DUPLICATECHANNEL, "ERROR: Configuration includes ROL " << dataChannelConfiguration->get_PhysAddress() << " more than once");
					throw(ex1);
				}
				if(roblist.find(dataChannelConfiguration->get_Id()) != roblist.end()){
					CREATE_ROS_EXCEPTION(ex1, ModulesException, ROBINNP_DUPLICATEROBID, "ERROR: Configuration includes ROB " << dataChannelConfiguration->get_Id() << " more than once");
					throw(ex1);
				}
			}
		}

		roblist.insert(dataChannelConfiguration->get_Id());
		uidlist.insert(dataChannelConfiguration->UID());

		m_numberOfLevel1Last[channelIndex] = 0;

		if (dataChannelConfiguration->get_PhysAddress() > 11)
		{
			CREATE_ROS_EXCEPTION(ex1, ModulesException, PCIROBIN_NOROLPA, "ERROR: Invalid ROL physical address for data channel # " << channelIndex << " on ROBIN " << m_physicalAddress);
			throw(ex1);
		}
		m_rolPhysicalAddressTable[channelIndex] = dataChannelConfiguration->get_PhysAddress();

		// first data channel in the configuration
		if (!s_firstModuleFound && channelIndex == 0)
		{
			m_firstRolPhysicalAddress = dataChannelConfiguration->get_PhysAddress();
			s_firstModuleFound = true;
		}

		m_channelVector[dataChannelConfiguration->get_PhysAddress()] = m_robIn->getParentSubRob(dataChannelConfiguration->get_PhysAddress());
		m_channelToPhysical.push_back(dataChannelConfiguration->get_PhysAddress());
		m_uidMap[dataChannelConfiguration->UID()] = dataChannelConfiguration->get_PhysAddress();

		if(channelIndex == 0){
			configuration.setCRCCheckInterval(robinNPChannelConfiguration->get_CrcCheckInterval());
		}

		RobinNPROLConfig rolConfig;
		rolConfig.setDiscardMode(false); //check DB entry for this
		rolConfig.setRobId(dataChannelConfiguration->get_Id());
		rolConfig.setRolDataGen(robinNPChannelConfiguration->get_RolDataGen());
		rolConfig.setRolEnabled(false);
		rolConfig.setTestSize(robinNPChannelConfiguration->get_TestSize());
		configuration.configureRol(rolConfig,dataChannelConfiguration->get_PhysAddress());

		channelIndex++;

	}

	m_robIn->configure(configuration);


	initStatistics();

}


/****************************************/
void RobinNPDescriptorReadoutModule::unconfigure(const daq::rc::TransitionCmd& )
/****************************************/
{
	DEBUG_TEXT(DFDB_ROSFM, 15, "RobinNPDescriptorReadoutModule::unconfigure: Called");

	delete[] m_rolPhysicalAddressTable;
	delete[] m_numberOfLevel1Last;

	//Release the Robin
	if (m_robIn != 0)
	{
		delete m_robIn;
		m_robIn = 0;
	}


	DEBUG_TEXT(DFDB_ROSFM, 15, "RobinNPDescriptorReadoutModule::unconfigure: Done");
}



/************************************************/
void RobinNPDescriptorReadoutModule::disable(const std::vector<std::string>& channelList)
/************************************************/
{
	//Start discard mode
	for (auto uid : channelList) {
		DEBUG_TEXT(DFDB_ROSFM, 15, "RobinNPDescriptorReadoutModule::disable: Called with UID = " << uid);

		std::map<std::string, unsigned int >::iterator it;
		it = m_uidMap.find(uid);
		if (it == m_uidMap.end())
		{
			DEBUG_TEXT(DFDB_ROSFM, 20, "RobinNPDescriptorReadoutModule::disable: Cannot find data chanel for UID = " << uid);
		}
		else
		{
			m_robIn->requestEnterDiscardMode(it->second);

		}

		DEBUG_TEXT(DFDB_ROSFM, 15, "RobinNPDescriptorReadoutModule::disable: Done");
	}
}


//Note: "enable" in this context means "enable channel" and therefore "disable discard mode"
/***********************************************/
void RobinNPDescriptorReadoutModule::enable(const std::vector<std::string>& channelList)
/***********************************************/
{
	//End discard mode
	for (auto uid : channelList) {
		DEBUG_TEXT(DFDB_ROSFM, 15, "RobinNPDescriptorReadoutModule::enable: Called with UID = " << uid);

		std::map<std::string, unsigned int >::iterator it;
		it = m_uidMap.find(uid);
		if (it == m_uidMap.end())
		{
			DEBUG_TEXT(DFDB_ROSFM, 20, "RobinNPDescriptorReadoutModule::enable: Cannot find data chanel for UID = " << uid);
		}
		else
		{
			m_robIn->requestLeaveDiscardMode(it->second);

		}

		DEBUG_TEXT(DFDB_ROSFM, 15, "RobinNPDescriptorReadoutModule::enable: Done");
	}
}

/**********************************/
void RobinNPDescriptorReadoutModule::clearInfo()
/**********************************/
{
	DEBUG_TEXT(DFDB_ROSFM, 15, "RobinNPDescriptorReadoutModule::clearInfo: Entered");

}


/**************************************************/
void RobinNPDescriptorReadoutModule::publishFullStatistics(void)
/**************************************************/
{
	DEBUG_TEXT(DFDB_ROSFM, 15, "RobinNPDescriptorReadoutModule::publishFullStatistics: Entered");


	DEBUG_TEXT(DFDB_ROSFM, 15, "RobinNPDescriptorReadoutModule::publishFullStatistics: Done");
}


/******************************************/
void RobinNPDescriptorReadoutModule::requestFragment(NPRequestDescriptor *descriptor){

	DEBUG_TEXT(DFDB_ROSFM, 15, "RobinNPDescriptorReadoutModule::requestFragment: called");

	//unsigned int descriptorId = descriptor->getId();
	std::vector<std::vector<unsigned int>> localChannelList(m_robIn->getNumSubRobs());
	//std::cout << "ID Size " << descriptor->localIds()->size() << std::endl;
	std::vector<unsigned int>::const_iterator end = descriptor->localIds()->end();
	for (std::vector<unsigned int>::const_iterator it = descriptor->localIds()->begin(); it != end; ++it){
		//std::cout << "chan here?? " << *it << " " << m_firstChannelIndex << std::endl;

		if((*it - m_firstChannelIndex) < m_numberOfDataChannels){
			u_int rolID = m_channelToPhysical[*it - m_firstChannelIndex];
			//Place request on DMA queue for relevant channels
			//std::cout << "chan here " << rolID << " " << m_channelVector[rolID] << std::endl;


			if(descriptor->status(*it) != NPRequestDescriptor::RequestStatus::REQUEST_COMPLETE){
				//std::cout << "chan " << rolID << " " << m_channelVector[rolID] << std::endl;
				localChannelList[m_channelVector[rolID]].push_back(rolID);
			}
		}
	}

	//Issue one dma completion notification request per subRob and pass the total number of active subRobs back to the descriptor
	unsigned int activeSubRobs = 0;
	for(unsigned int subRob = 0; subRob < m_robIn->getNumSubRobs(); ++subRob){
		if(localChannelList[subRob].size() != 0){
			//std::cout << "Sending " << localChannelList[subRob].size() << " requests for subRob " << subRob << std::endl;
			m_robIn->requestFragment(subRob,&localChannelList[subRob],descriptor);
			activeSubRobs++;
		}

	}

	descriptor->expectReply(activeSubRobs);

}
/******************************************/

/******************************************/
void RobinNPDescriptorReadoutModule::clearFragments(std::vector<unsigned int> &l1IDs){
	/******************************************/
	DEBUG_TEXT(DFDB_ROSFM, 15, "RobinNPReadoutModule::clearRequest: called");
	//std::cout << "Clear Fragments Called" << std::endl;
	//std::vector<unsigned int>::const_iterator end = l1IDs.end();
	/*for (std::vector<unsigned int>::const_iterator it = l1IDs.begin() ; it != end; ++it){
		std::cout << "ID to clear " << *it << std::endl;
	}*/
	m_robIn->clearRequest(&l1IDs);
}

/***************************************************************/
const std::vector<DataChannel *> * RobinNPDescriptorReadoutModule::channels()
/***************************************************************/
{
	DEBUG_TEXT(DFDB_ROSFM, 15, "RobinNPDescriptorReadoutModule::channels: Called - NOT IMPLEMENTED!");
	CREATE_ROS_EXCEPTION(exunimp, ModulesException, PCIROBIN_PSIZE, "RobinNPDescriptorReadoutModule::channels not implemented");
	ers::error(exunimp);
	return 0;
}

void RobinNPDescriptorReadoutModule::registerChannels(uint32_t &localBaseID, std::vector<tbb::concurrent_bounded_queue<DataPage*>*>& queues, std::vector<std::vector<uint32_t> >& atlasIDs){

	for(unsigned int subRob = 0; subRob < m_robIn->getNumSubRobs(); ++subRob){
		m_robIn->registerSubRobIndex(subRob,(subRob+atlasIDs.size()));
	}

	m_robIn->initialisePageQueues(queues);

	std::vector<std::vector<uint32_t>> tempIDs(m_robIn->getNumSubRobs());

	m_firstChannelIndex = localBaseID;

	for (auto channel : *m_channelListDal){

		const daq::df::RobinDataChannel* dataChannelConfiguration= m_configurationDB->cast<daq::df::RobinDataChannel, daq::core::ResourceBase> (channel);

		m_robIn->registerChannelIndex(dataChannelConfiguration->get_PhysAddress(),localBaseID);
		u_int rolSubRob = m_robIn->getSubRob(dataChannelConfiguration->get_PhysAddress());
		tempIDs[rolSubRob].push_back(dataChannelConfiguration->get_Id());

		localBaseID++;
	}

	atlasIDs.insert(atlasIDs.end(),tempIDs.begin(),tempIDs.end());

}

void RobinNPDescriptorReadoutModule::getL1CountPointers(std::vector<unsigned int *>& statsVec,std::vector<bool*>& enableVec){

	for (auto channel : *m_channelListDal){

		const daq::df::RobinDataChannel* dataChannelConfiguration= m_configurationDB->cast<daq::df::RobinDataChannel, daq::core::ResourceBase> (channel);

		RobinNPROLStats *statistics = m_robIn->getROLStatistics(dataChannelConfiguration->get_PhysAddress());
		statsVec.push_back(&statistics->m_mostRecentId);
		enableVec.push_back(m_robIn->getROLEnableStatus(dataChannelConfiguration->get_PhysAddress()));
	}
}

void RobinNPDescriptorReadoutModule::setCollectorQueue(
		tbb::concurrent_bounded_queue<NPRequestDescriptor*>* descriptorQueue){
	m_collectorQueue=descriptorQueue;
	m_robIn->setCollectorQueue(descriptorQueue);
}

/***********************************/
ISInfo* RobinNPDescriptorReadoutModule::getISInfo()
/***********************************/
{

	//Add methods to read out temperature of RobinNP

	auto tsStop=std::chrono::system_clock::now();

	//m_robIn->testProbe();

	for(unsigned int rol = 0; rol < m_robIn->getNumRols(); ++rol){

		RobinNPROLStats *statistics = m_robIn->getROLStatistics(rol);
		// This is just a selection of the most interesting parameters
		m_statistics.pagesFree[rol]      = statistics->m_pagesFree;
		m_statistics.pagesInUse[rol]     = statistics->m_pagesInUse;
		m_statistics.mostRecentId[rol]   = statistics->m_mostRecentId;
		m_statistics.rolXoffStat[rol]    = statistics->m_rolXoffStat;
		m_statistics.rolDownStat[rol]    = statistics->m_rolDownStat;
		m_statistics.bufferFull[rol]     = statistics->m_bufferFull;
		m_statistics.numberOfXoffs[rol]  = statistics->m_xoffcount;
		m_statistics.numberOfLdowns[rol]  = statistics->m_ldowncount;

		m_statistics.xoffPercentage[rol]  = statistics->m_xoffpercentage;
		m_statistics.memreadcorruptions[rol] = statistics->m_memreadcorruptions;
		m_statistics.fragsReceived[rol]  = statistics->m_fragStat[fragsReceived];
		m_statistics.fragsIndexed[rol]  = statistics->m_fragStat[fragsAdded];
		m_statistics.fragsAvailable[rol]  = statistics->m_fragStat[fragsAvail];
		m_statistics.fragsTruncated[rol] = statistics->m_fragStat[fragsTruncated];
		m_statistics.fragsCorrupted[rol] = statistics->m_fragStat[fragsCorrupted];
		m_statistics.fragsReplaced[rol]  = statistics->m_fragStat[fragsReplaced];
		m_statistics.fragsOutofsync[rol] = statistics->m_fragStat[fragsOutOfSync];
		m_statistics.fragsShort[rol]  = statistics->m_fragStat[fragsShort];
		m_statistics.fragsLong[rol]  = statistics->m_fragStat[fragsLong];
		m_statistics.fragsDiscarded[rol]  = statistics->m_fragStat[fragsDiscarded];
		m_statistics.fragsFormatError[rol]  = statistics->m_fragStat[fragsFormatError];
		m_statistics.fragsMarkerError[rol]  = statistics->m_fragStat[fragsMarkerError];
		m_statistics.fragsFramingError[rol]  = statistics->m_fragStat[fragsFramingError];
		m_statistics.fragsCtrlWordError[rol]  = statistics->m_fragStat[fragsCtrlWordError];
		m_statistics.fragsDataWordError[rol]  = statistics->m_fragStat[fragsDataWordError];
		m_statistics.fragsSizeMismatch[rol]  = statistics->m_fragStat[fragsSizeMismatch];
		m_statistics.fragsRejected[rol]  = statistics->m_fragStat[fragsRejected];
		m_statistics.fragsTxError[rol]  = statistics->m_fragStat[fragsTxError];
		m_statistics.rolEnabled[rol] = m_robIn->getRolState(rol);

		m_statistics.fragsIncompleteOnDMA[rol] = statistics->m_fragsIncomplete;
		m_statistics.rolInputBandwidth[rol] = statistics->m_rolInputBandwidth;
		m_statistics.rolOutputBandwidth[rol] = statistics->m_rolOutputBandwidth;

		m_statistics.rolReadoutFraction[rol] = statistics->m_readoutFraction;

		m_statistics.pagesRejected[rol] = statistics->m_pageStat[pagesSupressed];
		m_statistics.pagesInvalid[rol] = statistics->m_pageStat[pagesInvalid];
		m_statistics.pagesProvided[rol] = statistics->m_pageStat[pagesProvided];
		m_statistics.numberOfNotDeleted[rol] = statistics->m_fragStat[fragsMissOnClear];
		m_statistics.numberOfMayCome[rol] = statistics->m_fragStat[fragsPending];
		m_statistics.numberOfNeverToCome[rol] = statistics->m_fragStat[fragsNotAvail];
		m_statistics.robId[rol] = statistics->m_robId;
		m_statistics.multipageIndexError[rol] = statistics->m_multipageIndexError;

		//m_statistics.statusErrors0[rol] =

		// level1 rate (for this channel)
		uint64_t numberOfLevel1Delta;
		if (m_numberOfLevel1Last[rol] == 0 || !m_statistics.rolEnabled[rol]){
			numberOfLevel1Delta = m_statistics.fragsReceived[rol];
		}
		else{
			numberOfLevel1Delta = m_statistics.fragsReceived[rol] - m_numberOfLevel1Last[rol];
		}

		auto elapsed=tsStop - m_tsStopLast;
		m_statistics.level1RateHz[rol] = (u_int)(numberOfLevel1Delta / ((float)std::chrono::duration_cast<std::chrono::microseconds> (elapsed).count()/1e6));

		m_numberOfLevel1Last[rol] = m_statistics.fragsReceived[rol];
	}

	m_tsStopLast = tsStop;

	return &m_statistics;
}


/***********************************/
void RobinNPDescriptorReadoutModule::initStatistics()
/***********************************/
{
	std::cout << "Initialising Statistics" << std::endl;
	//Add methods to read out temperature of RobinNP

	unsigned int numChannels = m_robIn->getNumRols();

	// This is just a selection of the most interesting parameters
	m_statistics.pagesFree.resize(numChannels);
	m_statistics.pagesInUse.resize(numChannels);
	m_statistics.mostRecentId.resize(numChannels);
	m_statistics.rolXoffStat.resize(numChannels);
	m_statistics.rolDownStat.resize(numChannels);
	m_statistics.bufferFull.resize(numChannels);
	m_statistics.numberOfXoffs.resize(numChannels);
	m_statistics.numberOfLdowns.resize(numChannels);
	m_statistics.xoffPercentage.resize(numChannels);

	m_statistics.memreadcorruptions.resize(numChannels);
	m_statistics.rolInputBandwidth.resize(numChannels);
	m_statistics.rolOutputBandwidth.resize(numChannels);
	m_statistics.rolReadoutFraction.resize(numChannels);

	m_statistics.fragsReceived.resize(numChannels);
	m_statistics.fragsIndexed.resize(numChannels);
	m_statistics.fragsAvailable.resize(numChannels);
	m_statistics.fragsTruncated.resize(numChannels);
	m_statistics.fragsCorrupted.resize(numChannels);
	m_statistics.fragsReplaced.resize(numChannels);
	m_statistics.fragsOutofsync.resize(numChannels);
	m_statistics.fragsIncompleteOnDMA.resize(numChannels);
	m_statistics.pagesRejected.resize(numChannels);
	m_statistics.pagesInvalid.resize(numChannels);
	m_statistics.pagesProvided.resize(numChannels);
	m_statistics.multipageIndexError.resize(numChannels);

	m_statistics.numberOfNotDeleted.resize(numChannels);
	m_statistics.numberOfMayCome.resize(numChannels);
	m_statistics.numberOfNeverToCome.resize(numChannels);
	m_statistics.robId.resize(numChannels);
	m_statistics.rolEnabled.resize(numChannels);

	m_statistics.fragsShort.resize(numChannels);
	m_statistics.fragsLong.resize(numChannels);
	m_statistics.fragsDiscarded.resize(numChannels);
	m_statistics.fragsFormatError.resize(numChannels);
	m_statistics.fragsMarkerError.resize(numChannels);
	m_statistics.fragsFramingError.resize(numChannels);
	m_statistics.fragsCtrlWordError.resize(numChannels);
	m_statistics.fragsDataWordError.resize(numChannels);
	m_statistics.fragsSizeMismatch.resize(numChannels);
	m_statistics.fragsRejected.resize(numChannels);
	m_statistics.fragsTxError.resize(numChannels);

	// level1 rate
	m_statistics.level1RateHz.resize(numChannels);

}

/***********************************************************************************/
void RobinNPDescriptorReadoutModule::user(const daq::rc::UserCmd& command)
/***********************************************************************************/
{
	DEBUG_TEXT(DFDB_ROSFM, 15, "RobinNPDescriptorReadoutModule::userCommand: Called with commandName = " << command.commandName());

	if((command.commandName() == "clear") || (command.commandName() == "CLEAR")) {
		for (auto rol_id : command.commandParameters()) {
			std::map<std::string, unsigned int >::iterator it;
			it = m_uidMap.find(rol_id);
			if (it == m_uidMap.end())
			{
				DEBUG_TEXT(DFDB_ROSFM, 20, "RobinNPDescriptorReadoutModule::userCommand: Cannot find data channel for UID = " << rol_id);
			}
			else
			{
				SEND_ROS_INFO("Restarting ROL 0x" << HEX(m_robIn->getAtlasID(it->second)));
				//m_robIn->restartRol(it->second);
				m_robIn->requestEnterDiscardMode(it->second);
				m_robIn->requestLeaveDiscardMode(it->second);
				DEBUG_TEXT(DFDB_ROSFM, 20, "RobinNPDescriptorReadoutModule::userCommand: Channel " << rol_id << " has been reset");
			}
		}

                if (command.commandParameters().size() == 0 ) {
                  // If no channels are provided, then we reset all enabled
                  // channels
                  for (auto& [name, channel] : m_uidMap) {
                    if (*m_robIn->getROLEnableStatus(channel)) {

                      SEND_ROS_INFO("Restarting ROL 0x" << HEX(m_robIn->getAtlasID(channel)));
                      m_robIn->requestEnterDiscardMode(channel);
                      m_robIn->requestLeaveDiscardMode(channel);
                    }
                  }
                }
        }

	else {
		DEBUG_TEXT(DFDB_ROSFM, 10, "RobinNPDescriptorReadoutModule::userCommand:Illegal command received");
		return;
	}


	DEBUG_TEXT(DFDB_ROSFM, 15, "RobinNPDescriptorReadoutModule::userCommand: Done");
}

//FOR THE PLUGIN FACTORY
extern "C"
{
extern ReadoutModule* createRobinNPDescriptorReadoutModule();
}
ReadoutModule* createRobinNPDescriptorReadoutModule()
{
	return (new RobinNPDescriptorReadoutModule());
}

