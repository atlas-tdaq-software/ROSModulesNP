// $Id: test_RobinNP.cpp jpanduro $
/********************************************************************/
/*                                                              	*/
/*  file test_robinNP 	                                         	*/
/*                                                              	*/
/*  Test program for the ROS ModulesNP & ChannelsNP Class       	*/
/*                                                              	*/
/*  2013 Author(s) - W. Panduro Vazquez	                        	*/
/*					 M. Joos										*/
/*                                                              	*/
/*  The program allows to:                                      	*/
/*  . exercise the methods of the ROS ModulesNP & ChannelsNP Class	*/
/*  . measure the performance                                   	*/
/*  																*/
/*  Inherited from original test_Robin - M. Joos & J. Vermeulen		*/
/*                                                              	*/
/********************************************************************/

#include <unistd.h>
#include <iostream>
#include <signal.h>
#include <iomanip>
#include <errno.h>
#include <exception>
#include <vector>
#include <string>
#include <sys/types.h>
#include <getopt.h>
#include <stdio.h>
#include <fcntl.h>
#include <sched.h>
#include <sstream>
#include "ROSEventFragment/ROBFragment.h"
#include "ROSModulesNP/RobinNPReadoutModule.h"
#include "ROSModulesNP/RobinNPDataChannel.h"
#include "ROSBufferManagement/Buffer.h"
#include "DFSubSystemItem/Config.h"
#include "ROSGetInput/get_input.h"
#include "rcc_time_stamp/tstamp.h"
#include "ROSRobinNP/RobinNPHardware.h"
#include "test_RobinNP.h"
#include "ConfigInfoObject.h"
#include "RunControl/FSM/FSMCommands.h"

using namespace ROS;


//Prototypes
int getdefparams(DFCountedPointer<Config> configuration);


// Globals
int getstat;
u_int dblevel = 0, dbpackage = DFDB_ROSFM;
u_long crctab16[65536]; // for CRC calculation
const u_int maxNumberOfTickets = s_DMAQueueFIFOSize; //Defined in ROSRobinNP/RobinNPHardware.h

ConfigInfoObject gConfInfo;

struct rolVariables
{
	std::vector <u_int> idgroup;
	u_int nevents;
	u_int nevents2;
	u_int nextticket;
	u_int nextl1id;
	u_int tickets[maxNumberOfTickets][2];
	u_int firstLoopReAskCount;
	u_int totalReceivedFragments;
	u_int firstLoopReAskCountByRolTicket[maxNumberOfTickets];
	u_int secondLoopReAskCountByRolTicket[maxNumberOfTickets];
	u_int lastReceivedLevel1Id;
	u_int lastClearedLevel1Id;
	u_int numberOfClearsQueued;
	int requeueCounter;
	tstamp tStartDelay[maxNumberOfTickets];
	tstamp tLastStartDelay;
	double nextLvl1IdNotRounded;
	double waitingTime[maxNumberOfTickets];
};

/******************************************************************************/
void DoPerformanceMeasurement(ReadoutModule** ReadoutModule, u_int procAffinity)
/******************************************************************************/
{
	cpu_set_t mask;
	CPU_ZERO(&mask);
	CPU_SET(procAffinity, &mask);
	std::cout << "Setting Core Affinity of main process to " << procAffinity << std::endl;
	int result = sched_setaffinity(0, sizeof(mask), &mask); //fix affinity for main process to a single core to avoid time slicing issues with timestamp library
	if (result != 0){
		perror("sched_setaffinity: ");
	}

	rolVariables rolArray[gConfInfo.maxChannels];
	std::vector<DataChannel *> *channels;
	u_int isempty, loop, looprol, rol;
	u_int numberOfEvents, numberOfEventsFirst, numberOfEventsLeft, maxNumberOfReasks1, maxNumberOfReasks2;
	u_int nrols = 12, nrolsUsed = 0;
	u_int deletesPerGroup;
	ROBFragment *fragment;
	tstamp ts0, ts1, ts2, tStopDelay, tsRef;
	double stepInLvl1Ids, percent = 10.0, timingResult, timePerEvent = 0.0;
	double reducFactorStep = 0.01, reducFactor = 0.0, timeWaited, minWaitInterval;
	double startupModFactor;
	double rateResult = 0.0, rateL1 = 0.0;
	double maxRateNoReAsk = 0.0, maxRateReAsk = 0.0, maxRateL1NoReAsk = 0.0, maxRateL1ReAsk = 0.0;
	const double initialWaitingTime = 5000000.0, waitIncrement = 1.0;
	double waitingTimeIncrement = 0.0;
	bool firstCycle = true, reAskFlag, stopFlag = false;
	float stepSize = 1.0;
	int downCount = 0, upCount = 0;
	bool increaseReducFactorFlag = true;
	bool firstFlag = true;
	float measuredBandWidthNoReAsk, measuredBandWidthReAsk;
	int fragSize, receivedFragSize = 0;

#define DEBUG(...);
	//#define DEBUG printf

	deletesPerGroup = gConfInfo.deleteGroupSize;
	percent = gConfInfo.requestFraction;

	for(u_int rolIndex = 0; rolIndex < gConfInfo.maxChannels; ++rolIndex){
		nrolsUsed += (gConfInfo.linkMask & (1 << rolIndex)) >> rolIndex;
	}

	std::cout << "Number of ROLs used = " << nrolsUsed << " link mask = " << gConfInfo.linkMask << std::endl;
	if (gConfInfo.noClearsFlag)
	{
		std::cout << "No clears sent!" << std::endl;
		gConfInfo.iterateFlag = false;
	}

	// determine minimum waitingTime assuming 5 us minimum per trigger, with a threshold
	for (looprol = 0; looprol < nrols; looprol++)
	{
		rolArray[looprol].firstLoopReAskCount = 0;
		rolArray[looprol].totalReceivedFragments = 0;
		rolArray[looprol].lastReceivedLevel1Id = 1;
		rolArray[looprol].lastClearedLevel1Id = 0;
		rolArray[looprol].numberOfClearsQueued = 0;

		for (u_int i = 0; i < maxNumberOfTickets; i++)
		{
			rolArray[looprol].firstLoopReAskCountByRolTicket[i] = 0;
			rolArray[looprol].secondLoopReAskCountByRolTicket[i] = 0;
			rolArray[looprol].waitingTime[i] = 0.0;
		}
	}

	numberOfEvents = gConfInfo.numberOfLVL1AcceptsPerCycle; //1M normally
	numberOfEventsFirst = (u_int) (gConfInfo.setupNeventFraction * (float) numberOfEvents); //fraction = 0.5, hence initial set of 500k
	numberOfEventsLeft = numberOfEvents - numberOfEventsFirst; //500k remain

	for(u_int rolIndex = 0; rolIndex < gConfInfo.maxChannels; ++rolIndex)
	{
		rolArray[rolIndex].nevents2 = numberOfEventsFirst * ((gConfInfo.linkMask & (1 << rolIndex)) >> rolIndex);
		rolArray[rolIndex].nevents = (int) ((float) rolArray[rolIndex].nevents2 * percent / 100); //reading out 100% to 500k here
	}

	if (!gConfInfo.noClearsFlag){
		stepInLvl1Ids = 100.0 / percent;
	}
	else{
		stepInLvl1Ids = 0.0;
	}

	for(u_int rolIndex = 0; rolIndex < gConfInfo.maxChannels; ++rolIndex)
	{
		rolArray[rolIndex].nextLvl1IdNotRounded = 0.0;
		rolArray[rolIndex].nextl1id = 0;
	}

	channels = const_cast<std::vector<DataChannel *> *>(ReadoutModule[0]->channels());

	//Calculate optimal number of tickets to use per ROL accounting for extra 23 words in payload for header
	unsigned int numberOfTicketsStore[2] = {};
	unsigned int numRolsPerSubRob = ((RobinNPReadoutModule*)ReadoutModule[0])->getNumRolsPerSubRob();
	unsigned int numSubRobs = ((RobinNPReadoutModule*)ReadoutModule[0])->getNumSubRobs();

	if(nrolsUsed < (numRolsPerSubRob + 1)){
		numberOfTicketsStore[0] = floor(s_DMAQueueFIFOSize*1.0/(ceil((gConfInfo.numberOfWordsPerFragment + 23.0)/gConfInfo.pageSize)*nrolsUsed));
	}
	else
	{
		numberOfTicketsStore[0] = floor(s_DMAQueueFIFOSize*1.0/(ceil((gConfInfo.numberOfWordsPerFragment + 23.0)/gConfInfo.pageSize)*numRolsPerSubRob));
		numberOfTicketsStore[1] = floor(s_DMAQueueFIFOSize*1.0/(ceil((gConfInfo.numberOfWordsPerFragment + 23.0)/gConfInfo.pageSize)*(nrolsUsed - numRolsPerSubRob)));
	}
	for(u_int subRob = 0; subRob < numSubRobs; ++subRob)
	{
		std::cout << "Optimal number of tickets per ROL for SubRob " << subRob << " calculated to be " << numberOfTicketsStore[subRob] << std::endl;
		if(numberOfTicketsStore[subRob] > gConfInfo.memoryPoolNumPages)
		{
			std::cout << "Optimal count larger than maximum allowed memory pool pages, setting to this value: " << gConfInfo.memoryPoolNumPages << std::endl;
			numberOfTicketsStore[subRob] = gConfInfo.memoryPoolNumPages;
		}
	}

	((RobinNPReadoutModule **)ReadoutModule)[0]->initTicketing((numberOfTicketsStore[0] + numberOfTicketsStore[1])*numRolsPerSubRob);

	timeWaited = 0.0;
	ts_clock(&tsRef);
	while (timeWaited < initialWaitingTime)
	{
		ts_clock(&tStopDelay);
		//std::cout << "ts_clock(&tStopDelay);" << std::endl;
		timeWaited = 1000000.0 * ts_duration(tsRef, tStopDelay);
	}

	for(u_int rolIndex = 0; rolIndex < gConfInfo.maxChannels; ++rolIndex){
		rolArray[rolIndex].idgroup.clear();
	}

	if (gConfInfo.externalDataGeneratorFlag == true)
	{
		gConfInfo.SendRemoteCommand();
	}

	//std::cout << "Press Any Key To Start Requests" << std::endl;
	//getchar();

	//Fill the message queue
	for (looprol = 0; looprol < nrols; looprol++)
	{
		if (rolArray[looprol].nevents2 > 0)
		{
			u_int numberOfTickets = numberOfTicketsStore[looprol >= numRolsPerSubRob];
			for (loop = 0; loop < numberOfTickets; loop++)
			{
				DEBUG("Initial queue fill request fragment %i\n",rolArray[looprol].nextl1id);
				rolArray[looprol].tickets[loop][0] = (*channels)[looprol]->requestFragment(rolArray[looprol].nextl1id);
				rolArray[looprol].tickets[loop][1] = rolArray[looprol].nextl1id;
				rolArray[looprol].nextLvl1IdNotRounded += stepInLvl1Ids;
				rolArray[looprol].nextl1id = (int) rolArray[looprol].nextLvl1IdNotRounded;
			}
		}
		rolArray[looprol].nextticket = 0;
	}

	rol = 0;

	reAskFlag = false;

	for(u_int rolIndex = 0; rolIndex < gConfInfo.maxChannels; ++rolIndex){
		rolArray[rolIndex].requeueCounter = 0;
	}

	// start loop over request fractions
	while ( (fabs(rateResult -  gConfInfo.targetDeleteRate) > 0.1) && (stepSize > 0.05) )
	{
		maxRateNoReAsk = 0.0;
		maxRateReAsk = 0.0;

		ts_clock(&ts0);
		// here is the first starting point for the measurement of the length of the time interval
		// between this request and the next request
		for (looprol = 0; looprol < nrols; looprol++)
		{
			u_int numberOfTickets = numberOfTicketsStore[looprol >= numRolsPerSubRob];
			for (loop = 0; loop < numberOfTickets; loop++){
				ts_clock(&rolArray[looprol].tStartDelay[loop]);
			}
		}
		if (!gConfInfo.noClearsFlag){
			stepInLvl1Ids = 100.0 / percent;
		}
		else{
			stepInLvl1Ids = 0.0;
		}

		for(u_int rolIndex = 0; rolIndex < gConfInfo.maxChannels; ++rolIndex)
		{
			rolArray[rolIndex].nevents2 = numberOfEventsFirst * ((gConfInfo.linkMask & (1 << rolIndex)) >> rolIndex);
			rolArray[rolIndex].nevents = (int) ((float) rolArray[rolIndex].nevents2 * percent / 100);
		}

		do
		{
			bool continueLoop = false;
			for(u_int rolIndex = 0; rolIndex < gConfInfo.maxChannels; ++rolIndex)
			{
				if(rolArray[rolIndex].nevents != 0){
					continueLoop = true;
				}
			}

			while(continueLoop)
			{
				isempty = 1;
				while(isempty)  //Wait for a complete ROB fragment
				{
					if (reAskFlag == false)
					{
						u_int maxEvents = rolArray[0].nevents;
						rol = 0;
						for(u_int rolIndex = 1; rolIndex < gConfInfo.maxChannels; ++rolIndex)
						{
							if(maxEvents < rolArray[rolIndex].nevents)
							{
								maxEvents = rolArray[rolIndex].nevents;
								rol = rolIndex;
							}
						}
					}
					else
					{
						bool rolSet = false;
						for (u_int rolIndex = 0; rolIndex < gConfInfo.maxChannels; ++rolIndex ) 
						{
							if ( gConfInfo.linkMask & (1 << rolIndex) ) 
							{
								if ( !rolSet ) 
								{
									rolSet = true;
									rol = rolIndex;
								} 
								else 
								{
									if (rolArray[rol].requeueCounter > rolArray[rolIndex].requeueCounter){
										rol = rolIndex;
									}
								}
							}
						}

					}

					fragment = dynamic_cast<ROBFragment *> ((*channels)[rol]->getFragment(rolArray[rol].tickets[rolArray[rol].nextticket][0]));
					if (fragment)
					{
						receivedFragSize = (u_int)fragment->size();       //Total fragment size
						if (gConfInfo.dataCheckFlag){
							checkData(fragment, rolArray[rol].tickets[rolArray[rol].nextticket][1], rol, gConfInfo.check_verbosity);
						}
						if (gConfInfo.dataDumpFlag){
							dumpData(fragment, rol);
						}

						if((fragment->status() & 0xf0000000) == 0)
						{
							// good fragment (with data)
							rolArray[rol].totalReceivedFragments++;
							rolArray[rol].lastReceivedLevel1Id = fragment->level1Id();
							rolArray[rol].idgroup.push_back(fragment->level1Id());
							delete fragment;
							if (rolArray[rol].nevents){
								rolArray[rol].nevents--;
							}
							if (!gConfInfo.noClearsFlag && (rolArray[rol].idgroup.size() == deletesPerGroup)){
								(*channels)[rol]->releaseFragment(&rolArray[rol].idgroup);
								rolArray[rol].idgroup.clear();
							}
							isempty = 0;
						}
						else
						{
							// fragment was not OK (no data)
							rolArray[rol].firstLoopReAskCount++;
							delete fragment;
							// requeue request: this is necessary, as the original request was not succesful, probably due to data not yet available, so we try again
							DEBUG("First Loop Re-Request Fragment %i\n",rolArray[rol].tickets[rolArray[rol].nextticket][1]);
							rolArray[rol].tickets[rolArray[rol].nextticket][0] = (*channels)[rol]->requestFragment(rolArray[rol].tickets[rolArray[rol].nextticket][1]);
							// increase delay if necessary
							rolArray[rol].waitingTime[rolArray[rol].nextticket] += waitingTimeIncrement;
							rolArray[rol].firstLoopReAskCountByRolTicket[rolArray[rol].nextticket]++;

							rolArray[rol].nextticket++;
							if (rolArray[rol].nextticket == numberOfTicketsStore[rol >= numRolsPerSubRob]){
								rolArray[rol].nextticket = 0;
							}
							reAskFlag = true;
							rolArray[rol].requeueCounter++;
						}
					}
				}

				int reql1id = rolArray[rol].nextl1id;
				int n = rolArray[rol].nextticket;
				// Wait if necessary, then queue a new L1ID
				ts_clock(&tStopDelay);
				timeWaited = 1000000.0 * ts_duration(rolArray[rol].tStartDelay[n], tStopDelay);
				while (timeWaited < rolArray[rol].waitingTime[n])
				{
					ts_clock(&tStopDelay);
					timeWaited = 1000000.0 * ts_duration(rolArray[rol].tStartDelay[n], tStopDelay);
				}
				DEBUG("First Loop Request Fragment %i,%i\n",reql1id,rolArray[rol].nextl1id);
				rolArray[rol].tickets[n][0] = (*channels)[rol]->requestFragment(reql1id);
				rolArray[rol].tickets[n][1] = reql1id;

				// here is the starting point for the  measurement of the length of the time interval between this request and the next request
				ts_clock(&rolArray[rol].tStartDelay[n]);
				rolArray[rol].tLastStartDelay = rolArray[rol].tStartDelay[n];

				rolArray[rol].nextLvl1IdNotRounded += stepInLvl1Ids;
				rolArray[rol].nextl1id = (int) rolArray[rol].nextLvl1IdNotRounded;
				rolArray[rol].nextticket++;

				if (rolArray[rol].nextticket == numberOfTicketsStore[rol >= numRolsPerSubRob]){
					rolArray[rol].nextticket = 0;
				}
				reAskFlag = false;

				continueLoop = false;
				for(u_int rolIndex = 0; rolIndex < gConfInfo.maxChannels; ++rolIndex)
				{
					if(rolArray[rolIndex].nevents != 0){
						continueLoop = true;
					}
				}
			}

			for(u_int rolIndex = 0; rolIndex < gConfInfo.maxChannels; ++rolIndex)
			{
				rolArray[rolIndex].nevents2 = numberOfEventsLeft * ((gConfInfo.linkMask & (1 << rolIndex)) >> rolIndex);
				rolArray[rolIndex].nevents = (int) ((float) rolArray[rolIndex].nevents2 * percent / 100);
			}

			// determine overall minimum time interval between successive requests
			{
				minWaitInterval = 0.0;
				int nActiveRols = 0;
				u_int ticketTotal = 0;
				for (u_int i = 0; i < nrols; i++)
				{
					if (rolArray[i].nevents2 > 0)
					{
						u_int numberOfTickets = numberOfTicketsStore[i >= numRolsPerSubRob];
						for (u_int j = 0; j < numberOfTickets; j++){
							minWaitInterval += rolArray[i].waitingTime[j];
						}
						nActiveRols++;
						ticketTotal+=numberOfTickets;
					}
				}
				minWaitInterval = reducFactor * minWaitInterval / ((float) (ticketTotal));
				// minWaitInterval = minWaitInterval / ((float) (numberOfTickets * nActiveRols));
				minWaitInterval = minWaitInterval / ((float) (ticketTotal*1.0)/nActiveRols);
			}
			reAskFlag = false;

			continueLoop = false;
			for(unsigned int rolIndex = 0; rolIndex < gConfInfo.maxChannels; ++rolIndex){
				rolArray[rolIndex].requeueCounter = 0;
				if(rolArray[rolIndex].nevents != 0){
					continueLoop = true;
				}
			}

			ts_clock(&ts1);

			while(continueLoop)
			{
				isempty = 1;
				while(isempty)  //Wait for a complete ROB fragment
				{
					if (reAskFlag == false)
					{
						u_int maxEvents = rolArray[0].nevents;
						rol = 0;
						for(u_int rolIndex = 1; rolIndex < gConfInfo.maxChannels; ++rolIndex)
						{
							if(maxEvents < rolArray[rolIndex].nevents)
							{
								maxEvents = rolArray[rolIndex].nevents;
								rol = rolIndex;
							}
						}
					}

					else
					{
						bool rolSet = false;
						for (u_int rolIndex = 0; rolIndex < gConfInfo.maxChannels; ++rolIndex ) 
						{
							if ( gConfInfo.linkMask & (1 << rolIndex) ) 
							{
								if ( !rolSet ) 
								{
									rolSet = true;
									rol = rolIndex;
								}
								else
								{
									if (rolArray[rol].requeueCounter > rolArray[rolIndex].requeueCounter){
										rol = rolIndex;
									}
								}
							}
						}

					}

					fragment = dynamic_cast<ROBFragment *> ((*channels)[rol]->getFragment(rolArray[rol].tickets[rolArray[rol].nextticket][0]));
					if (fragment)
					{ // good fragment (with data)
						if (gConfInfo.dataCheckFlag){
							checkData(fragment, rolArray[rol].tickets[rolArray[rol].nextticket][1], rol, gConfInfo.check_verbosity);
						}
						if (gConfInfo.dataDumpFlag){
							dumpData(fragment, rol);
						}

						if((fragment->status() & 0xf0000000) == 0)
						{
							rolArray[rol].totalReceivedFragments++;
							rolArray[rol].lastReceivedLevel1Id = fragment->level1Id();
							rolArray[rol].idgroup.push_back(fragment->level1Id());
							DEBUG("Second Loop Got Fragment %i\n",rolArray[rol].lastReceivedLevel1Id);
							delete fragment;
							if (rolArray[rol].nevents){
								rolArray[rol].nevents--;
							}

							if (!gConfInfo.noClearsFlag && (rolArray[rol].idgroup.size() == deletesPerGroup)){
								(*channels)[rol]->releaseFragment(&rolArray[rol].idgroup);
								rolArray[rol].idgroup.clear();
							}
							isempty = 0;
						}
						else
						{
							delete fragment;
							// requeue request: this is necessary, as the original request was not succesful, probably due to data not yet available, so we try again
							DEBUG("Second Loop Re-Request Fragment %i\n",rolArray[rol].tickets[rolArray[rol].nextticket][1]);
							rolArray[rol].tickets[rolArray[rol].nextticket][0] = (*channels)[rol]->requestFragment(rolArray[rol].tickets[rolArray[rol].nextticket][1]);
							rolArray[rol].secondLoopReAskCountByRolTicket[rolArray[rol].nextticket]++;
							rolArray[rol].nextticket++;
							if (rolArray[rol].nextticket == numberOfTicketsStore[rol >= numRolsPerSubRob]){
								rolArray[rol].nextticket = 0;
							}

							rolArray[rol].requeueCounter++;
							reAskFlag = true;
						}
					}
				}

				int reql1id = rolArray[rol].nextl1id;
				int n = rolArray[rol].nextticket;
				// Wait if necessary, then queue a new L1ID
				ts_clock(&tStopDelay);
				timeWaited = 1000000.0 * ts_duration(rolArray[rol].tLastStartDelay, tStopDelay);
				while (timeWaited < minWaitInterval)
				{
					ts_clock(&tStopDelay);
					timeWaited = 1000000.0 * ts_duration(rolArray[rol].tLastStartDelay, tStopDelay);
				}
				DEBUG("Second Loop Request Fragment %i\n",reql1id);
				rolArray[rol].tickets[n][0] = (*channels)[rol]->requestFragment(reql1id);
				rolArray[rol].tickets[n][1] = reql1id;
				// here is the starting point for the  measurement of the length of the time interval between this request and the next request
				ts_clock(&rolArray[rol].tLastStartDelay);
				rolArray[rol].nextLvl1IdNotRounded += stepInLvl1Ids;
				rolArray[rol].nextl1id = (int) rolArray[rol].nextLvl1IdNotRounded;
				// rolArray[rol].nextl1id++;
				rolArray[rol].nextticket++;
				if (rolArray[rol].nextticket == numberOfTicketsStore[rol >= numRolsPerSubRob]){
					rolArray[rol].nextticket = 0;
				}

				reAskFlag = false;

				continueLoop = false;
				for(u_int rolIndex = 0; rolIndex < gConfInfo.maxChannels; ++rolIndex)
				{
					if(rolArray[rolIndex].nevents != 0){
						continueLoop = true;
					}
				}
			}

			ts_clock(&ts2);
			timingResult = 1000000.0 * ts_duration(ts1, ts2);

			maxNumberOfReasks1 = 0;
			maxNumberOfReasks2 = 0;
			for (looprol = 0; looprol < nrols; looprol++)
			{
				u_int n;

				if (rolArray[looprol].nevents2 > 0)
				{
					u_int numberOfTickets = numberOfTicketsStore[looprol >= numRolsPerSubRob];
					for (n=0;n<numberOfTickets;n++)
					{
						if (rolArray[looprol].firstLoopReAskCountByRolTicket[n] > maxNumberOfReasks1){
							maxNumberOfReasks1 = rolArray[looprol].firstLoopReAskCountByRolTicket[n];
						}
						if (rolArray[looprol].secondLoopReAskCountByRolTicket[n] > maxNumberOfReasks2){
							maxNumberOfReasks2 = rolArray[looprol].secondLoopReAskCountByRolTicket[n];
						}
					}
					if (gConfInfo.verboseFlag && gConfInfo.nonIntPerformMeasFlag)
					{
						std::cout << std::endl;
						std::cout << "ROL " << looprol << " : Number of getFragment with data                   = " << rolArray[looprol].totalReceivedFragments << std::endl;
						std::cout << "ROL " << looprol << " : Number of lost events                             = " << rolArray[looprol].firstLoopReAskCount << std::endl;
						std::cout << "ROL " << looprol << " : Number of getFragment without data during startup = ";
						for (n = 0; n < numberOfTickets; n++){
							std::cout << rolArray[looprol].firstLoopReAskCountByRolTicket[n] << " ";
						}

						std::cout << std::endl;
						std::cout << "ROL " << looprol << " : Number of getFragment without data during running = ";
						for (n = 0; n < numberOfTickets; n++){
							std::cout << rolArray[looprol].secondLoopReAskCountByRolTicket[n] << " ";
						}

						std::cout << std::endl;
						std::cout << "ROL " << looprol << " : Minimum time interval between successive requests for same ticket = ";
						for (n = 0; n < numberOfTickets; n++){
							std::cout << rolArray[looprol].waitingTime[n] << " ";
						}

						std::cout << " us" << std::endl << std::endl;
					}
				}
			}

			u_int totalEvents = 0;
			for(u_int rolIndex = 0; rolIndex < gConfInfo.maxChannels; ++rolIndex){
				totalEvents |= rolArray[rolIndex].nevents2;
			}

			if (gConfInfo.verboseFlag && gConfInfo.nonIntPerformMeasFlag)
			{
				std::cout << "Minimum time interval between successive requests = " << minWaitInterval / ( (float) nrolsUsed)  << " us" << std::endl;
				std::cout << "Minimum time per event = " << percent * minWaitInterval / 100.0 << " us" << std::endl << std::endl;
				std::cout << "Measuring time     =  " << timingResult << " us" << std::endl;
				std::cout << "Number of events   = " << (totalEvents) << std::endl;
				for(u_int rolIndex = 0; rolIndex < gConfInfo.maxChannels; ++rolIndex)
				{
					if ((gConfInfo.linkMask & (1 << rolIndex)) >> rolIndex){
						printf("rol %i lastClearedLevel1Id %d lastReceivedLevel1Id %d numberOfClearsQueued %d\n", rolIndex, rolArray[rolIndex].lastClearedLevel1Id, rolArray[rolIndex].lastReceivedLevel1Id, rolArray[rolIndex].numberOfClearsQueued);
					}
				}
			}

			timePerEvent = timingResult / ( (float) totalEvents );
			rateResult = 1000.0/timePerEvent;
			if (!gConfInfo.noClearsFlag){
				rateL1 = rateResult;
			}
			else{
				rateL1 = 0.0;
			}

			if (gConfInfo.verboseFlag && gConfInfo.nonIntPerformMeasFlag)
			{
				printf ("Time per event       = %f us\n\n", timePerEvent);
				printf ("LVL1 rate            = %f kHz\n", rateL1);
				printf ("request rate         = %f kHz\n", ((float) nrolsUsed) * percent *10.0/timePerEvent);
				printf ("request fraction     = %f%c\n", percent, '%');
				printf ("fragment size        = %d words\n",  gConfInfo.numberOfWordsPerFragment);
				printf ("min. time per event / measured time = %f\n", (percent * minWaitInterval) / (100.0 * timePerEvent));
				printf ("max # of reasks 1    = %d\n", maxNumberOfReasks1);
				printf ("# of requests 1      = %d\n", (int) (percent * (float) numberOfEventsFirst /100.0));
				printf ("max # of reasks 2    = %d\n", maxNumberOfReasks2);
				printf ("# of requests 2      = %d\n", (int) (percent * (float) rolArray[0].nevents2 / 100.0));
				printf ("reducFactor          = %f\n", reducFactor);
				printf ("min. time per event  = %f us\n", percent * minWaitInterval / 100.0);
				printf ("size of delete block = %d\n", gConfInfo.deleteGroupSize);
				printf ("link mask            = %d\n", gConfInfo.linkMask);
				std::cout << std::endl << "Compact representation of results (for use in Excel):" << std::endl;
				printf ("LVL1 rate (kHz), request rate (kHz), request fraction (%c), fragment size (words),\n", '%');
				printf ("min. time per event / measured time ,max # of reasks 1, # of requests 1,\n");
				printf ("max # of reasks 2, # of requests 2, reducFactor, min. time per event\n");
				printf ("size of delete block, link mask\n");
			}
			if (gConfInfo.nonIntPerformMeasFlag)
			{
				if (maxNumberOfReasks2 == 0)
				{
					printf ("noreasks   ");
					if (rateResult > maxRateNoReAsk)
					{
						maxRateNoReAsk = rateResult;
						maxRateL1NoReAsk = rateL1;
					}
				}
				else
				{
					printf ("withreasks ");
					if (rateResult > maxRateReAsk)
					{
						maxRateReAsk = rateResult;
						maxRateL1ReAsk = rateL1;
					}
				}


				printf("%7.3f %7.3f %5.3f %d %5.3f %7d %d %7d %d %6.4f %5.3f %d %d\n", rateL1, ((float) nrolsUsed) * percent *10.0/timePerEvent, percent, gConfInfo.numberOfWordsPerFragment,
						(percent * minWaitInterval) / (100.0 * timePerEvent), maxNumberOfReasks1, (int) (percent * (float) numberOfEventsFirst /100.0),
						maxNumberOfReasks2, (int) (percent * (float) rolArray[0].nevents2 / 100.0), reducFactor, percent * minWaitInterval / 100.0, gConfInfo.deleteGroupSize, gConfInfo.linkMask);

				timingResult = 1000000.0 * ts_duration(ts0, ts1);
				timePerEvent = timingResult / ( (float) numberOfEventsFirst);
				if (gConfInfo.verboseFlag == true)
				{
					std::cout << std::endl << "Startup time     =  " << timingResult << " us" << std::endl;
					std::cout << "Number of events = " << numberOfEventsFirst << std::endl;
					std::cout << "Time per event   =  " << timePerEvent << " us" << std::endl;
				}
			}

			if (firstCycle == true)
			{
				//printf("First Cycle\n");
				// now we start with minimum time intervals
				reducFactor = gConfInfo.initialValueReducFactor;
				reducFactorStep = gConfInfo.initialStepReducFactor;

				for(u_int subRob = 0; subRob < numSubRobs; ++subRob)
				{
					for (u_int i = 0; i < numberOfTicketsStore[subRob]; i++)
					{
						// waiting time is numberOfTickets * 5 us
						rolArray[subRob*numRolsPerSubRob].waitingTime[i] = 100.0 * ((float) numberOfTicketsStore[subRob] * 5.0) / percent;
						for(u_int rolIndex = subRob*numRolsPerSubRob + 1; rolIndex < numRolsPerSubRob; ++rolIndex){
							rolArray[rolIndex].waitingTime[i] = rolArray[subRob*numRolsPerSubRob].waitingTime[i];
						}
						waitingTimeIncrement = waitIncrement;
					}
				}

				firstCycle = false;
				if (maxNumberOfReasks2 == 0 || gConfInfo.oneCycleFlag == true){
					stopFlag = true;
				}
			}
			else
			{
				// get original value of minWaitInterval back;
				minWaitInterval = minWaitInterval / reducFactor;
				startupModFactor = 1.0;

				// strategy: first get in startup phase reasks below 50, then try in measurement phase to get reasks to 0
				if (maxNumberOfReasks1 > 0.0001 * totalEvents ){
					startupModFactor = 1.0 + reducFactorStep;
				}
				else
				{
					if (maxNumberOfReasks2 == 0)
					{
						if (firstFlag == false)
						{
							if (increaseReducFactorFlag == true){
								stopFlag = true;
							}
						}
						else{
							increaseReducFactorFlag = false;
						}
					}
					else
					{
						// maxNumberOfReasks2 != 0
						if (firstFlag == false)
						{
							if (increaseReducFactorFlag == false){  // return to maxNumberOfReasks2 == 0
								increaseReducFactorFlag = true;
							}
						}
						else{
							increaseReducFactorFlag = true;
						}
					}
					if (increaseReducFactorFlag == true){
						reducFactor = reducFactor + reducFactorStep;
					}
					else{
						reducFactor = reducFactor - reducFactorStep;
					}
					if (firstFlag == true){
						firstFlag = false;
					}
				}
				printf (">>>> new startupModFactor %f, reducFactor %f, step %f\n", startupModFactor, reducFactor, reducFactorStep);

				for(u_int subRob = 0; subRob < numSubRobs; ++subRob)
				{
					for (u_int i = 0; i < numberOfTicketsStore[subRob]; i++)
					{
						rolArray[subRob*numRolsPerSubRob].waitingTime[i] = minWaitInterval * (float) (numberOfTicketsStore[subRob]) * startupModFactor;
						for(u_int rolIndex = subRob*numRolsPerSubRob + 1; rolIndex < numRolsPerSubRob; ++rolIndex){
							rolArray[rolIndex].waitingTime[i] = rolArray[subRob*numRolsPerSubRob].waitingTime[i];
						}
					}
				}
			}

			//printf("Resetting event Arrays\n");

			for (looprol = 0; looprol < nrols; looprol++)
			{
				rolArray[looprol].firstLoopReAskCount = 0;
				rolArray[looprol].totalReceivedFragments = 0;

				u_int numberOfTickets = numberOfTicketsStore[looprol >= numRolsPerSubRob];
				for (u_int i = 0; i < numberOfTickets; i++)
				{
					rolArray[looprol].firstLoopReAskCountByRolTicket[i] = 0;
					rolArray[looprol].secondLoopReAskCountByRolTicket[i] = 0;
				}
			}

			for(u_int rolIndex = 0; rolIndex < gConfInfo.maxChannels; ++rolIndex)
			{
				rolArray[rolIndex].nevents2 = numberOfEventsFirst * ((gConfInfo.linkMask & (1 << rolIndex)) >> rolIndex);
				rolArray[rolIndex].nevents = (int) ((float) rolArray[rolIndex].nevents2 * percent / 100); //reading out 100% to 500k here
			}

			//printf("Checking output, percent = %f, nevents[0] = %i\n",percent,nevents[0]);
			reAskFlag = false;

			for(u_int rolIndex = 0; rolIndex < gConfInfo.maxChannels; ++rolIndex){
				rolArray[rolIndex].requeueCounter = 0;
			}

			ts_clock(&ts0);
		}
		while ((stopFlag == false) && (reducFactor < 1.1));

		gConfInfo.requestFraction = percent;
		if (rateResult > gConfInfo.targetDeleteRate)
		{
			if (downCount > 0)
			{
				stepSize = stepSize / 2.0;
				downCount = 0;
			}
			percent = percent + stepSize;
			upCount++;
			//printf("Rate Higher than target delete rate, percent = %f, stepsize = %f\n",rateResult,stepSize);
		}
		else
		{
			if (upCount > 0)
			{
				stepSize = stepSize / 2.0;
				upCount = 0;
			}
			percent = percent - stepSize;
			downCount++;
			//printf("Rate less than or equal to than target delete rate, percent = %f, stepsize = %f\n",rateResult,stepSize);
		}
		if (!gConfInfo.iterateFlag){
			break;
		}
		else
		{
			printf("Iteration Selected - Continuing\n");
			// next iteration step, reinit flags and timeintervals
			firstCycle = true;
			stopFlag = false;
			firstFlag = true;

			for (looprol = 0; looprol < nrols; looprol++)
			{
				rolArray[looprol].firstLoopReAskCount = 0;
				rolArray[looprol].totalReceivedFragments = 0;

				u_int numberOfTickets = numberOfTicketsStore[looprol >= numRolsPerSubRob];
				for (u_int i = 0; i < numberOfTickets; i++)
				{
					rolArray[looprol].firstLoopReAskCountByRolTicket[i] = 0;
					rolArray[looprol].secondLoopReAskCountByRolTicket[i] = 0;
					rolArray[looprol].waitingTime[i] = 0.0;
				}
			}
		}
	}

	//23 = 14 from DG header + 9 from main message header - should the 14 now be part of the 50? i.e. only add 9?
	//the fact we get a header of 22 (minus one since no crc) suggests we are still getting the DG header externally

	fragSize = gConfInfo.numberOfWordsPerFragment + (gConfInfo.externalDataGeneratorFlag ? 10 : 23);
	if (receivedFragSize != fragSize){
		printf("Size received fragments: %d words != expected size = %d words\n", receivedFragSize, fragSize);
	}
	measuredBandWidthNoReAsk = ((float) nrolsUsed) * gConfInfo.requestFraction * rateResult * 4.0 * ((float) receivedFragSize)/ 100000.0;
	printf("Next line: LVL1 (kHz), req. rate (kHz), req. fraction (%c), fragment size (words), size+hdrs/trailer (words), bw (MByte/s)\n",'%');
	printf("Final   %7.3f %7.3f %5.3f %d %d %5.3f\n",
			rateL1, ((float) nrolsUsed) * gConfInfo.requestFraction * rateResult / 100.0, gConfInfo.requestFraction,
			gConfInfo.numberOfWordsPerFragment, receivedFragSize, measuredBandWidthNoReAsk);
	/*FILE *Fp= fopen ( "results_alldata.csv","a" ) ;
	fprintf(Fp,"%7.3f,%7.3f,%5.3f,%d,%d,%5.3f,%5.3f\n",
			rateL1, ((float) nrolsUsed) * gConfInfo.requestFraction * rateResult / 100.0, gConfInfo.requestFraction,
			gConfInfo.numberOfWordsPerFragment, receivedFragSize, measuredBandWidthNoReAsk, timePerEvent);*/
	if (gConfInfo.nonIntPerformMeasFlag)
	{
		if (gConfInfo.verboseFlag ){
			printf ("\nLVL1 rate (kHz), request rate (kHz), request fraction (%c), fragment size (words), size+hdrs/trailer (words), bandwidth (MByte/s)\n",'%');
		}
		measuredBandWidthNoReAsk = ((float) nrolsUsed) * gConfInfo.requestFraction * maxRateNoReAsk * 4.0 * ((float) receivedFragSize)/ 100000.0;
		printf ("maxnoreasks   %7.3f %7.3f %5.3f %d %d %5.3f\n", maxRateL1NoReAsk, ((float) nrolsUsed) * gConfInfo.requestFraction * maxRateNoReAsk / 100.0, gConfInfo.requestFraction, gConfInfo.numberOfWordsPerFragment, receivedFragSize, measuredBandWidthNoReAsk);
		measuredBandWidthReAsk = ((float) nrolsUsed) * gConfInfo.requestFraction * maxRateReAsk * 4.0 * ((float) receivedFragSize)/ 100000.0;
		printf ("maxwithreasks %7.3f %7.3f %5.3f %d %d %5.3f\n", maxRateL1ReAsk, ((float) nrolsUsed) * gConfInfo.requestFraction * maxRateReAsk / 100.0, gConfInfo.requestFraction, gConfInfo.numberOfWordsPerFragment, receivedFragSize, measuredBandWidthReAsk);
		if (maxRateNoReAsk > maxRateReAsk){
			printf ("maxall        %7.3f %7.3f %5.3f %d %d %5.3f\n", maxRateL1NoReAsk, ((float) nrolsUsed) * gConfInfo.requestFraction * maxRateNoReAsk / 100.0, gConfInfo.requestFraction, gConfInfo.numberOfWordsPerFragment, receivedFragSize, measuredBandWidthNoReAsk);
		}
		else{
			printf ("maxall        %7.3f %7.3f %5.3f %d %d %5.3f\n", maxRateL1ReAsk, ((float) nrolsUsed) * gConfInfo.requestFraction * maxRateReAsk / 100.0, gConfInfo.requestFraction, gConfInfo.numberOfWordsPerFragment, receivedFragSize, measuredBandWidthReAsk);
		}
	}
}


/**********************************/
void sigquit_handler(int /*signum*/)
/**********************************/
{
	getstat = 1;
}


/**********************/
void documentation(void)
/**********************/
{
	std::string docString[] =
	{"Documentation for performance measurements:",
			"",
			"A performance measurement is done with either the internal data generator or an",
			"external S-link source. In the latter case the dolar-slink_src program has to",
			"be controlled from the computer from which test_Robin is run. This is done with",
			"the help of ssh, which invokes a script setting up the appropriate environment,",
			"followed by starting the dolar_slink_src program with appropriate parameters. ",
			"For example, the script (with name dolar) could contain:",
			"",
			"#!/bin/tcsh",
			"setenv CMTCONFIG i686-slc4-gcc34-opt",
			"source /project/atlas/opt/tdaq/tdaq/tdaq-01-08-03/installed/setup.csh",
			"dolar_slink_src -c 1110 -s $1 -w 4  >>&! dolarout.txt &",
			"",
			"which is then started (assuming dolar is found in the home directory) using:",
			"",
			"ssh machine -x dolar fragsize",
			"",
			"where \"fragsize\" is used as argument of the -s parameter of the dolar_slink_src",
			"program, \"machine\" is the name of the computer with the DOLAR card(s).",
			"",
			"NB: use -w 10 for the PCIe ROBIN.",
			"",
			"The script name can be specified as parameter using the -a option.",
			"In that case fragsize does not need to be specified, it is taken from the",
			"-w command line parameter.",
			"",
			"At the end of the execution of test_Robin the dolar_slink_src program on the",
			"remote computer has to be stopped. This is again done with an appropriate",
			"script, which contains (if the name of the script is killdolar)",
			"",
			"foreach name ( `ps aux | grep \"dolar\" | grep -v grep | grep -v killdolar | awk '{print $2}'` )",
			"kill -TERM $name",
			"",
			"Running it is done with:",
			"",
			"ssh machine  -x killdolar",
			"",
			"or by supplying the name of the script as argument of the -z parameter.",
			"",
			"For each measurement there are two phases, a setup phase followed by a measuring",
			"phase. For the first setup and measuring phase the number of requests for which",
			"no fragment is returned (i.e. that have to be re-issued) are counted. The program",
			"finishes if for all requests during the measuring phase a freagment was returned,",
			"otherwise the program continues with a new setup and measuring phase.",
			"During the setup phase requests for which no fragment was returned give rise to an",
			"increase in the minimum time between successive requests for a ROL and for a ",
			"certain \"ticket\" (= outstanding request). There are at max. 3 ROLs and 4",
			"tickets per ROL, so 12 delays are adjusted. After the setup phase the average",
			"of the delays is computed, multiplied by a certain factor (the \"reduction ",
			"factor\") and with these delays a measurement of the fragment input rate per ROL",
			"(the \"LVL1 rate\") is done. After the first setup and measuring phase a second",
			"cycle is started only if there were requests not returning a fragment during",
			"the measurement phase. In that case first the minimum time interval between",
			"successive requests during the setup phase is set by adjusting it until",
			"there are less than 0.001*nevents reasks. Next the reduction",
			"factor is adjusted upward until in a next cycle no reasks are necessary. The",
			"factor is reduced if no reasks have been seen until reasks are necessary and",
			"then it is increased one step. Then the process stops and the LVL1",
			"rate measured is the outcome of the measurement.",
			"",
			"Example usage: test_Robin -p -r 10 -v : here a request fraction of 10% and",
			"verbose output is selected, the internal data generator is used.",
			""};

	for (int i = 0; i < 62; i++){
		std::cout << docString[i] << std::endl;
	}
}



/**************/
void usage(void)
/**************/
{
	std::cout << "Option of original version: " << std::endl;
	std::cout << "-t: Run in test mode. I.e. do not disable the ROLs after the test " << std::endl;
	std::cout << "Options for non-interactive performance measurements: " << std::endl;
	printf("-a  name of script for running dolar_slink_src [%s]\n",  gConfInfo.dolarScriptName);
	printf("-b  name of computer with DOLAR card [%s] \n", gConfInfo.dolarComputerName);
	printf("-d: documentation\n");
	printf("-e: external data source\n");
	printf("-f  fraction of events used for setup phase [%f]\n", gConfInfo.setupNeventFraction);
	printf("-h: this screen\n");
	printf("-i: check data and print the size of each fragment\n");
	printf("-j: non-interactive dump of data\n");
	printf("-k  size of delete group [%d]\n", gConfInfo.deleteGroupSize);
	printf("-n  number of LVL1 accepts per cycle [%d] \n", gConfInfo.numberOfLVL1AcceptsPerCycle);
	printf("-m  link mask [%d]\n", gConfInfo.linkMask);
	printf("-o: one cycle only\n");
	printf("-p: non-interactive perf. measurement\n");
	printf("-q  initial value of reduction factor [%f]\n", gConfInfo.initialValueReducFactor);
	printf("-r  request fraction in percent [%f]\n", gConfInfo.requestFraction);
	printf("-s  step size of reduction factor [%f]\n", gConfInfo.initialStepReducFactor);
	printf("-v: verbose output\n");
	printf("-w  words per fragment [%d]\n", gConfInfo.numberOfWordsPerFragment);
	printf("-x  ROBIN page size [%d]\n", gConfInfo.pageSize);
	printf("-y  ROBIN number of pages [%d]\n", gConfInfo.numPages);
	printf("-z  name of script for killing dolar_slink_src [%s] \n", gConfInfo.nameOfKillScript);
	printf("-P  PCI slot number [%d]\n", gConfInfo.pciSlotNumber);
	printf("-F  ROBIN FPGA firmware version [0x%x] \n", gConfInfo.firmwareVersion);
	printf("-T  Target delete rate [%f kHz]\n", gConfInfo.targetDeleteRate);
	printf("-V  Data check mode. 0=no check, 1=full check, 2=stop after first error. [%d]\n", gConfInfo.check_verbosity);
	printf("-D: Do not send clears, use the  same Id (0) for all requests\n");
	printf("-M  MaxRXPages: maximum number of pages per fragment [%d]\n", gConfInfo.maxRXPages);
	printf("-N  MemoryPoolNumPages: maximum number of pages in ROS memory pool [%d]\n", gConfInfo.memoryPoolNumPages);
	printf("-O  MemoryPoolPageSize: size of page in ROS memory pool [%d]\n", gConfInfo.memoryPoolPageSize);
	printf("-H  enable debug printout if compiled with -dbg configuration [%d]\n", gConfInfo.debugPrintFlag);
	printf("-A  set core affinity for main process [%d]\n", gConfInfo.debugPrintFlag);
}

/*****************************/
int main(int argc, char **argv)
/*****************************/
{
	int c, testmode = 0, defparams, count, num, value, il1id, l1id, option, quit_flag = 0;
	u_int lastticket = 0, dataChannel;
	std::vector<DataChannel *> *channels;
	std::vector <u_int> idgroup[12];
	struct sigaction sa;
	ReadoutModule* ReadoutModule[FM_POOL_NUMB] = {0};
	ROBFragment* robfragment;
	DFCountedPointer<Config> configuration = Config::New();
	char *theEnd;
	int procAffinity = 0;
	daq::rc::TransitionCmd cmd(daq::rc::FSM_COMMAND::INIT_FSM); // Dummy run control transition command not used by any RobinNPReadoutModule methods
	while ((c = getopt(argc, argv, "ta:b:c:def:hijk:n:m:ops:q:r:vw:x:y:z:P:F:T:V:DEM:N:O:HA:")) != -1)
	{
		if (testmode == 1)
		{
			std::cout << "-t not allowed together with other options" << std::endl;
			exit(0);
		}
		switch (c)
		{
		case 'a': gConfInfo.SetRemoteStartCmd(optarg); break;
		case 'b': gConfInfo.SetRemoteComputer(optarg); break;
		case 'd': documentation(); exit(0); break;
		case 'e': gConfInfo.externalDataGeneratorFlag = true; break;
		case 'f': gConfInfo.setupNeventFraction = strtod(optarg, &theEnd); break;
		case 'h': usage(); exit(0); break;
		case 'i': gConfInfo.dataCheckFlag = true; break;
		case 'j': gConfInfo.dataDumpFlag = true; break;
		case 'k': gConfInfo.deleteGroupSize = strtol(optarg, &theEnd, 0); break;
		case 'm': gConfInfo.SetLinkMask(strtol(optarg, &theEnd, 0)); break;
		case 'n': gConfInfo.numberOfLVL1AcceptsPerCycle = strtol(optarg, &theEnd, 0); break;
		case 'o': gConfInfo.oneCycleFlag = true; break;
		case 'p': gConfInfo.nonIntPerformMeasFlag = true; break;
		case 's': gConfInfo.initialStepReducFactor = strtod(optarg, &theEnd); break;
		case 'q': gConfInfo.initialValueReducFactor = strtod(optarg, &theEnd); break;
		case 'r': gConfInfo.requestFraction = strtod(optarg, &theEnd); break;
		case 't': testmode = 1; break;
		case 'v': gConfInfo.verboseFlag = true; break;
		case 'w': gConfInfo.numberOfWordsPerFragment = strtol(optarg, &theEnd, 0); break;
		case 'x': gConfInfo.pageSize = strtol(optarg, &theEnd, 0); break;
		case 'y': gConfInfo.numPages = strtol(optarg, &theEnd, 0); break;
		case 'z': gConfInfo.SetRemoteKillCmd(optarg); break;
		case 'P': gConfInfo.pciSlotNumber = strtol(optarg, &theEnd, 0); break;
		case 'F': gConfInfo.firmwareVersion = strtol(optarg, &theEnd, 0); break;
		case 'T': gConfInfo.targetDeleteRate = strtod(optarg, &theEnd); gConfInfo.iterateFlag=true;break;
		case 'V': gConfInfo.check_verbosity = strtol(optarg, &theEnd, 0); break;
		case 'D': gConfInfo.noClearsFlag = true; break;
		case 'M': gConfInfo.maxRXPages = strtol(optarg, &theEnd, 0); break;
		case 'N': gConfInfo.memoryPoolNumPages = strtol(optarg, &theEnd, 0); break;
		case 'O': gConfInfo.memoryPoolPageSize = strtol(optarg, &theEnd, 0); break;
		case 'H': gConfInfo.debugPrintFlag = true; break;
		case 'A': procAffinity = strtol(optarg, &theEnd, 0); break;
		default:
			std::cout << "Invalid option " << c << std::endl;
			std::cout << "Usage: " << argv[0] << " [options]: " << std::endl;
			usage();
			exit (0);
		}
	}

	ts_open(1, TS_DUMMY);

	TS_OPEN(100000, TS_H2);	// for Robin S/W profiling
	TS_START(TS_H2);
	TS_RECORD(TS_H2,1);

	// Install signal handler for SIGQUIT
	sigemptyset(&sa.sa_mask);
	sa.sa_flags   = 0;
	sa.sa_handler = sigquit_handler;

	// Dont block in intercept handler
	if (sigaction(SIGQUIT, &sa, NULL) < 0)
	{
		std::cout << "main: sigaction() FAILED with ";
		int code = errno;
		if (code == EFAULT) std::cout << "EFAULT" << std::endl;
		if (code == EINVAL) std::cout << "EINVAL" << std::endl;
		exit (-1);
	}

	try
	{
		IPCCore::init(argc, argv);
	}
	catch(daq::ipc::Exception & ex)
	{
		ers::fatal(ex);
	}

	try
	{
		//compute CRC table
		{
			const u_long crchighbit = (u_long) (1 << 15);
			const u_long polynom =  0x1021;
			for (int i = 0; i < 65536; i++)
			{
				u_long crc = i;

				for (int j = 0; j < 16; j++)
				{
					u_long bit = crc & crchighbit;
					crc <<= 1;
					if (bit){
						crc ^= polynom;
					}
				}
				crctab16[i] = crc & 0xffff;
			}
		}

		if(gConfInfo.debugPrintFlag)
		{
			std::cout << "DFDebug Print Mode Selected - Ensure you have compiled with -dbg configuration to see debug messages" << std::endl;
			DF::GlobalDebugSettings::setup(20, 1);
		}

		if (gConfInfo.externalDataGeneratorFlag == true)
		{
			gConfInfo.SendRemoteKill();
		}

		if (gConfInfo.nonIntPerformMeasFlag  || gConfInfo.dataCheckFlag || gConfInfo.dataDumpFlag)
		{
			gConfInfo.SetParams(configuration);
			if (ReadoutModule[0] != 0)
			{
				std::cout << "Array element 0 has already been filled"  << std::endl;
				return 1;
			}
			if (gConfInfo.verboseFlag == false && gConfInfo.dataCheckFlag == false && gConfInfo.dataDumpFlag == false) freopen ("/dev/null", "a+", stdout);

			ReadoutModule[0] = new RobinNPReadoutModule();
			ReadoutModule[0]->setup(configuration);
			ReadoutModule[0]->configure(cmd);
			ReadoutModule[0]->prepareForRun(cmd);

			if (gConfInfo.verboseFlag == false && gConfInfo.dataCheckFlag == false && gConfInfo.dataDumpFlag == false)
			{
				int fd;

				fclose(stdout);
				// found this at: http://www.thescripts.com/forum/thread222840.html
				fd = open("/dev/tty", O_WRONLY);
				if(fd != -1){
					stdout = fdopen(fd, "w");
				}
				else{
					std::cout << "Failed to open /dev/tty" << std::endl;
				}
			}

			DoPerformanceMeasurement(ReadoutModule, procAffinity);

			ReadoutModule[0]->stopGathering(cmd);
			ReadoutModule[0]->unconfigure(cmd);
			delete ReadoutModule[0];
			ReadoutModule[0] = 0;

			if (gConfInfo.externalDataGeneratorFlag == true)
			{
				gConfInfo.SendRemoteKill();
			}
		}
		else
		{
			do
			{
				std::cout << std::dec << std::endl << std::endl;
				std::cout << " Automatic tests:  " << std::endl;
				std::cout << " Run synchronous benchmark (request all)     : " << TIMING4 << std::endl;
				std::cout << " Run synchronous benchmark (request some)    : " << TIMING5 << std::endl;
				std::cout << " Run synchronous benchmark (up to 12 ROLs)   : " << TIMING6 << std::endl;
				std::cout << " Run integrated release test (up to 12 ROLs) : " << TIMING7 << std::endl;
				std::cout << "================================================" << std::endl;
				std::cout << " Create a RobinNPReadoutModule               : " << CREATE << std::endl;
				std::cout << " Configure a RobinNPReadoutModule            : " << CONFIGURE << std::endl;
				std::cout << " Start a RobinNPReadoutModule                : " << START << std::endl;
				std::cout << " Request a ROB fragment                      : " << REQUEST << std::endl;
				std::cout << " Get a ROB fragment                          : " << GET << std::endl;
				std::cout << " Release many ROB fragments                  : " << RELEASEMANY << std::endl;
				std::cout << " Stop a RobinNPReadoutModule                 : " << STOP << std::endl;
				std::cout << " Unconfigure a Readout Module                : " << UNCONFIGURE << std::endl;
				std::cout << " Delete a Readout Module                     : " << DELETE << std::endl;
				std::cout << " Request many Fragments and dump             : " << DUMPMANY << std::endl;
				std::cout << " Get ROBINNP statistics (getISInfo)          : " << GETINFO << std::endl;
				std::cout << " Set a configuration variable                : " << SETCONFIG << std::endl;
				std::cout << " Reset ROBIN                                 : " << RESET << std::endl;
				std::cout << " Set debug parameters                        : " << SETDEBUG << std::endl;
				std::cout << " HELP                                        : " << HELP << std::endl;
				std::cout << " QUIT                                        : " << QUIT << std::endl;
				std::cout << " ? : ";

				option = getdec();
				switch(option)
				{
				case CREATE:
					std::cout << "Enter the array index (0.." << FM_POOL_NUMB - 1 << ")" << std::endl;
					num = getdecd(0);

					if (ReadoutModule[num] != 0)
					{
						std::cout << "This array element has already been filled"  << std::endl;
						break;
					}
					ReadoutModule[num] = new RobinNPReadoutModule();
					std::cout << "Readout Module created" << std::endl;
					break;

				case CONFIGURE:
					std::cout << "Enter the array index (0.." << FM_POOL_NUMB-1 << ")" << std::endl;
					num = getdecd(0);

					if (ReadoutModule[num] == 0)
					{
						std::cout << "This array element has not yet been filled"  << std::endl;
						break;
					}

					getparams(configuration, 0);
					ReadoutModule[num]->setup(configuration);
					ReadoutModule[num]->configure(cmd);
					break;

				case START:
					std::cout << "Enter the array index (0.." << FM_POOL_NUMB - 1 << ")" << std::endl;
					num = getdecd(0);

					if (ReadoutModule[num] == 0)
					{
						std::cout << "This array element has not yet been filled"  << std::endl;
						break;
					}

					ReadoutModule[num]->prepareForRun(cmd);

					if (gConfInfo.externalDataGeneratorFlag == true)
					{
						gConfInfo.SendRemoteCommand();
					}

					break;


				case REQUEST:
					std::cout << "Enter the array index (0.." << FM_POOL_NUMB - 1 << ")" << std::endl;
					num = getdecd(0);

					if (ReadoutModule[num] == 0)
					{
						std::cout << "This array element has not yet been filled"  << std::endl;
						break;
					}

					std::cout << "Enter ROL (0 - 11)" << std::endl;
					dataChannel = getdecd(0);
					if (dataChannel > 11)
					{
						std::cout << "Wrong channel number." << std::endl;
						break;
					}

					std::cout << "Enter the Level 1 ID" << std::endl;
					l1id = gethexd(1);

					channels = const_cast<std::vector<DataChannel *> *>(ReadoutModule[num]->channels());

					value = (*channels)[dataChannel]->requestFragment(l1id);
					std::cout << "Request returns the ticket " << value << std::endl;
					lastticket = value;
					break;

				case GET:
					std::cout << "Enter the array index (0.." << FM_POOL_NUMB - 1 << ")" << std::endl;
					num = getdecd(0);

					if (ReadoutModule[num] == 0)
					{
						std::cout << "This array element has not yet been filled"  << std::endl;
						break;
					}

					std::cout << "Enter ROL (0 - 11)" << std::endl;
					dataChannel = getdecd(0);
					if (dataChannel > 11)
					{
						std::cout << "Wrong channel number." << std::endl;
						break;
					}

					std::cout << "Enter the ticket received from request()" << std::endl;
					l1id = getdecd(lastticket);

					channels = const_cast<std::vector<DataChannel *> *>(ReadoutModule[num]->channels());
					robfragment = dynamic_cast<ROBFragment *>((*channels)[dataChannel]->getFragment(l1id));
					if (robfragment == 0){ // may come or lost
						std::cout << " getFragment returns : " << robfragment << " i.e. fragment may come or is lost " << std::endl;
					}
					else
					{
						std::cout << "Dumping ROB fragment" << std::endl;
						std::cout << "ROB fragment pointer = " << std::hex << robfragment << std::dec << std::endl;
						std::cout << *robfragment;
						delete robfragment;
					}
					break;

				case RELEASEMANY:
				{
					std::cout << "Enter the array index (0.." << FM_POOL_NUMB - 1 << ")" << std::endl;
					num = getdecd(0);

					if (ReadoutModule[num] == 0)
					{
						std::cout << "This array element has not yet been filled"  << std::endl;
						break;
					}

					std::cout << "Enter ROL (0 - 11)" << std::endl;
					dataChannel = getdecd(0);
					if (dataChannel > 11)
					{
						std::cout << "Wrong channel number." << std::endl;
						break;
					}

					std::cout << "How many events do you want to release" << std::endl;
					value = getdecd(100);

					std::cout << "Enter the L1ID to start with" << std::endl;
					il1id = getdecd(0);

					idgroup[0].clear();

					for(int loop = il1id; loop < (il1id + value); loop++){
						idgroup[0].push_back(loop);
					}

					channels = const_cast<std::vector<DataChannel *> *>(ReadoutModule[num]->channels());
					(*channels)[dataChannel]->releaseFragment(&idgroup[0]);
				}
				break;

				case STOP:
					std::cout << "Enter the array index (0.." << FM_POOL_NUMB-1 << ")" << std::endl;
					num = getdecd(0);

					if (ReadoutModule[num] == 0)
					{
						std::cout << "This array element has not yet been filled"  << std::endl;
						break;
					}

					ReadoutModule[num]->stopGathering(cmd);
					break;

				case UNCONFIGURE:
					std::cout << "Enter the array index (0.." << FM_POOL_NUMB-1 << ")" << std::endl;
					num = getdecd(0);

					if (ReadoutModule[num] == 0)
					{
						std::cout << "This array element has not yet been filled"  << std::endl;
						break;
					}
					ReadoutModule[num]->unconfigure(cmd);
					break;

				case DELETE:
					std::cout << "Enter the array index (0.." << FM_POOL_NUMB-1 << ")" << std::endl;
					num = getdecd(0);

					if (ReadoutModule[num] == 0)
					{
						std::cout << "This array element has not yet been filled" << std::endl;
						break;
					}

					delete ReadoutModule[num];
					ReadoutModule[num] = 0;
					break;

				case TIMING4:
				{
					u_int nevents, nevents2, nextticket, nextl1id, tickets[4][2], isempty, loop, ddd = 0, datay = 0, datan = 0, ndel = 0;
					ROBFragment *fragment;
					tstamp ts1, ts2;
					float delta1, delta2;

					if (ReadoutModule[0] != 0)
					{
						std::cout << "Array element 0 has already been filled"  << std::endl;
						return 1;
					}
					ReadoutModule[0] = new RobinNPReadoutModule();

					std::cout << "Configure Robin (0) or use default parameters (1)" << std::endl;
					defparams = getdecd(1);

					if (defparams == 0){
						getparams(configuration, 1);
					}
					else{
						getdefparams(configuration);
					}
					dataChannel = 0;  // index into channels

					ReadoutModule[0]->setup(configuration);
					ReadoutModule[0]->configure(cmd);
					ReadoutModule[0]->prepareForRun(cmd);

					if (gConfInfo.externalDataGeneratorFlag == true)
					{
						gConfInfo.SendRemoteCommand();
					}

					std::cout << "Enable debug mode (0 = No  1 = Yes)" << std::endl;
					ddd = getdecd(ddd);

					std::cout << "How many events do you want to process " << std::endl;
					nevents = getdecd(1000);
					nevents2 = nevents;
					nextl1id = 0;
					getstat = 0;
					channels = const_cast< std::vector<DataChannel *> * >(ReadoutModule[0]->channels());
					std::cout << "Running. Press ctrl+\\ to get statistics" << std::endl;

					ts_clock(&ts1);
					//Fill the message Q
					for (loop = 0; loop < 4; loop++)
					{
						if (ddd){
							std::cout << "Posting request for L1ID " << nextl1id << std::endl;
						}
						tickets[loop][0] = (*channels)[dataChannel]->requestFragment(nextl1id);
						tickets[loop][1] = nextl1id;
						nextl1id++;
					}
					nextticket = 0;

					idgroup[0].clear();

					while(nevents)
					{
						//Wait for a complete ROB fragment
						isempty = 1;
						while(isempty)
						{
							if (getstat)
							{
								getstat = 0;
								std::cout << "Number of empty events             = " << datan << std::endl;
								std::cout << "Number of complete events          = " << datay << std::endl;
								std::cout << "Number of release(100) groups sent = " << ndel << std::endl;
								std::cout << "Currently waiting for L1IDs " << tickets[0][1] << ", " << tickets[1][1] << ", "  << tickets[2][1] << " and " << tickets[3][1] << std::endl;
							}

							if (ddd){
								std::cout << "waiting for L1ID " << tickets[nextticket][1] << std::endl;
							}
							fragment = dynamic_cast<ROBFragment *>((*channels)[dataChannel]->getFragment(tickets[nextticket][0]));

							if (fragment)
							{ // good fragment (with data)
								if (ddd){
									std::cout << "after getFragment: fragment status = " << std::hex << fragment->status() << std::dec << " L1id = " << fragment->level1Id() << std::endl;
								}
								if (ddd){
									std::cout << "level1Id = " << fragment->level1Id() <<std::endl;
								}

								delete fragment;
								idgroup[0].push_back(tickets[nextticket][1]);
								if (ddd){
									std::cout << "L1ID = " << tickets[nextticket][1] << " received and scheduled for release (datay = " << datay << ")" << std::endl;
								}
								isempty = 0;
								datay++;
							}
							else  //Requeue the L1ID: lost fragments are not handled correctly ...
							{
								if (ddd){
									std::cout << "Requeueing L1ID " <<  tickets[nextticket][1] << std::endl;
								}
								tickets[nextticket][0] = (*channels)[dataChannel]->requestFragment(tickets[nextticket][1]);
								nextticket++;
								if (nextticket == 4){
									nextticket = 0;
								}
								datan++;
							}
						}

						//Queue a new L1ID
						if (ddd){
							std::cout << "Requesting new L1ID = " << nextl1id << std::endl;
						}
						tickets[nextticket][0] = (*channels)[dataChannel]->requestFragment(nextl1id);
						tickets[nextticket][1] = nextl1id;
						nextl1id++;
						nextticket++;
						if (nextticket == 4){
							nextticket = 0;
						}
						//Send a delete request if we have received 100 events
						if ((datay % 100) == 0)
						{
							if (ddd){
								std::cout << "100 events released" << std::endl;
							}
							//(*channels)[dataChannel]->releaseFragment(&idgroup[0]);

							bool releaseOK;

							RobinNPDataChannel *npdc;
							npdc = (RobinNPDataChannel *) (*channels)[dataChannel];
							releaseOK = npdc->releaseFragmentAck(&idgroup[0]);
							if (releaseOK == false){
								std::cout << "ERROR on release" << std::endl;
							}





							idgroup[0].clear();
							ndel++;
						}
						nevents--;
					}

					ts_clock(&ts2);
					delta1 = 1000000.0 * ts_duration(ts1, ts2);

					std::cout << "Number of getFragment without data = " << datan << std::endl;
					std::cout << "Number of getFragment with data    = " << datay << std::endl;
					std::cout << "Total time     =  " << delta1 << " us" << std::endl;
					delta2 = delta1 / nevents2;
					std::cout << "Time per event =  " << delta2 << " us" << std::endl;

					ReadoutModule[0]->stopGathering(cmd);
					ReadoutModule[0]->unconfigure(cmd);
					delete ReadoutModule[0];
					ReadoutModule[0] = 0;
				}
				break;

				case TIMING5:
				{
					u_int clearfirst, nevents, nevents2, nextticket, nextl1id, tickets[4][2];
					u_int isempty, loop;
					u_int min1, min2, ddd = 0, datay = 0, datan = 0, ndel = 0;
					ROBFragment *fragment;
					tstamp ts1, ts2;
					float limit, percent = 10.0, delta1, delta2;

					if (ReadoutModule[0] != 0)
					{
						std::cout << "Array element 0 has already been filled"  << std::endl;
						return 1;
					}
					ReadoutModule[0] = new RobinNPReadoutModule();

					std::cout << "Configure Robin (0) or use default parameters (1)" << std::endl;
					defparams = getdecd(1);

					if (defparams == 0){
						getparams(configuration, 1);
					}
					else{
						getdefparams(configuration);
					}
					dataChannel = 0;	// only ONE channel

					ReadoutModule[0]->setup(configuration);
					ReadoutModule[0]->configure(cmd);
					ReadoutModule[0]->prepareForRun(cmd);

					if (gConfInfo.externalDataGeneratorFlag == true)
					{
						gConfInfo.SendRemoteCommand();
					}

					std::cout << "Enter the (float) request percentage" << std::endl;
					percent = getfloatd(percent);

					std::cout << "Enable debug mode (0 = No  1 = Yes)" << std::endl;
					ddd = getdecd(ddd);

					std::cout << "How many events do you want to process " << std::endl;
					nevents = getdecd(1000);
					nevents2 = nevents;
					nextl1id = 1;
					clearfirst = 1;
					limit = 0.0;
					getstat = 0;
					channels = const_cast< std::vector<DataChannel *> * >(ReadoutModule[0]->channels());
					std::cout << "Running. Press ctrl+\\ to get statistics" << std::endl;

					ts_clock(&ts1);
					//Fill the message Q
					tickets[0][0] = (*channels)[dataChannel]->requestFragment(nextl1id);
					tickets[0][1] = nextl1id;
					for (loop = 1; loop < 4; loop++)
					{
						nextl1id = getnext(percent, limit, nextl1id);
						tickets[loop][0] = (*channels)[dataChannel]->requestFragment(nextl1id);
						tickets[loop][1] = nextl1id;
					}
					nextticket = 0;

					while(nevents)
					{
						//Wait for a complete ROB fragment
						isempty = 1;
						while(isempty)
						{
							if (getstat)
							{
								getstat = 0;
								std::cout << "Number of empty events             = " << datan << std::endl;
								std::cout << "Number of complete events          = " << datay << std::endl;
								std::cout << "Number of release(100) groups sent = " << ndel << std::endl;
								std::cout << "Currently waiting for L1IDs " << tickets[0][1] << ", " << tickets[1][1] << ", " << tickets[2][1] << " and " << tickets[3][1] << std::endl;
							}

							if (ddd){
								std::cout << "waiting for L1ID " << tickets[nextticket][1] << std::endl;
							}
							fragment = dynamic_cast<ROBFragment *>((*channels)[dataChannel]->getFragment(tickets[nextticket][0]));

							if (fragment)
							{ // good fragment (with data)
								if (ddd){
									std::cout << " after getFragment: fragment status = " << std::hex << fragment->status() << std::dec << " L1id = " << fragment->level1Id() << std::endl;
								}
								delete fragment;

								idgroup[0].push_back(tickets[nextticket][1]);
								if (ddd){
									std::cout << "L1ID = " << tickets[nextticket][1] << " received and scheduled for release (datay = " << datay << ")" << std::endl;
								}
								isempty = 0;
								datay++;
							}
							else  //Requeue the L1ID; lost fragments are not handled correctly
							{
								tickets[nextticket][0] = (*channels)[dataChannel]->requestFragment(tickets[nextticket][1]);
								nextticket++;
								if (nextticket == 4){
									nextticket = 0;
								}
								datan++;
							}
						}

						//Queue a new L1ID
						nextl1id = getnext(percent, limit, nextl1id);
						tickets[nextticket][0] = (*channels)[dataChannel]->requestFragment(nextl1id);
						tickets[nextticket][1] = nextl1id;
						nextticket++;
						if (nextticket == 4){
							nextticket = 0;
						}
						//Find the lowest L1ID that we are still waiting for
						if (tickets[0][1] < tickets[1][1]){
							min1 = tickets[0][1];
						}
						else{
							min1 = tickets[1][1];
						}
						if (tickets[2][1] < tickets[3][1]){
							min2 = tickets[2][1];
						}
						else{
							min2 = tickets[3][1];
						}
						if (min1 < min2){
							min2 = min1;
						}
						if (ddd){
							std::cout << "Lowest outstanding L1ID = " << min2 << std::endl;
						}

						//Send a delete request if we have received 100 events
						if ((clearfirst + 100) < min2)
						{
							idgroup[0].clear();
							for(int loop = 0; loop < 100; loop++){
								idgroup[0].push_back(clearfirst++);
							}
							(*channels)[dataChannel]->releaseFragment(&idgroup[0]);
							ndel++;
							if (ddd){
								std::cout << "events released up to L1ID " << clearfirst - 1 << std::endl;
							}
						}
						nevents--;
					}

					ts_clock(&ts2);
					delta1 = 1000000.0 * ts_duration(ts1, ts2);

					std::cout << "Number of getFragment without data = " << datan << std::endl;
					std::cout << "Number of getFragment with data    = " << datay << std::endl;
					std::cout << "Total time     =  " << delta1 << " us" << std::endl;
					delta2 = delta1 / nevents2;
					std::cout << "Time per event out =  " << delta2 << " us" << std::endl;
					delta2 = delta2 * percent / 100.0;
					std::cout << "Time per event in =  " << delta2 << " us" << std::endl;

					ReadoutModule[0]->stopGathering(cmd);
					ReadoutModule[0]->unconfigure(cmd);
					delete ReadoutModule[0];
					ReadoutModule[0] = 0;
				}
				break;

				case TIMING6:
				{
					u_int nevents[12], nevents2[12], nextticket[12], nextl1id[12], tickets[12][4][2];
					u_int rdump = 0, cdata = 0, isempty, loop, looprol;
					u_int rol, ddd = 0, datal[12] = {}, datay[12] = {}, datan[12] = {}, ndel[12] = {};
					u_int nrols, numberOfBadEvents = 0;
					bool eventOk;
					ROBFragment *fragment;
					tstamp ts1, ts2;
					float delta1, delta2;

					TS_PAUSE(TS_H2);
					nrols = getparams(configuration, 0);
					if (ReadoutModule[0] != 0)
					{
						std::cout << "Array element 0 has already been filled"  << std::endl;
						return 1;
					}
					ReadoutModule[0] = new RobinNPReadoutModule();
					ReadoutModule[0]->setup(configuration);
					ReadoutModule[0]->configure(cmd);
					ReadoutModule[0]->prepareForRun(cmd);

					if (gConfInfo.externalDataGeneratorFlag == true)
					{
						gConfInfo.SendRemoteCommand();
					}

					std::cout << "Enable debug mode (0 = No  1 = Yes)" << std::endl;
					ddd = getdecd(ddd);

					std::cout << "Enable data checking (0 = No  1 = Yes  2 = Reduced output)" << std::endl;
					cdata = getdecd(cdata);

					std::cout << "Dump raw data (0 = No  1 = Yes)" << std::endl;
					rdump = getdecd(rdump);

					std::cout << "How many events do you want to process " << std::endl;
					u_int requestedNumber = getdecd(200000);

					for(u_int rolIndex = 0; rolIndex < gConfInfo.maxChannels; ++rolIndex)
					{
						nextl1id[rolIndex] = 1;
						if(nrols > rolIndex)
						{
							nevents[rolIndex] = requestedNumber;
							nevents2[rolIndex] = nevents[rolIndex];
						}
						else
						{
							nevents[rolIndex] = 0;
							nevents2[rolIndex] = 0;
						}
					}

					getstat = 0;
					channels = const_cast<std::vector<DataChannel *> *>(ReadoutModule[0]->channels());
					TS_RESUME(TS_H2);
					std::cout << "Running. Press ctrl+\\ to get statistics" << std::endl;

					ts_clock(&ts1);
					//Fill the message Q
					for (looprol = 0; looprol < nrols; looprol++)
					{
						for (loop = 0; loop < 4; loop++)
						{
							tickets[looprol][loop][0] = (*channels)[looprol]->requestFragment(nextl1id[looprol]);
							tickets[looprol][loop][1] = nextl1id[looprol];
							nextl1id[looprol]++;
						}
						nextticket[looprol] = 0;
						idgroup[looprol].clear();
					}

					rol = 0;

					bool continueLoop = false;
					for(u_int rolIndex = 0; rolIndex < nrols; ++rolIndex)
					{
						if(nevents[rolIndex] != 0){
							continueLoop = true;
						}
					}

					while(continueLoop)
					{
						isempty = 1;
						while(isempty)  //Wait for a complete ROB fragment
						{
							if (getstat)
							{
								getstat = 0;
								for (looprol = 0; looprol < nrols; looprol++)
								{
									std::cout << "ROL " << looprol << ":" << std::endl;
									std::cout << "Number of complete events          = " << datay[looprol] << std::endl;
									std::cout << "Number of missing events           = " << datan[looprol] << std::endl;
									std::cout << "Number of lost events              = " << datal[looprol] << std::endl;
									std::cout << "Number of events to be requested   = " << nevents[looprol] << std::endl;
									std::cout << "Number of release(100) groups sent = " << ndel[looprol] << std::endl;
									std::cout << "Currently waiting for L1IDs " << tickets[looprol][0][1] << ", " << tickets[looprol][1][1] << ", " << tickets[looprol][2][1] << " and " << tickets[looprol][3][1] << std::endl;
								}
							}

							while(nevents[rol] == 0)
							{
								rol++;                 //try another ROL
								if (rol == nrols){
									rol = 0;
								}
							}

							if (ddd){
								std::cout << "waiting for L1ID " << tickets[rol][nextticket[rol]][1] << " from ROL " << rol << std::endl;
							}
							fragment = dynamic_cast<ROBFragment *> ((*channels)[rol]->getFragment(tickets[rol][nextticket[rol]][0]));

							if (fragment)
							{ // good fragment (with data)
								if (ddd){
									std::cout << " after getFragment: fragment status = " << std::hex << fragment->status() << std::dec << " L1id = " << fragment->level1Id() << std::endl;
								}
								if (cdata)
								{
									if (ddd)
									{
										std::cout << "Calling checkData with" << std::endl;
										std::cout << "fragment at 0x " << &fragment << std::endl;
										std::cout << "tickets[rol][nextticket[rol]][1] = " << tickets[rol][nextticket[rol]][1] << std::endl;
										std::cout << "rol                              = " << rol << std::endl;
										std::cout << "rdump                            = " << rdump << std::endl;
									}
									eventOk = checkData(fragment, tickets[rol][nextticket[rol]][1], rol, cdata);
									if (eventOk != 0){
										numberOfBadEvents++;
									}
								}

								if(rdump){
									dumpData(fragment, rol);
								}
								if (ddd){
									std::cout << "masked status = " << std::hex << (fragment->status() & 0xf0000000) << std::dec << std::endl;
								}
								if((fragment->status() & 0xf0000000) == 0x90000000)
								{
									if (ddd){
										std::cout << "Fragment lost" << std::endl;
									}
									datal[rol]++;
								}
								else
								{
									if (ddd){
										std::cout << "Fragment OK" << std::endl;
									}
									datay[rol]++;
								}

								delete fragment;

								if (ddd){
									std::cout << "Event with L1ID " << tickets[rol][nextticket[rol]][1] << " received from ROL "  << rol << std::endl;
								}

								idgroup[rol].push_back(tickets[rol][nextticket[rol]][1]);

								isempty = 0;
							}
							else  //Requeue the L1ID
							{
								tickets[rol][nextticket[rol]][0] = (*channels)[rol]->requestFragment(tickets[rol][nextticket[rol]][1]);
								nextticket[rol]++;
								if (nextticket[rol] == 4){
									nextticket[rol] = 0;
								}
								/////if(datay[rol])
								datan[rol]++;
								rol++;  //try another ROL
								if (rol == nrols){
									rol = 0;
								}
							}
						}

						//Queue a new L1ID
						tickets[rol][nextticket[rol]][0] = (*channels)[rol]->requestFragment(nextl1id[rol]);
						tickets[rol][nextticket[rol]][1] = nextl1id[rol];
						nextl1id[rol]++;
						nextticket[rol]++;
						if (nextticket[rol] == 4){
							nextticket[rol] = 0;
						}
						//Send a delete request if we have received 100 events
						if (((datay[rol] + datal[rol]) % 100) == 0)
						{
							if (ddd){
								std::cout << "100 events released on ROL " << rol << std::endl;
							}

							(*channels)[rol]->releaseFragment(&idgroup[rol]);
							idgroup[rol].clear();

							ndel[rol]++;
						}
						if (nevents[rol]){
							nevents[rol]--;
						}
						rol++;         //try another ROL
						if (rol == nrols){
							rol = 0;
						}
						continueLoop = false;
						for(u_int rolIndex = 0; rolIndex < nrols; ++rolIndex)
						{
							if(nevents[rolIndex] != 0){
								continueLoop = true;
							}
						}
					}

					ts_clock(&ts2);
					delta1 = 1000000.0 * ts_duration(ts1, ts2);

					for (looprol = 0; looprol < nrols; looprol++)
					{
						std::cout << "ROL " << looprol << " : Number of getFragment without data = " << datan[looprol] << std::endl;
						std::cout << "ROL " << looprol << " : Number of getFragment with data    = " << datay[looprol] << std::endl;
						std::cout << "ROL " << looprol << " : Number of lost events              = " << datal[looprol] << std::endl;
					}
					std::cout << "Total time     =  " << delta1 << " us" << std::endl;
					delta2 = delta1 / (nevents2[0] + nevents2[1]);
					std::cout << "Time per event =  " << delta2 << " us" << std::endl;

					if (cdata){
						std::cout << "Number of events where data check failed = " << numberOfBadEvents << std::endl;
					}
					if (!testmode){
						ReadoutModule[0]->stopGathering(cmd);
					}
					ReadoutModule[0]->unconfigure(cmd);
					delete ReadoutModule[0];
					ReadoutModule[0] = 0;
				}
				break;

				case TIMING7:
				{
					tstamp ts1, ts2;
					float delta;
					int todo, last;
					u_int nrols, ddd = 0;
					int first = 1, total = 1000, gsize = 100;
					int rol[12] = {};

					nrols = getparams(configuration, 0);
					if (ReadoutModule[0] != 0)
					{
						std::cout << "Array element 0 has already been filled"  << std::endl;
						return 1;
					}
					ReadoutModule[0] = new RobinNPReadoutModule();
					ReadoutModule[0]->setup(configuration);
					ReadoutModule[0]->configure(cmd);
					ReadoutModule[0]->prepareForRun(cmd);

					if (gConfInfo.externalDataGeneratorFlag == true)
					{
						gConfInfo.SendRemoteCommand();
					}

					channels = const_cast< std::vector<DataChannel *> * >(ReadoutModule[0]->channels());

					std::cout << "Enable debug mode (0 = No  1 = Yes)" << std::endl;
					ddd = getdecd(ddd);

					std::cout << "Enter the L1ID of the first event to be released" << std::endl;
					first = getdecd(first);
					std::cout << "How many events do you want to release" << std::endl;
					total = getdecd(total);
					std::cout << "Enter the release group size (max 100)" << std::endl;
					gsize = getdecd(gsize);

					int numRolsToDel = 0;
					for(u_int rolIndex = 0; rolIndex < nrols; ++rolIndex)
					{
						std::cout << "Delete events from ROL " << rolIndex << " (1=yes  0=no)" << std::endl;
						rol[rolIndex] = getdecd(rol[rolIndex]);
						if(rol[rolIndex]){
							numRolsToDel++;
						}
					}

					ts_clock(&ts1);

					last = first + total - 1;
					while (first < last)
					{
						if ((first + gsize) < last){
							todo = gsize;
						}
						else{
							todo = last - first + 1;
						}

						if (ddd){
							std::cout << "Deleting group of " << todo << " events with IDs " << first << " to " << first + todo - 1 << std::endl;
						}

						idgroup[0].clear();
						for(int loop = 0; loop < todo; loop++){
							idgroup[0].push_back(first++);
						}
						for(u_int rolIndex = 0; rolIndex < nrols; ++rolIndex){
							if (rol[rolIndex]) {
								(*channels)[rolIndex]->releaseFragment(&idgroup[0]);
							}
						}
					}

					ts_clock(&ts2);

					delta = ts_duration(ts1, ts2);
					std::cout << "Total time for " << total << " events = " << delta * 1000000 << " us" << std::endl;
					if(numRolsToDel != 0 && total != 0){
						std::cout << "Time for one event (fragment) from one ROL = " << delta * 1000000 / total / numRolsToDel << " us" << std::endl;
					}
					ReadoutModule[0]->stopGathering(cmd);
					ReadoutModule[0]->unconfigure(cmd);
					delete ReadoutModule[0];
					ReadoutModule[0] = 0;
				}
				break;

				case DUMPMANY:
					std::cout << "Enter the array index (0.." << FM_POOL_NUMB-1 << ")" << std::endl;
					num = getdecd(0);

					if (ReadoutModule[num] == 0)
					{
						std::cout << "This array element has not yet been filled"  << std::endl;
						break;
					}

					std::cout << "Enter ROL (0 - 11)" << std::endl;
					dataChannel = getdecd(0);
					if (dataChannel > 11)
					{
						std::cout << "Wrong channel number." << std::endl;
						break;
					}

					std::cout << "How many events do you want to request" << std::endl;
					count = getdecd(100);

					std::cout << "Enter the L1ID to start with" << std::endl;
					il1id = getdecd(1);

					channels = const_cast<std::vector<DataChannel *> *>(ReadoutModule[num]->channels());

					for (l1id = il1id; l1id < (count + il1id); l1id++)
					{
						std::cout << "Requesting LVL1 ID: " << l1id << std::endl;
						value = (*channels)[dataChannel]->requestFragment(l1id);
						robfragment = dynamic_cast<ROBFragment *>((*channels)[dataChannel]->getFragment(value));
						std::cout << "Dumping ROB fragment" << std::endl;
						std::cout << "ROB fragment pointer = " << std::hex << robfragment << std::dec << std::endl;
						if(robfragment != 0){
							robfragment->print();
							delete robfragment;
						}
						else{
							std::cout << "Failed dynamic case to ROBFragment" << std::endl;
						}
					}
					break;

				case GETINFO:
				{
					std::cout << "Enter the array index (0.." << FM_POOL_NUMB - 1 << ")" << std::endl;
					num = getdecd(0);
					if(num >= FM_POOL_NUMB){
						std::cout << "Array index too large"  << std::endl;
						break;
					}
					if (ReadoutModule[num] == 0)
					{
						std::cout << "This array element has not yet been filled"  << std::endl;
						break;
					}

					std::cout << "Enter ROL (0 - 11)" << std::endl;
					dataChannel = getdecd(0);
					if (dataChannel > 11)
					{
						std::cout << "Wrong channel number." << std::endl;
						break;
					}

					channels = const_cast< std::vector<DataChannel *> * >(ReadoutModule[num]->channels());

					ISInfo *info = (*channels)[dataChannel]->getISInfo();

					std::cout << "RobIn reply:" << std::endl;
					std::cout << "--------------------------------------------------" << std::endl;
					info->print(std::cout);
					std::cout << "--------------------------------------------------" << std::endl;
				}
				break;

				case SETCONFIG:
					std::cout << "Not yet available" << std::endl;
					break;

				case RESET:
					std::cout << "Not yet available" << std::endl;
					break;

				case SETDEBUG:
					setdebug();
					break;

				case HELP:
					std::cout <<  " Exerciser program for the BusBasedRobinNPReadoutModule Class." << std::endl;
					std::cout <<  " The TIMING commands allows to measure the timing of the" << std::endl;
					std::cout <<  " the basic BusBasedRobinNPReadoutModule methods: this is a bit for specialists ..." << std::endl;
					break;

				case QUIT:
					quit_flag = 1;
					break;

				default:
					std::cout <<  "not implemented yet" << std::endl;
					break;
				} //main switch
			} while (quit_flag == 0);
		}
	}
	catch(std::exception &ex)
	{
		std::cout << ex.what() << std::endl;
	}

	TS_SAVE(TS_H2, "Robin_timing");
	TS_CLOSE(TS_H2);
	ts_close(TS_DUMMY);
	return 0;
}


/***************************************************/
int getnext(float percent, float limit, int nextl1id)
/***************************************************/
{
	while (limit < 100.0)
	{
		limit += percent;
		nextl1id++;
	}
	limit -= 100.0;
	return nextl1id;
}


/****************/
int setdebug(void)
/****************/
{
	printf("Enter the debug level: ");
	dblevel = getdecd(dblevel);
	printf("Enter the debug package: ");
	dbpackage = getdecd(dbpackage);
	DF::GlobalDebugSettings::setup(dblevel, dbpackage);
	return(0);
}



/*******************************************/
void dumpData(ROBFragment *fragment, int rol)
/*******************************************/
{
	u_int i, fsize;
	Buffer *fragBuffer = fragment->buffer();
	MemoryPage *memPage = (MemoryPage *) *(fragBuffer->begin());
	u_int *fragBufferAddress = (u_int *)memPage->address();

	fsize = (u_int)fragment->size();       //Total fragment size

	std::cout << "ROL " << rol << ": Fragment size = " << fsize << std::endl;
	for (i = 0; i < fsize; i++)
	{
		if((i % 8) == 0){
			std::cout << std::hex << "0x" << std::setw(3) << i << std::dec << ": ";
		}
		std::cout << std::hex << " 0x" << std::setw(8) << fragBufferAddress[i] << std::dec;

		if((i % 8) == 7){
			std::cout << std::endl;
		}
	}
	std::cout << std::endl;

	/*
  if (rdump)
  {
   std::cout << std::endl;
   std::cout << "ROL " << rol << ": Fragment size = " << fsize << std::endl;
   std::cout << "dstart = " << dstart << std::endl;
   std::cout << "sstart = " << sstart << std::endl;
   std::cout << "tstart = " << tstart << std::endl;
   std::cout << "dsize = " << dsize << std::endl;
   for (i = 0; i < fsize; i++)
   {
     std::cout << "word(" << i << "): 0x" << std::hex << fragBufferAddress[i] << std::dec;
     if (i == 0) std::cout << "  ROB: header marker" << std::endl;
     else if (i == 1) std::cout << "  ROB: total fragment size" << std::endl;
     else if (i == 2) std::cout << "  ROB: header size" << std::endl;
     else if (i == 3) std::cout << "  ROB: format version number" << std::endl;
     else if (i == 4) std::cout << "  ROB: source identifier" << std::endl;
     else if (i == 5) std::cout << "  ROB: number of status elements" << std::endl;
     else if (i == 6) std::cout << "  ROB: status element 1" << std::endl;
     else if (i == 7) std::cout << "  ROB: status element 2 (most recent ID; do not check)" << std::endl;
     else if (i == 8) std::cout << "  ROB: CRC type" << std::endl;
     else if (i == 9) std::cout << "  ROD: header marker" << std::endl;
     else if (i == 10) std::cout << "  ROD: header size" << std::endl;
     else if (i == 11) std::cout << "  ROD: format version number" << std::endl;
     else if (i == 12) std::cout << "  ROD: source identifier" << std::endl;
     else if (i == 13) std::cout << "  ROD: run number" << std::endl;
     else if (i == 14) std::cout << "  ROD: Level 1 ID" << std::endl;
     else if (i == 15) std::cout << "  ROD: Bunch crossing ID" << std::endl;
     else if (i == 16) std::cout << "  ROD: Level 1 trigger type" << std::endl;
     else if (i == 17) std::cout << "  ROD: Detector event type" << std::endl;
     else if (i == (fsize - 4)) std::cout << "  ROD: number of status elements" << std::endl;
     else if (i == (fsize - 3)) std::cout << "  ROD: number of data elements" << std::endl;
     else if (i == (fsize - 2)) std::cout << "  ROD: status block position (follows data)" << std::endl;
     else if (i == (fsize - 1))
     {
       std::cout << "  ROB: CRC word" << std::endl;
       std::cout << "Calculated CRC: 0x" << std::hex << calculatedCRC << std::dec << std::endl;
     }
     else std::cout << std::endl;
    }
  }
	 */

	return;
}


/*************************************************************/
int checkData(ROBFragment *fragment, int id, int rol, int mode)
/*************************************************************/
{
	static u_int evok = 0, evlim = 1;
	int notok = 0;
	Buffer *fragBuffer = fragment->buffer();
	MemoryPage *memPage = (MemoryPage *) *(fragBuffer->begin());
	u_int ref_data, ui, *fragBufferAddress = (u_int *)memPage->address();
	u_int i, ssize, fsize, dsize, hsize, tsize, dstart, sstart, tstart;
	u_int headerData[25];
	u_int headerDataGen[25];
	u_int trailerData[4];
	uint32_t crc1, crc2, calculatedCRC;

	if (mode < 1){
		return(0);   //Mode 0 = no check!
	}

	//We need some constants....
	fsize = (u_int)fragment->size();       //Total fragment size
	hsize = 18;                            //ROD header + ROB header size
	tsize = 4;                             //ROB trailer + ROD trailer size
	ssize = 1;                             //Number of status words in the ROD fragment
	dsize = fsize - hsize - tsize - ssize; //ROD data size
	dstart = hsize;
	sstart = fsize - (tsize + ssize);      //status elements follow data
	tstart = fsize - tsize;

	std::cout << "Total fragmet size = " << fsize << std::endl;
	std::cout << "ROD data size      = " << dsize << std::endl;

	headerData[0]  = 0xdd1234dd;                    //ROB: header marker
	headerData[1]  = fsize;                         //ROB: total fragment size
	headerData[2]  = 0x00000009;                    //ROB: header size
	headerData[3]  = 0x04000000;                    //ROB: format version number
	headerData[4]  = 0x510000 + rol;                //ROB: source identifier
	headerData[5]  = 0x00000002;                    //ROB: number of status elements
	headerData[6]  = 0x00000000;                    //ROB: status element 1
	headerData[7]  = 0x00000000;                    //ROB: status element 2 (most recent L1ID; do not check)
	headerData[8]  = 0x00000001;                    //ROB: check sum type
	headerData[9]  = 0xee1234ee;                    //ROD: header marker
	headerData[10]  = 0x00000009;                   //ROD: header size
	headerData[11]  = 0x03010000;                   //ROD: format version number
	headerData[12]  = 0x00a00000;                   //ROD: source identifier
	headerData[13]  = 0x00000001;                   //ROD: run number
	headerData[14]  = id;                           //ROD: Level 1 ID
	headerData[15]  = 3 * id;                       //ROD: Bunch crossing ID
	headerData[16]  = 0x00000007;                   //ROD: Level 1 trigger type
	headerData[17]  = 0x0000000a;                   //ROD: Detector event type

	headerDataGen[0]  = 0xdd1234dd;                 //ROB: header marker
	headerDataGen[1]  = fsize;                      //ROB: total fragment size
	headerDataGen[2]  = 0x00000009;                 //ROB: header size
	headerDataGen[3]  = 0x04000000;                 //ROB: format version number
	headerDataGen[4]  = 0x510000 + rol;             //ROB: source identifier
	headerDataGen[5]  = 0x00000002;                 //ROB: number of status elements
	headerDataGen[6]  = 0x00000000;                 //ROB: status element 1
	headerDataGen[7]  = 0x00000000;                 //ROB: status element 2 (most recent L1ID; do not check)
	headerDataGen[8]  = 0x00000001;                 //ROB: check sum type
	headerDataGen[9]  = 0xee1234ee;                 //ROD: header marker
	headerDataGen[10]  = 0x00000009;                //ROD: header size
	headerDataGen[11]  = 0x03010000;                //ROD: format version number
	headerDataGen[12]  = 0xabcdacbd;                //ROD: source identifier
	headerDataGen[13]  = 0x00000000;                //ROD: run number
	headerDataGen[14]  = id;                        //ROD: Level 1 ID
	headerDataGen[15]  = 0x11111111;                //ROD: Bunch crossing ID
	headerDataGen[16]  = 0xb0b0b0b0;                //ROD: Level 1 trigger type
	headerDataGen[17]  = 0x0d0d0d0d;                //ROD: Detector event type

	trailerData[0] = 0x00000001;                    //ROD: number of status elements
	trailerData[1] = dsize;                         //ROD: number of data elements
	trailerData[2] = 0x00000001;                    //ROD: status block position (follows data)
	trailerData[3] = 0x00000000;                    //ROB: CRC value (do not check)

	// start CRC calculation
	crc1 = 0xffff;
	crc2 = 0xffff;

	//Header
	for (i = 0; i < hsize; i++)
	{
		if ((fragBufferAddress[i] != headerData[i]) && (fragBufferAddress[i] != headerDataGen[i]) && i != 7)
		{
			std::cout << "Header " << i << ": 0x" << std::hex << fragBufferAddress[i] << " [expected 0x" << headerData[i];
			if (headerData[i] != headerDataGen[i]){
				std::cout << " or 0x" << headerDataGen[i];
			}
			std::cout << "]" << std::dec << std::endl;
			notok = 1;
			if (mode == 2)
			{
				std::cout << "L1ID = " << id << "  ROL = " << rol << std::endl;
				return(notok);
			}
		}
		if (i > 8)
		{
			// ROD header, need to be taken into account for CRC calculation
			crc1 = (crc1 << 16) ^ crctab16[(crc1 & 0xffff) ^ (fragBufferAddress[i] >> 16)];
			crc2 = (crc2 << 16) ^ crctab16[(crc2 & 0xffff) ^ (fragBufferAddress[i] & 0xffff)];
		}
	}

	//Data
	ref_data = fragBufferAddress[dstart] + 1;
	crc1 = (crc1 << 16) ^ crctab16[(crc1 & 0xffff) ^ (fragBufferAddress[dstart] >> 16)];
	crc2 = (crc2 << 16) ^ crctab16[(crc2 & 0xffff) ^ (fragBufferAddress[dstart] & 0xffff)];
	for (ui = 1; ui < dsize; ui++)
	{
		if (fragBufferAddress[ui + dstart] != ref_data)
		{
			std::cout << "Data " << ui + dstart << ": 0x" << std::hex << fragBufferAddress[ui + dstart] << " [expected 0x" << ref_data << " ] " << std::dec << std::endl;
			notok = 1;
			if (mode == 2)
			{
				std::cout << "L1ID = " << id << "  ROL = " << rol << std::endl;
				return(notok);
			}
		}
		crc1 = (crc1 << 16) ^ crctab16[(crc1 & 0xffff) ^ (fragBufferAddress[ui + dstart] >> 16)];
		crc2 = (crc2 << 16) ^ crctab16[(crc2 & 0xffff) ^ (fragBufferAddress[ui + dstart] & 0xffff)];
		ref_data = fragBufferAddress[ui + dstart] + 1;
	}

	//Status
	if (fragBufferAddress[sstart] != 0)
	{
		std::cout << "Status " << sstart << ": 0x" << std::hex << fragBufferAddress[sstart] << " [expected 0x0 ] " << std::dec << std::endl;
		notok = 1;
		if (mode == 2)
		{
			std::cout << "L1ID = " << id << "  ROL = " << rol << std::endl;
			return(notok);
		}
	}
	crc1 = (crc1 << 16) ^ crctab16[(crc1 & 0xffff) ^ (fragBufferAddress[sstart] >> 16)];
	crc2 = (crc2 << 16) ^ crctab16[(crc2 & 0xffff) ^ (fragBufferAddress[sstart] & 0xffff)];

	//Trailer
	for (i = 0; i < tsize - 1; i++)  //Do not check the CRC value in the last word of the trailer
	{
		if (fragBufferAddress[i + tstart] != trailerData[i])
		{
			std::cout << "Trailer " << i + tstart << ": 0x" << std::hex << fragBufferAddress[i + tstart] << " [expected 0x" << trailerData[i] << " ] " << std::dec << std::endl;
			notok = 1;
			if (mode == 2)
			{
				std::cout << "L1ID = " << id << "  ROL = " << rol << std::endl;
				return(notok);
			}
		}
		crc1 = (crc1 << 16) ^ crctab16[(crc1 & 0xffff) ^ (fragBufferAddress[i + tstart] >> 16)];
		crc2 = (crc2 << 16) ^ crctab16[(crc2 & 0xffff) ^ (fragBufferAddress[i + tstart] & 0xffff)];
	}

	if(notok){
		std::cout << "L1ID = " << id << "  ROL = " << rol << std::endl;
	}

	crc1 &= 0xffff;
	crc2 &= 0xffff;
	calculatedCRC = (crc1 << 16) + crc2;
	if (fragBufferAddress[fsize - 1] != calculatedCRC){
		notok = 1;
	}

	if (calculatedCRC != fragBufferAddress[fsize - 1])
	{
		std::cout << "ERROR: The CRC does not match!" << std::endl;
		std::cout << "ROB trailer CRC = 0x" << std::hex << fragBufferAddress[fsize - 1] << ". Calculated CRC = 0x" << calculatedCRC << std::dec << std::endl;
	}

	if (!notok)
	{
		evok++;
		if (!(evok % evlim)){
			std::cout << evok << " events OK. Last L1ID was " << id << std::endl;
		}
		if (evok == (10 * evlim)){
			evlim = evok;
		}
	}

	return notok;
}


/***************************************************************/
int getparams(DFCountedPointer<Config> configuration, u_int mode)
/***************************************************************/
{
	// Definitions:
	// PhysicalAddress        = The PCI slot number of the Robin card
	// numberOfChannels       = The number of ROLs on one Robin card
	// memoryPoolNumPages     = The number of memory pages for ROB fragments
	// memoryPoolPageSize     = The size of a memory page for ROB fragments
	// ROLPhysicalAddress     = The physical number of a ROL on a Robin card
	// cfgParms[l_index].name = The Robin coonfiguration parameters

	u_int yesno, value, noChannels, p1, p2, p3;

	std::cout << "Enter the value for <PhysicalAddress> (this is the PCI logical board number: 0 .. n)" << std::endl;
	value = getdecd(0);
	configuration->set("PhysicalAddress", value);

	std::cout << "Enter the value for <FPGAVersion> " << std::endl;
	value = gethexd(0x1060000);
	configuration->set("FPGAVersion", value);

	if (mode == 0)
	{
		std::cout << "Enter the value for <numberOfChannels> " << std::endl;
		noChannels = getdecd(1);
		configuration->set("numberOfChannels", noChannels);
	}
	else
	{
		configuration->set("numberOfChannels", 1);
		noChannels = 1;
	}

	std::cout << "Enter the value for <Pagesize>" << std::endl;
	p2 = getdecd(512);
	configuration->set("Pagesize", p2);

	std::cout << "Enter the value for <Numpages>" << std::endl;
	value = getdecd(32768);
	configuration->set("Numpages", value);

	std::cout << "Enter the value for <MaxRxPages>" << std::endl;
	p1 = getdecd(2);
	configuration->set("MaxRxPages", p1);

	configuration->set("triggerQueue", 0);

	for (u_int loop = 0; loop < noChannels; loop++)
	{
		std::cout << "Enter the value for <ROLPhysicalAddress#" << loop << "> " << std::endl;
		value = getdecd(loop);
		configuration->set("ROLPhysicalAddress", value, loop);

		std::cout << "Enter the value for <Id#" << loop << "> " << std::endl;
		value = getdecd(loop);
		configuration->set("Id", (0x51 << 16) + loop, loop);

		std::cout << "Enter the value for <memoryPoolNumPages#" << loop << ">" << std::endl;
		value = getdecd(20);
		configuration->set("memoryPoolNumPages", value, loop);

		std::cout << "Enter the value for <memoryPoolPageSize#" << loop << "> in bytes" << std::endl;
		p3 = getdecd(16384);
		configuration->set("memoryPoolPageSize", p3, loop);

		if ((p2 * 4 * p1) > p3)
		{
			std::cout << "With the parameters you have selected the ROBIN can send fragments of up to " << p2 * 4 * p1 << " bytes" << std::endl;
			std::cout << "But the local buffer manager can only handle fragments of at most " << p3 << " bytes" << std::endl;
			std::cout << "Don't say you have not be warned" << std::endl;
		}

		std::cout << "Do you want to enter more ROL specific ROBIN configuration parameters (1=yes  0=no)" << std::endl;
		yesno = getdecd(0);

		configuration->set("CrcCheckInterval", 100, loop);
		configuration->set("UID", "dummy" + loop, loop);
		configuration->set("ForceL1ID", 0, loop);

		if (yesno)
		{
			std::cout << "Enter the value for <RolDataGen#" << loop << ">" << std::endl;
			value = getdecd(0);
			configuration->set("RolDataGen", value, loop);

			std::cout << "Enter the value for <TestSize#" << loop << ">" << std::endl;
			value = getdecd(0);
			configuration->set("TestSize", value, loop);

			std::cout << "Enter the value for <Keepfrags#" << loop << ">" << std::endl;
			value = getdecd(0);
			configuration->set("Keepfrags", value, loop);

			std::cout << "Enter the value for <ForceL1ID#" << loop << ">" << std::endl;
			value = getdecd(0);
			configuration->set("ForceL1ID", value, loop);

			std::cout << "Enter the value for <CrcCheckInterval#" << loop << ">" << std::endl;
			value = getdecd(100);
			configuration->set("CrcCheckInterval", value, loop);

			configuration->set("UID", "dummy" + loop, loop);
		}
	}

	return(noChannels);
}



/******************************************************/
int getdefparams(DFCountedPointer<Config> configuration)
/******************************************************/
{
	configuration->set("PhysicalAddress", 0);
	configuration->set("FPGAVersion", 0x1060000);
	configuration->set("numberOfChannels", 1);
	configuration->set("SubDetectorId", 0x51);
	configuration->set("Pagesize", 512);
	configuration->set("Numpages", 32768);
	configuration->set("MaxRxPages", 2);
	configuration->set("triggerQueue", 0);
	configuration->set("ROLPhysicalAddress", 0, 0);
	configuration->set("memoryPoolNumPages", 20, 0);
	configuration->set("memoryPoolPageSize", 16384, 0);
	configuration->set("CrcCheckInterval", 100, 0);
	configuration->set("UID", "dummy", 0);
	configuration->set("ForceL1ID", 0, 0);
	configuration->set("RolDataGen", 0, 0);
	configuration->set("TestSize", 200, 0);
	configuration->set("Keepfrags", 0, 0);
	configuration->set("Id", 0, 0);
	return(1);
}
