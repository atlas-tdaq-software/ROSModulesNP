
#include <iostream>
#include <string.h>
#include <stdlib.h>

#include "ConfigInfoObject.h"

/**********************************/
ConfigInfoObject::ConfigInfoObject()
/**********************************/
{
	externalDataGeneratorFlag   = false;
	setupNeventFraction         = 0.5;
	deleteGroupSize             = 100;
	numberOfLVL1AcceptsPerCycle = 1000000;
	linkMask                    = 0xFFF;
	oneCycleFlag                = false;
	initialStepReducFactor      = 0.02;
	initialValueReducFactor     = 0.9;
	requestFraction             = 100.0;
	pageSize                    = 512;
	numPages                    = 32768;
	maxRXPages                  = 2;
	memoryPoolPageSize          = 16384;
	memoryPoolNumPages          = 100; //number of tickets must not exceed this
	verboseFlag                 = false;
	numberOfWordsPerFragment    = 100;
	pciSlotNumber               = 0;
	firmwareVersion             = 0x1060000;
	check_verbosity             = 2;
	targetDeleteRate			= 100.0;
	nonIntPerformMeasFlag       = false;
	dataCheckFlag               = false;
	dataDumpFlag                = false;
	iterateFlag                 = false;
	clearAllFlag                = false;
	noClearsFlag                = false;
	debugPrintFlag = false;

	strcpy (dolarScriptName,   "dolar");
	strcpy (dolarComputerName, "osiris");
	strcpy (nameOfKillScript,  "killdolar");
	strcpy (nameOfConfigFile,  "testRobinConfig.txt");

	remoteStartCmd[0] = 0;
	strcat(remoteStartCmd, "ssh ");
	strcat(remoteStartCmd, dolarComputerName);
	strcat(remoteStartCmd, " -x ");
	strcat(remoteStartCmd, dolarScriptName);
	strcat(remoteStartCmd, " ");

	remoteKillCmd[0] = 0;
	strcat(remoteKillCmd, "ssh ");
	strcat(remoteKillCmd, dolarComputerName);
	strcat(remoteKillCmd, " -x ");
	strcat(remoteKillCmd, nameOfKillScript);
}


/*************************************************/
void ConfigInfoObject::SetRemoteStartCmd(char *cmd)
/*************************************************/
{
	if (strlen(cmd) > 80)
	{
		printf("Remote start command too long\n");
		exit(0);
	}

	std::cout << "command " << cmd << std::endl;

	remoteStartCmd[0] = 0;
	strcat(remoteStartCmd, "ssh ");
	strcat(remoteStartCmd, dolarComputerName);
	strcat(remoteStartCmd, " -x ");
	strcat(remoteStartCmd, cmd);
	strcat(remoteStartCmd, " ");
}


/**************************************************/
void ConfigInfoObject::SetRemoteComputer(char *name)
/**************************************************/
{
	if (strlen(name) > 80)
	{
		printf("Remote computer name too long\n");
		exit(0);
	}

	dolarComputerName[0] = 0;
	strcat(dolarComputerName, name);
	SetRemoteStartCmd(dolarScriptName);
	SetRemoteKillCmd(nameOfKillScript);
}


/************************************************/
void ConfigInfoObject::SetRemoteKillCmd(char *cmd)
/************************************************/
{
	if (strlen(cmd) > 80)
	{
		printf("Remote kill command too long\n");
		exit(0);
	}

	remoteKillCmd[0] = 0;
	strcat(remoteKillCmd, "ssh ");
	strcat(remoteKillCmd, dolarComputerName);
	strcat(remoteKillCmd, " -x ");
	strcat(remoteKillCmd, cmd);
}


/********************************************/
void ConfigInfoObject::SendRemoteCommand(void)
/********************************************/
{
	char nWords[20];

	sprintf(nWords, "%d", numberOfWordsPerFragment);
	strcat(remoteStartCmd, nWords);

	if (verboseFlag == true)
		printf("Remote command: %s\n", remoteStartCmd);
	else
		strcat(remoteStartCmd, " > /dev/null");
	system(remoteStartCmd);
}


/*****************************************/
void ConfigInfoObject::SendRemoteKill(void)
/*****************************************/
{
	if (verboseFlag == true)
		printf("Remote kill command: %s\n", remoteKillCmd);
	else
		strcat(remoteKillCmd, " > /dev/null");
	system(remoteKillCmd);
}



/*********************************************************************/
int ConfigInfoObject::SetParams(DFCountedPointer<Config> configuration)
/*********************************************************************/
{
	// derived from getparams

	// Definitions:
	// PhysicalAddress        = The PCI slot number of the Robin card
	// timeout                = The message time-out in seconds
	// numberOfChannels       = The number of ROLs on one Robin card
	// memoryPoolNumPages     = The number of memory pages for ROB fragments
	// memoryPoolPageSize     = The size of a memory page for ROB fragments
	// msgInputMemorySize     = The amount of message DPM to be reserved in one Robin card
	// miscSize               = The amount of PC memory to be reserved for misc. messages
	// ChannelId              = The logical ID of a ROL
	// ROLPhysicalAddress     = The physical number of a ROL on a Robin card
	// cfgParms[l_index].name = The Robin configuration parameters

	configuration->set("PhysicalAddress", pciSlotNumber);
	configuration->set("FPGAVersion", firmwareVersion);

	u_int noChannels = 0;
	for(unsigned int channels = 0; channels < maxChannels; ++channels){
		noChannels += (linkMask & (1 << channels)) >> channels;
	}
	configuration->set("numberOfChannels", noChannels);

	/*
    Some of the benchmarking functions do not run if there is insufficient DPM memory
    These functions are designed such that they generate a certain number of outstanding
    requests for fragments. These messages block parts of the available DPM space. At one moment
    the S/W will try to release a group of events. The message with the L1IDs is large
    (~400 bytes) and the release function is blocking (i.e. it will not return if it cannot send
    the message due to a lack of DPM memory). If the release function fails to get
    DPM memory because one of the outstanding fragment requests is in the way the release function
    cannot do its job and blocks the whole process.
	 */

	configuration->set("Pagesize", pageSize);
	configuration->set("Numpages", numPages);
	configuration->set("MaxRxPages", maxRXPages);
	configuration->set("triggerQueue", 0);

	for (u_int loop = 0; loop < noChannels; loop++)
	{
	  configuration->set("Id", (0x51 << 16) + loop, loop);
		configuration->set("ROLPhysicalAddress", loop, loop);
		configuration->set("memoryPoolNumPages", memoryPoolNumPages, loop);
		configuration->set("memoryPoolPageSize", memoryPoolPageSize, loop);

		if ((pageSize * 4 * maxRXPages) > memoryPoolPageSize)
		{
			std::cout << "With the parameters you have selected the ROBIN can send fragments of up to " << pageSize * 4 * maxRXPages << " bytes" << std::endl;
			std::cout << "But the local buffer manager can only handle fragments of at most " << memoryPoolPageSize << " bytes" << std::endl;
			std::cout << "Relevant parameters: pageSize ROBIN = " << pageSize << " words, maxRXPages = " << maxRXPages << std::endl;
			std::cout << "Therefore maximum fragment size = " << pageSize * maxRXPages << " words = " << pageSize * maxRXPages * 4 << " bytes" << std::endl;
			std::cout << "This should fit into one page of the memory pool in the ROS (memoryPoolPageSize) of " << memoryPoolPageSize << " bytes" << std::endl;
			exit(0);
		}

		if (externalDataGeneratorFlag == false)
			configuration->set("RolDataGen", 1, loop);
		else
			configuration->set("RolDataGen", 0, loop);

		configuration->set("UID", "dummy" + loop, loop);
		configuration->set("TestSize", numberOfWordsPerFragment, loop);
		configuration->set("Keepfrags", 0, loop);
		configuration->set("ForceL1ID", 0, loop);
		configuration->set("CrcCheckInterval", 100, loop);
	}

	return(noChannels);
}


/******************************************/
void ConfigInfoObject::SetLinkMask(int mask)
/******************************************/
{
	if ((mask >= 0) && (mask < 4096))
		linkMask = mask;
	else
	{
		printf("invalid mask %d\n", mask);
		exit(0);
	}
	if ( (linkMask & 1) == 0)
	{
		printf("link 0 should always be enabled, but is not enabled for the linkmask (%d) used\n", linkMask);
		exit(0);
	}
}
