#ifndef TESTROBINNP_H
#define TESTROBINNP_H

#include "ROSEventFragment/ROBFragment.h"
#include "DFSubSystemItem/Config.h"

using namespace ROS;

enum
{
	TIMING4 = 1,
	TIMING5,
	TIMING6,
	TIMING7,
	CREATE,
	CONFIGURE,
	START,
	REQUEST,
	GET,
	GC,
	RELEASEMANY,
	STOP,
	UNCONFIGURE,
	DELETE,
	DUMPMANY,
	GETINFO,
	SETCONFIG,
	RESET,
	SETDEBUG,
	HELP,
	QUIT = 100
};


//Constants
#define FM_POOL_NUMB 5      //Max. number of Robin cards


// Prototypes
int checkData(ROBFragment *fragment, int id, int rol, int mode);
int getnext(float percent, float limit, int nextl1id);
int setdebug(void);
int getparams(DFCountedPointer<Config> configuration, u_int mode);
void dumpData(ROBFragment *fragment, int rol);
void sigquit_handler(int signum);
void usage(void);

#endif //TESTROBINNP_H
