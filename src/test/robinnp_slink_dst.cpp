/*****************************************************************************/
/*                                                                           */
/* File Name        : robinnp_slink_dst.cpp                                  */
/*                                                                           */
/* Author           : Markus Joos			                     */
/*                                                                           */
/***** C 2014 - A nickel program worth a dime ********************************/

//This program was developed on the basis of the documentation provided at:
//twiki.cern.ch/twiki/bin/viewauth/Atlas/RobinNPProgrammingGuide


//Open questions: 
//- how to reset a ROL (such that data in the buffers gets cleared)


#include <unistd.h>
#include <iostream>
#include <signal.h>
#include <iomanip>
#include <errno.h>
#include <exception>
#include <vector>
#include <string>
#include <sys/types.h>
#include <getopt.h>
#include <stdio.h>
#include <fcntl.h>
#include "ROSEventFragment/ROBFragment.h"
#include "ROSModulesNP/RobinNPReadoutModule.h"
#include "ROSModulesNP/RobinNPDataChannel.h"
#include "ROSBufferManagement/Buffer.h"
#include "DFSubSystemItem/Config.h"
#include "ROSGetInput/get_input.h"
#include "rcc_time_stamp/tstamp.h"
#include "ROSRobinNP/RobinNPHardware.h"
#include "ROSRobinNP/RolNP.h"
#include "test_RobinNP.h"
#include "ConfigInfoObject.h"
#include "RunControl/FSM/FSMCommands.h"

using namespace ROS;

#ifndef TRUE
  #define TRUE                  0x01
#endif

#ifndef FALSE
  #define FALSE                 0x00
#endif

// global variables
int verbose = FALSE;
int do_not_delete = FALSE;
int print = FALSE;
int dcheck = TRUE;
int crccheck = FALSE;
int datagen = FALSE;
int L1extdcheck = TRUE; 
int npackets = -1;
int occurence = 1;
int useecrs = FALSE;
int testsize = 100;
int maxrxpages = 2;
int robinpagesize = 512;
int memorypoolnumpages = 5000;
int memorypoolpagesize = 81920;
unsigned int fpgaversion = 0x1060000;
int usdelay = 0;
int writefile = FALSE;
int outputFile;
char filename[200] = {0};
int checkfile = FALSE;
int inputFile;
u_int firstl1id = 0;
u_int mrid = 0;   
char filename2[200] = {0};
int rawdump = 0, dev, flg_error, delta_time, fragmentSize;
long ipacket, lpacket;
long start_time,end_time;
float t_per_packet, packet_per_s, mb_per_s;
u_int rec_size,exp_size, last_l1id = 999999;
char refbuf[0x100000];
u_int dblevel = 0, dbpackage = DFDB_ROSFM;
bool DVS = FALSE;
u_int li1d_list[64 * 1024];   
static u_int mori = 0, noecr = 0;   
daq::rc::TransitionCmd cmd(daq::rc::FSM_COMMAND::INIT_FSM);
RobinNPReadoutModule *module;
u_int nextl1id;
std::vector<DataChannel *> *channels;
bool run;

/*
int getstat;
u_int dblevel = 0, dbpackage = DFDB_ROSFM;
u_long crctab16[65536]; // for CRC calculation
const u_int maxNumberOfTickets = 15; //can alter - WPV
  int c;
  u_int prereq = 1, dataChannel;
  std::vector <u_int> idgroup[12];
  struct sigaction sa;
  DFCountedPointer<Config> configuration = Config::New();
  u_int tsize = 200, nevents = 1000, nextticket, nextl1id, ticket;
  u_int isempty, loop;
  u_int min2 = 0, datay = 0, datan = 0, ndel = 0;
  ROBFragment *fragment;
*/


typedef enum printType { SUMMARY=0 , FULL=1 } PrintType;
typedef enum checkType { SIZE , L1ID , GLOBAL } CheckType;

u_int ccode;
int errors[GLOBAL + 1]    = { GLOBAL * 0 , 0 };
int errorsTot[GLOBAL + 1] = { GLOBAL * 0 , 0 };

// Constants
#define MAX_PACKET_SIZE  (1024 * 1024)  // = 1 MB 
#define MAXROLS          24 


//Prototypes
u_int next_l1id_ecr(RobinNPDataChannel *prolnp, u_int *l1id);
u_int fill_ecr_list(RobinNPDataChannel *prolnp);


/**************/
void usage(void)
/**************/
{
  std::cout << "Valid options are ..." << std::endl;
  std::cout << "-o x: Occurence (= ROL number 1..24)               -> Default: " << occurence << std::endl;
  std::cout << "-n x: Number of packets                            -> Default: infinite" << std::endl;
  std::cout << "-I x: Start with L1ID=x                            -> Default: 0" <<std::endl;
  std::cout << "-a  : Check ROB CRC                                -> Default: FALSE" << std::endl;
  std::cout << "-b  : Use RolDataGen                               -> Default: FALSE" << std::endl;
  std::cout << "  -S: TestSize in words                            -> Default: 100" << std::endl;
  std::cout << "-e  : Use ECRs                                     -> Default: FALSE" << std::endl;
  std::cout << "-p  : Print packets                                -> Default: FALSE" << std::endl;
  std::cout << "-c  : Disable data check                           -> Default: FALSE" << std::endl;  
  std::cout << "  Modifiers of -c option:" << std::endl;
  std::cout << "  -L: Disable checking that L1ids are consecutive  -> Default: FALSE" << std::endl;  
  std::cout << "-f x: Write data to file. The parameter is the path and the name of the file" << std::endl;
  std::cout << "-C x: Compare with data from file. The parameter is the path and the name of the reference file" << std::endl;
  std::cout << "-v  : Verbose output                               -> Default: FALSE" << std::endl;
  std::cout << "-r x: Number of raw data words to be dumped        -> Default: 0" << std::endl;
  std::cout << "-w x: Number of microseconds to wait after" << std::endl;
  std::cout << "      the reception of a packet                    -> Default: 0" << std::endl;  
  std::cout << "-s  : ROBINNP page size (in words)                 -> Default: 512" << std::endl; 
  std::cout << "-N  : ROBINNP memorypoolNumPages                   -> Default: 5000" << std::endl;
  std::cout << "-P  : ROBINNP memorypoolPageSize                   -> Default: 81920" << std::endl;
  std::cout << "-F  : ROBINNP FPGAVersion                          -> Default: 0x1060000" << std::endl;
  std::cout << "-m  : Max number of pages per event (1..512)       -> Default: 2" << std::endl;
  std::cout << "-q  : Do not delete fragments (for XOFF testing)   -> Default: FALSE" << std::endl;
  std::cout << "-d  : Debug level                                  -> Default: 0" << std::endl;
  std::cout << "-D  : Debug package                                -> Default: 14" << std::endl;
  std::cout << "--DVS : To be used by DVS                          -> Default: FALSE" << std::endl;
  std::cout << std::endl;
}


/***************************/
void printStat(PrintType arg) 
/***************************/
{
  std::cout << std::endl << std::endl << "----------Test Statistics-----------" << std::endl;

  if (arg) 
  {
    std::cout << " # packets transferred       = " << (ipacket - lpacket) << std::endl;
    std::cout << " # corrupted packets         = " << errors[GLOBAL] << std::endl;
    std::cout << "      wrong size             = " << errors[SIZE] << std::endl;
    std::cout << "      wrong L1id             = " << errors[L1ID] << std::endl;
    std::cout << " time elapsed (s)            = " << delta_time << std::endl;
    std::cout << "------------------------------------" << std::endl;
  }
  
  std::cout << " Total # packets transferred = " << (ipacket - 1) << std::endl;
  std::cout << " Total # corrupted packets   = " << errorsTot[GLOBAL] << std::endl;
  std::cout << "            wrong size       = " << errorsTot[SIZE] << std::endl;
  std::cout << "            wrong L1id       = " << errorsTot[L1ID] << std::endl;
  
  if (arg && (ipacket - lpacket)) 
  {    
    t_per_packet = ((float) delta_time * 1000000) / (ipacket - lpacket);
    packet_per_s = (float)1000000 / t_per_packet;
    mb_per_s = ((float)(ipacket - lpacket) * fragmentSize * 4) / ((float)delta_time * 1024 * 1024);
    std::cout << "------------------------------------" << std::endl;
    std::cout << " time/packet = " << t_per_packet << " microseconds" << std::endl;
    std::cout << " packet/s    = " << packet_per_s << " packet/s" << std::endl;
    std::cout << " Mbyte/s     = " << mb_per_s << std::endl;
  }

  std::cout << "------------------------------------" << std::endl << std::endl;
}


/*************************/
void errorFound(u_int type)
/*************************/
{
  if (!flg_error) 
  {
    errors[GLOBAL]++;
    errorsTot[GLOBAL]++;
  }
  
  flg_error = 1;
  errors[type]++;
  errorsTot[type]++;
}


/*********************/
void terminate_it(void) //MJ: not yet ported
/*********************/
{
  err_type code;

  std::cout << "printing last statistics" << std::endl;
  printStat(SUMMARY);

  module->stopGathering(cmd);
  module->unconfigure(cmd);
  delete module;

 
  code = ts_close(TS_DUMMY);
  if (code) 
    rcc_error_print(stdout, code);
  std::cout << "ts_close done" << std::endl;

  if (writefile)
    close (outputFile);
}


// sigquit handler
/**********************************/
void sigquit_handler(int /*signum*/)
/**********************************/
{
  int i;
  
  if (start_time) 
  {
    time(&end_time);
    delta_time = end_time - start_time;
    if (delta_time) 
      printStat(FULL);
  }
  
  // reset flags
  lpacket = ipacket;
  for (i = 0; i < GLOBAL; i++) 
    errors[i] = 0;
  time(&start_time);
}


// sigint handler
/*********************************/
void sigint_handler(int /*signum*/)
/*********************************/
{
	run = false;
//  terminate_it();
//  exit(-1);
}


/*****************************/
int main(int argc, char **argv) 
/*****************************/
{
  int c, robin_nr, rol_nr;
  u_int next_ok, l1id, code, maxdump, loop;
  u_long ticket;
  struct sigaction sa, saint;
  sigset_t smask;
  std::vector <u_int> l1ids;
  DFCountedPointer<Config> configuration = Config::New();
  ROBFragment *fragment;
  RobinNPDataChannel *prolnp;
  
  static struct option long_options[] = {{"DVS", no_argument, NULL, '1'}}; 
  while ((c = getopt_long(argc, argv, "I:abegho:n:pcLvf:C:d:s:S:m:D:r:w:1N:P:F:q", long_options, NULL)) != -1)
    switch (c) 
    {
    case 'h':
      std::cout << "Usage: " << argv[0] << " [options]: "<< std::endl;
      usage();
      exit(-1);
      break;

    case 'o':
      occurence = atoi(optarg);
      if (occurence <= 0 || occurence > MAXROLS)
      { 
	std::cout << "Occurence exceeds allowed bounds" << std::endl;
        exit(-1);
      }
      break;

    case 'n':
      npackets = atoi(optarg);
      if (npackets<0) 
      {
	std::cout << "number of packets must be positive" << std::endl;
	exit(-1);
      } 
      break;
                  
    case 'f':   
      writefile = TRUE;
      sscanf(optarg,"%s", filename);
      std::cout << "writing data to file " << filename << std::endl;
      break;

    case 'q':   
      do_not_delete = TRUE;
      std::cout << "Fragments will not be deleted. This will provoke a permanent XOFF" << std::endl;
      break;
      
     case 'C':   
      checkfile = TRUE;
      sscanf(optarg, "%s", filename2);
      std::cout << "Reading reference fragment from file " << filename2 << std::endl;
      break;
   
    case 'r': rawdump = atoi(optarg);       break; 
    case 'S': testsize = atoi(optarg);      break; 
    case 'd': dblevel = atoi(optarg);       break;
    case 'D': dbpackage = atoi(optarg);     break;                   
    case 'I':            
      if (sscanf(optarg, "%x", &firstl1id) != 1)
        printf("Failed to decode first L1ID\n");
      break;               
    case 'w': usdelay = atoi(optarg);       break;                   
    case 'p': print = TRUE;                 break;
    case 'c': dcheck = FALSE;               break;
    case 'L': L1extdcheck = FALSE;          break;
    case 'e': useecrs = TRUE;               break;
    case 'a': crccheck = TRUE;              break;
    case 'b': datagen = TRUE;               break;
    case 'v': verbose = TRUE;               break;
    case 's': robinpagesize = atoi(optarg); break;
    case 'm': maxrxpages = atoi(optarg);    break;
    case 'N': memorypoolnumpages = atoi(optarg);    break;   
    case 'P': memorypoolpagesize = atoi(optarg);    break;
    case 'F': sscanf(optarg, "%x", &fpgaversion);    break;    
    case '1': DVS = TRUE;                   break;

    default:
      std::cout << "Invalid option " << c << std::endl;
      std::cout << "Usage: " << argv[0] << " [options]: " << std::endl;
      usage();
      exit (-1);
    }

  std::cout << " Ready to receive ROD events of up to " << robinpagesize * maxrxpages << " words" << std::endl;

  // Initialize the debug macro
  DF::GlobalDebugSettings::setup(dblevel, dbpackage);      
      
  // Install signal handler for SIGQUIT (ctrl + \)
  sigemptyset(&sa.sa_mask);
  sa.sa_flags   = 0;
  sa.sa_handler = sigquit_handler;

  // Dont block in intercept handler
  if (sigaction(SIGQUIT, &sa, NULL) < 0) 
  {
    std::cout << "main: sigaction() FAILED with ";
    code = errno;
    if (code == EFAULT) std::cout << "EFAULT" << std::endl;
    if (code == EINVAL) std::cout << "EINVAL" << std::endl;
    exit(-1);
  }

  // Install signal handler for SIGINT (ctrl + c) 
  sigemptyset(&saint.sa_mask);
  saint.sa_flags   = 0;
  saint.sa_handler = sigint_handler;

  if (sigaction(SIGINT, &saint, NULL) < 0) 
  {
    std::cout << "main: sigaction() FAILED with ";
    code = errno;
    if (code == EFAULT) std::cout << "EFAULT" << std::endl;
    if (code == EINVAL) std::cout << "EINVAL" << std::endl;
    exit(-1);
  }

  code = ts_open(0, TS_DUMMY);
  if (code) 
  {
    rcc_error_print(stdout, code);
    exit (-1);
  }

  robin_nr = (occurence - 1) / 12;
  rol_nr = (occurence - 1) % 12;
  std::cout << "Opening access to ROL " << (rol_nr + 1) << " on ROBINNP " << (robin_nr + 1) << std::endl;
   
  if (verbose)
  {
    std::cout << "memorypoolnumpages = " << memorypoolnumpages<< std::endl;
    std::cout << "memorypoolpagesize = " << memorypoolpagesize << std::endl;
    std::cout << "fpgaversion        = 0x" << std::hex << fpgaversion << std::dec << std::endl;
  } 
   
  try
  {
    module = new RobinNPReadoutModule();

    configuration->set("PhysicalAddress", robin_nr);
    configuration->set("numberOfChannels", 1);
    configuration->set("Pagesize", robinpagesize);
    configuration->set("MaxRxPages", maxrxpages);
    configuration->set("Id", rol_nr, 0);
    configuration->set("ROLPhysicalAddress", rol_nr, 0);
    configuration->set("memoryPoolNumPages", memorypoolnumpages, 0);
    configuration->set("memoryPoolPageSize", memorypoolpagesize, 0);
    configuration->set("RolDataGen", 0, 0);
    configuration->set("CrcCheckInterval", 100000, 0);
    configuration->set("UID", "dummy", 0);
    configuration->set("ForceL1ID", 0, 0);
    configuration->set("FPGAVersion", fpgaversion);
    configuration->set("triggerQueue", 0);

    nextl1id = 0;
    channels = const_cast<std::vector<DataChannel *> *>(module->channels());   //MJ: does the S/W always create all 12 channels or do we have to specify somehow which channels are going to be used?

    if (datagen)
    {
      if(verbose)
	std::cout << "Enabling internal fragment generation for fragments of " << testsize << " words" << std::endl;
      configuration->set("TestSize", testsize, 0);
      configuration->set("RolDataGen", 1, 0);
    }
  
    if (crccheck)
    {
      if(verbose)
	std::cout << "Enabling CRC check for every ROB fragment" << std::endl;
      configuration->set("CrcCheckInterval", 100000, 0);
    }
    
    module->setup(configuration);
    module->configure(cmd);
    module->prepareForRun(cmd);
    module->initTicketing(1);
    prolnp = (RobinNPDataChannel *) (*channels)[0];
  }
  catch (ModulesException& e)
  {
    std::cout << e << std::endl;
    exit(-1);
  }
  
  // Open the file for output
  if (writefile)
  {
    outputFile = open(filename, O_WRONLY|O_CREAT|O_LARGEFILE, S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH);
    if (outputFile < 0)
    {
      std::cout << "Failed to open " << filename << std::endl;
      exit(-1);
    }
  }
        
  // Open the reference file
  if (checkfile)
  {
    inputFile = open(filename2, O_RDONLY);
    if (inputFile < 0)
    {
      std::cout << "Failed to open " << filename2 << std::endl;
      exit(-1);
    }

    int refsize = read(inputFile, refbuf, 0x100000);
    std::cout << "The reference file contains " << refsize << " bytes" << std::endl;
    if (refsize < 44)
    {
      std::cout << "The reference file contains less than 44 bytes (i.e. ROD header + trailer)" << std::endl;
      exit(-1);
    }
    if (refsize == 0x100000)
    {
      std::cout << "The reference file contains more than 1 MB" << std::endl;
      exit(-1);
    }
    close(inputFile);
  }
  
  time(&start_time);
  lpacket = 1;

  std::cout << std::endl << "Press ctrl-\\ to output statistics" << std::endl;
  std::cout << "Press ctrl-c to quit" << std::endl << std::endl;
  
  ipacket = 1;
  l1id = firstl1id;
  mrid = firstl1id;
  
  if(verbose)
  {
    std::cout << "fragment size (words) = " << testsize << std::endl;
    std::cout << "firstl1id = " << firstl1id << std::endl;
  }
  
  sigemptyset(&smask);
  
  run = true;
  while(run && (ipacket <= ((npackets >= 0) ? npackets : (ipacket))))
  {
    sigaddset(&smask, SIGINT);
    sigprocmask(SIG_BLOCK, &smask, NULL);  // Temporarily block ctrl + c

    ticket = prolnp->requestFragment(l1id); 
    if(verbose)
      std::cout << "Requesting L1ID = " << l1id << std::endl;
    
    fragment = dynamic_cast<ROBFragment *> (prolnp->getFragment(ticket)); 	    
    if(verbose)
      std::cout << "Event fragment for L1ID " << l1id << " received" << std::endl;
    
    //update the statistics in the shared memory file
    //MJ Note: This may slow down the program as the delay of "probe" is not known. 
    //It my be OK to call "probe" only every (e.g.) 100 events
    prolnp->probe();
    
    Buffer *fragBuffer = fragment->buffer();
    MemoryPage *memPage = (MemoryPage *) *(fragBuffer->begin());
    u_int *dataptr = (u_int *)memPage->address();
	
    sigprocmask(SIG_UNBLOCK, &smask, NULL);  // Re-enable ctrl + c
  
    fragmentSize = (u_int)fragment->size();
 
    if (fragment->status() & fragStatusPending) 
    {
      if (verbose)
        std::cout << "Event fragment for L1ID " << l1id << " is still pending" << std::endl;
      delete fragment;
    }
    else
    {
      if(verbose)
        std::cout << "ticket = 0x" << HEX(ticket) << std::endl;
  
      if (rawdump)
      {
	if (fragmentSize > rawdump)
          maxdump = rawdump;
	else
          maxdump = fragmentSize;
	
	std::cout << "Dumping the first " << maxdump << " words of the ROB data at " << HEX(dataptr) << std::endl;
	for(loop = 0; loop < maxdump; loop++)
          std::cout << "Word " << loop << " = 0x" << HEX(*dataptr++) << std::endl;
      }
                           
      if (print) 
        std::cout << *fragment;
      
      if (writefile)
      {
        const Buffer* buffer = fragment->buffer();
        Buffer::page_iterator page = buffer->begin(); 
        if (verbose)
          std::cout << "writing " << fragmentSize * 4 << " bytes to file" << std::endl; 
        int isok = write(outputFile, (void *)((u_long)(*page)->address()), fragmentSize * 4);
        if (isok < 0)
        {
          std::cout << "Error in writing to file" << std::endl;
          exit(-1);
        }
      }
                                  
      // Check the event
      if(dcheck)
      {
        flg_error = 0;
        ccode = fragment->check(ipacket - 1, L1extdcheck);
        if (code && verbose)
          std::cout << "Check of ROD fragment returns error " << ccode << std::endl;
        if (ccode != 0)
          errorFound(ccode);
      }
    
      if (checkfile)
      {
        u_int *refptr = (u_int *)refbuf;
        for (int dataword = 0; dataword < fragmentSize; dataword++)
        {
          if (*refptr != *dataptr)
            std::cout << "Data mismatch: Offset = 0x" << HEX(dataword * 4) << " Received data = 0x" << HEX(*dataptr) << "   Expected data = 0x" << HEX(*refptr) << std::endl;
          refptr++;
          dataptr++;
        }
      }

      // We don't need the fragment any more
      delete fragment;

      if (do_not_delete == FALSE)
      {
	bool releaseOk;

	releaseOk = prolnp->releaseFragmentAck(l1id);
	if (releaseOk == FALSE)
          std::cout << "ERROR: failed to release L1ID = " << HEX(l1id) << std::endl;
      }     
      ipacket++;
     
      if(useecrs)
      {
	next_ok = 0;
	while (!next_ok) 
	{
	  next_ok = next_l1id_ecr(prolnp, &l1id);
          if (verbose && next_ok)
            std::cout << "Now requesting L1ID = 0x" << HEX(l1id) << std::endl;
        }
      }
      else
        l1id++;

      if (verbose)
        std::cout << "Number of events received: " << (ipacket - 1) << std::endl;
      }
    
    if (usdelay)
      ts_delay(usdelay);
  }
  terminate_it();
  exit(0); 
}


/*********************************************/
u_int fill_ecr_list(RobinNPDataChannel *prolnp)
/*********************************************/
{      
  static ECRStatisticsBlock ecr_info;
  u_int startid, llp;   
  sigset_t sigint;
  
  //Disable SIGINT to avoid a race condition with the code called on exit
  sigemptyset(&sigint);
  sigaddset(&sigint, SIGINT);
  sigprocmask(SIG_BLOCK, &sigint, NULL);
  
  ecr_info = prolnp->getECR();

  sigprocmask(SIG_UNBLOCK, &sigint, NULL);  

  if (mori != ecr_info.mostRecentId || noecr != ecr_info.necrs)
  {
    mori = ecr_info.mostRecentId;
    noecr = ecr_info.necrs;
  }
  
  if (verbose)
  {
    std::cout << "fill_ecr_list: Most recent Id   = 0x" << HEX(ecr_info.mostRecentId) << std::endl;
    std::cout << "fill_ecr_list: Overflow         = " << ecr_info.overflow << std::endl;
    std::cout << "fill_ecr_list: Number of ECRs   = " << ecr_info.necrs << std::endl;
    for (u_int ecrloop = 0; ecrloop < ecr_info.necrs; ecrloop++)
      std::cout << "fill_ecr_list: Last L1ID before ECR # "<< ecrloop + 1 << " = 0x" << HEX(ecr_info.ecr[ecrloop]) << std::endl;
  }
  
  if (ecr_info.overflow)
  {
    std::cout << "fill_ecr_list: ECR Overflow detected" << std::endl;
    std::cout << "fill_ecr_list: It is therefore impossible to compute the proper L1IDs" << std::endl;
    std::cout << "fill_ecr_list: Have a nice day" << std::endl;
    exit(-1);
  }

  llp = 0;

  for (u_int ecrloop = 0; ecrloop < ecr_info.necrs; ecrloop++)
  {
    if(ecrloop == 0)
    {
      startid = mrid + 1;
      if (verbose)
	std::cout << "fill_ecr_list: first ECR block. startid = 0x" << HEX(startid) << std::endl;
    }
    else
    {
      startid = ecr_info.ecr[ecrloop] & 0xff000000;
      if (verbose)
	std::cout << "fill_ecr_list: another ECR block. startid = 0x" << HEX(startid) << std::endl;
    }

    for(u_int l1idloop = startid; l1idloop < (ecr_info.ecr[ecrloop] + 1); l1idloop++)
    {
      if (verbose)
	std::cout << "fill_ecr_list: ECR: pushing L1ID = 0x" << HEX(l1idloop) << std::endl;
      li1d_list[llp++] = l1idloop;
    }
  }

  if (ecr_info.necrs)
  {
    for (u_int l1idloop = (ecr_info.mostRecentId & 0xff000000); l1idloop < ecr_info.mostRecentId + 1; l1idloop++) 
    {
      if (verbose)
        std::cout << "fill_ecr_list: MR: pushing L1ID = 0x" << HEX(l1idloop) << std::endl;
      li1d_list[llp++] = l1idloop;
    }
  }
  else
  {
    for (u_int l1idloop = mrid + 1; l1idloop < ecr_info.mostRecentId + 1; l1idloop++) 
    {
      if (verbose)
        std::cout << "fill_ecr_list: justMR: pushing L1ID = 0x" << HEX(l1idloop) << std::endl;
      li1d_list[llp++] = l1idloop;
    }
  }

  mrid = ecr_info.mostRecentId;
  
  if (llp > (64 * 1024))
  {
    std::cout << "fill_ecr_list: ERROR: llp overflow" << std::endl;
    exit(-1);
  }
  return(llp);
}


/**********************************************************/      
u_int next_l1id_ecr(RobinNPDataChannel *prolnp, u_int *l1id)
/**********************************************************/      
{
  static u_int have_l1ids = 0, lptr = 0;
  
  if (have_l1ids == 0)
  {
    have_l1ids = fill_ecr_list(prolnp);
    lptr = 0;
  }
  
  if (have_l1ids == 0)
  { 
    if (verbose)
      std::cout << "next_l1id_ecr: Please wait...." << std::endl;
    return(0);  //Please wait
  } 
    
  if (lptr < have_l1ids)
  {
    if  (verbose)
      std::cout << "next_l1id_ecr: lptr = " << lptr << ", have_l1ids = " << have_l1ids << ", li1d_list[lptr] = 0x" << HEX(li1d_list[lptr]) << std::endl;
    
    *l1id = li1d_list[lptr++];
    if (lptr == have_l1ids)
      have_l1ids = 0;
    return(1);
  }  
  return(0);
}








